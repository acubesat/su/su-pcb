# Payload PCB

![Payload PCB Render](SU-PCB-Render.png)

The Payload PCB is the main board of AcubeSAT's payload. It is responsible for coordinating all of the different subsystems that make up the full experimental procedure. More specifically, the Payload PCB is responsible for controlling:
-   The **fluidic system**, which consists of one pump (with an internal stepper motor) and eight latching valves.
-   The **imaging system**, which consists of one USB camera and an LED PCB that serves as a flash for the camera.
-   The **active thermal control system**, which consists of several heaters and NTC thermistors for temperature readings.

Additionally, the Payload PCB is equipped with other sensors to monitor environmental conditions, including:
-   Humidity
-   Pressure
-   Temperature
-   Radiation / TID accumulation
