EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 14
Title "Science Unit Main Board (Payload Board)"
Date "2022-03-13"
Rev "2.0 (EM)"
Comp "SpaceDot"
Comment1 "AcubeSAT"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Components:ZXMHC3A01T8TA Q?
U 1 1 5FA6BC02
P 4450 2150
AR Path="/5FA6BC02" Ref="Q?"  Part="1" 
AR Path="/5FFAB9B8/5FA6BC02" Ref="Q7"  Part="1" 
AR Path="/6009AF74/5FA6BC02" Ref="Q?"  Part="1" 
F 0 "Q7" H 5150 2415 50  0000 C CNN
F 1 "ZXMHC3A01T8TA" H 5150 2324 50  0000 C CNN
F 2 "Components:ZXMHC3A01T8TA" H 5700 2250 50  0001 L CNN
F 3 "https://www.diodes.com/assets/Datasheets/ZXMHC3F381N8.pdf" H 5700 2150 50  0001 L CNN
F 4 "MOSFET Dual N/P-Ch 30V 4.98A/4.13A SOIC8 Diodes Inc ZXMHC3F381N8TC Quad N/P-channel MOSFET Transistor, 8-Pin SO" H 5700 2050 50  0001 L CNN "Description"
F 5 "1.75" H 5700 1950 50  0001 L CNN "Height"
F 6 "522-ZXMHC3F381N8TC" H 5700 1850 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Diodes-Incorporated/ZXMHC3F381N8TC?qs=7E5rbXJDVJoJr15tcLBfrA%3D%3D" H 5700 1750 50  0001 L CNN "Mouser Price/Stock"
F 8 "Diodes Inc." H 5700 1650 50  0001 L CNN "Manufacturer_Name"
F 9 "ZXMHC3F381N8TC" H 5700 1550 50  0001 L CNN "Manufacturer_Part_Number"
	1    4450 2150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 600167B1
P 4100 2250
AR Path="/600167B1" Ref="#PWR?"  Part="1" 
AR Path="/5FFAB9B8/600167B1" Ref="#PWR014"  Part="1" 
AR Path="/6009AF74/600167B1" Ref="#PWR?"  Part="1" 
F 0 "#PWR014" H 4100 2000 50  0001 C CNN
F 1 "GND" H 4105 2077 50  0000 C CNN
F 2 "" H 4100 2250 50  0001 C CNN
F 3 "" H 4100 2250 50  0001 C CNN
	1    4100 2250
	0    1    1    0   
$EndComp
Wire Wire Line
	5600 3450 5700 3450
Connection ~ 5700 3150
Wire Wire Line
	5600 3150 5700 3150
Wire Wire Line
	5700 3150 5700 2850
Wire Wire Line
	5600 2850 5700 2850
Connection ~ 5700 3450
Wire Wire Line
	5700 3750 5700 3450
Wire Wire Line
	5600 3750 5700 3750
$Comp
L Device:R R?
U 1 1 5FA6BC07
P 5450 2850
AR Path="/5FA6BC07" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/5FA6BC07" Ref="R7"  Part="1" 
AR Path="/6009AF74/5FA6BC07" Ref="R?"  Part="1" 
F 0 "R7" H 5520 2896 50  0000 L CNN
F 1 "10k" H 5520 2805 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5380 2850 50  0001 C CNN
F 3 "~" H 5450 2850 50  0001 C CNN
	1    5450 2850
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FA6BC08
P 5450 3150
AR Path="/5FA6BC08" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/5FA6BC08" Ref="R8"  Part="1" 
AR Path="/6009AF74/5FA6BC08" Ref="R?"  Part="1" 
F 0 "R8" H 5520 3196 50  0000 L CNN
F 1 "10k" H 5520 3105 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5380 3150 50  0001 C CNN
F 3 "~" H 5450 3150 50  0001 C CNN
	1    5450 3150
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FA6BC09
P 5450 3450
AR Path="/5FA6BC09" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/5FA6BC09" Ref="R9"  Part="1" 
AR Path="/6009AF74/5FA6BC09" Ref="R?"  Part="1" 
F 0 "R9" H 5520 3496 50  0000 L CNN
F 1 "10k" H 5520 3405 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5380 3450 50  0001 C CNN
F 3 "~" H 5450 3450 50  0001 C CNN
	1    5450 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FA6BC0A
P 5450 3750
AR Path="/5FA6BC0A" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/5FA6BC0A" Ref="R10"  Part="1" 
AR Path="/6009AF74/5FA6BC0A" Ref="R?"  Part="1" 
F 0 "R10" H 5520 3796 50  0000 L CNN
F 1 "10k" H 5520 3705 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5380 3750 50  0001 C CNN
F 3 "~" H 5450 3750 50  0001 C CNN
	1    5450 3750
	0    -1   -1   0   
$EndComp
Text HLabel 4450 2450 0    50   Input ~ 0
FLOW_PUMP_PWM_A2
Text HLabel 5300 2850 0    50   Input ~ 0
FLOW_PUMP_PWM_A1
Text HLabel 5300 3150 0    50   Input ~ 0
FLOW_PUMP_PWM_A2
Text HLabel 5300 3450 0    50   Input ~ 0
FLOW_PUMP_PWM_B1
Text HLabel 5300 3750 0    50   Input ~ 0
FLOW_PUMP_PWM_B2
$Comp
L Components:ZXMHC3A01T8TA Q?
U 1 1 5FA6BBFA
P 4550 5200
AR Path="/5FA6BBFA" Ref="Q?"  Part="1" 
AR Path="/5FFAB9B8/5FA6BBFA" Ref="Q8"  Part="1" 
AR Path="/6009AF74/5FA6BBFA" Ref="Q?"  Part="1" 
F 0 "Q8" H 5250 5465 50  0000 C CNN
F 1 "ZXMHC3A01T8TA" H 5250 5374 50  0000 C CNN
F 2 "Components:ZXMHC3A01T8TA" H 5800 5300 50  0001 L CNN
F 3 "https://www.diodes.com/assets/Datasheets/ZXMHC3F381N8.pdf" H 5800 5200 50  0001 L CNN
F 4 "MOSFET Dual N/P-Ch 30V 4.98A/4.13A SOIC8 Diodes Inc ZXMHC3F381N8TC Quad N/P-channel MOSFET Transistor, 8-Pin SO" H 5800 5100 50  0001 L CNN "Description"
F 5 "1.75" H 5800 5000 50  0001 L CNN "Height"
F 6 "522-ZXMHC3F381N8TC" H 5800 4900 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Diodes-Incorporated/ZXMHC3F381N8TC?qs=7E5rbXJDVJoJr15tcLBfrA%3D%3D" H 5800 4800 50  0001 L CNN "Mouser Price/Stock"
F 8 "Diodes Inc." H 5800 4700 50  0001 L CNN "Manufacturer_Name"
F 9 "ZXMHC3F381N8TC" H 5800 4600 50  0001 L CNN "Manufacturer_Part_Number"
	1    4550 5200
	1    0    0    -1  
$EndComp
Text Notes 6000 3000 0    50   ~ 0
2.2K working RES values
Text Notes 4050 3350 0    50   ~ 0
out4-out8
Text GLabel 6200 3300 2    50   Input ~ 0
5V
Text Notes 6400 3100 0    50   ~ 0
or 5V
Wire Wire Line
	5700 3150 5700 3300
Wire Wire Line
	6200 3300 5700 3300
Connection ~ 5700 3300
Wire Wire Line
	5700 3300 5700 3450
$Comp
L Components:NTK3134NT1G Q?
U 1 1 62C777AC
P 2950 2150
AR Path="/5F938AED/62C777AC" Ref="Q?"  Part="1" 
AR Path="/5FFAB9B8/62C777AC" Ref="Q9"  Part="1" 
F 0 "Q9" H 3380 2296 50  0000 L CNN
F 1 "FDV304P" H 3380 2205 50  0000 L CNN
F 2 "Components:FDV304P" H 3400 2100 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 3400 2000 50  0001 L CNN
F 4 "Power MOSFET" H 3400 1900 50  0001 L CNN "Description"
F 5 "0.55" H 3400 1800 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 3400 1700 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 3400 1600 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 3400 1500 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 3400 1400 50  0001 L CNN "Manufacturer_Part_Number"
	1    2950 2150
	1    0    0    -1  
$EndComp
$Comp
L Components:NTK3134NT1G Q?
U 1 1 62C7896B
P 4150 1850
AR Path="/5F938AED/62C7896B" Ref="Q?"  Part="1" 
AR Path="/5FFAB9B8/62C7896B" Ref="Q11"  Part="1" 
F 0 "Q11" H 4580 1996 50  0000 L CNN
F 1 "FDV304P" H 4580 1905 50  0000 L CNN
F 2 "Components:FDV304P" H 4600 1800 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 4600 1700 50  0001 L CNN
F 4 "Power MOSFET" H 4600 1600 50  0001 L CNN "Description"
F 5 "0.55" H 4600 1500 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 4600 1400 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 4600 1300 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 4600 1200 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 4600 1100 50  0001 L CNN "Manufacturer_Part_Number"
	1    4150 1850
	1    0    0    -1  
$EndComp
Text HLabel 2550 2150 1    50   Input ~ 0
FLOW_PUMP_PWM_A1
$Comp
L Device:R R?
U 1 1 62C80A36
P 2700 2150
AR Path="/62C80A36" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/62C80A36" Ref="R?"  Part="1" 
AR Path="/6009AF74/62C80A36" Ref="R?"  Part="1" 
F 0 "R?" H 2770 2196 50  0000 L CNN
F 1 "1k" H 2770 2105 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2630 2150 50  0001 C CNN
F 3 "~" H 2700 2150 50  0001 C CNN
	1    2700 2150
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 62C8173F
P 3900 1850
AR Path="/62C8173F" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/62C8173F" Ref="R?"  Part="1" 
AR Path="/6009AF74/62C8173F" Ref="R?"  Part="1" 
F 0 "R?" H 3970 1896 50  0000 L CNN
F 1 "1k" H 3970 1805 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3830 1850 50  0001 C CNN
F 3 "~" H 3900 1850 50  0001 C CNN
	1    3900 1850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2950 2150 2850 2150
Wire Wire Line
	4050 1850 4150 1850
Wire Wire Line
	4450 2350 3250 2350
Wire Wire Line
	4450 2150 4450 2050
$Comp
L Device:R R?
U 1 1 62C98B87
P 3250 2500
AR Path="/62C98B87" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/62C98B87" Ref="R?"  Part="1" 
AR Path="/6009AF74/62C98B87" Ref="R?"  Part="1" 
F 0 "R?" H 3320 2546 50  0000 L CNN
F 1 "10k" H 3320 2455 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3180 2500 50  0001 C CNN
F 3 "~" H 3250 2500 50  0001 C CNN
	1    3250 2500
	1    0    0    -1  
$EndComp
Connection ~ 3250 2350
$Comp
L power:GND #PWR?
U 1 1 62C99206
P 3250 2650
AR Path="/62C99206" Ref="#PWR?"  Part="1" 
AR Path="/5FFAB9B8/62C99206" Ref="#PWR?"  Part="1" 
AR Path="/6009AF74/62C99206" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3250 2400 50  0001 C CNN
F 1 "GND" H 3255 2477 50  0000 C CNN
F 2 "" H 3250 2650 50  0001 C CNN
F 3 "" H 3250 2650 50  0001 C CNN
	1    3250 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 62C99B0D
P 4300 2150
AR Path="/62C99B0D" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/62C99B0D" Ref="R?"  Part="1" 
AR Path="/6009AF74/62C99B0D" Ref="R?"  Part="1" 
F 0 "R?" V 4200 2050 50  0000 L CNN
F 1 "10k" V 4200 2150 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4230 2150 50  0001 C CNN
F 3 "~" H 4300 2150 50  0001 C CNN
	1    4300 2150
	0    1    1    0   
$EndComp
Connection ~ 4450 2150
Wire Wire Line
	4150 2150 4150 2250
Wire Wire Line
	4150 2250 4100 2250
Wire Wire Line
	4150 2250 4450 2250
Connection ~ 4150 2250
Text HLabel 3750 1850 1    50   Input ~ 0
FLOW_PUMP_PWM_A2
Text Label 5750 2250 0    50   ~ 0
LOAD_SUPPLY
Text HLabel 6300 2250 2    50   Input ~ 0
VCC
Wire Wire Line
	5650 2250 6300 2250
Text HLabel 5650 2150 2    50   Output ~ 0
FLOW_PUMP_PHASE_A-
Text GLabel 3250 1650 1    50   Input ~ 0
5V
Wire Wire Line
	3250 1650 3250 1750
Text GLabel 4450 1350 1    50   Input ~ 0
5V
Wire Wire Line
	4450 1350 4450 1450
Text HLabel 5650 2350 2    50   Output ~ 0
FLOW_PUMP_PHASE_A+
Text HLabel 5650 2450 2    50   Input ~ 0
FLOW_PUMP_PWM_A1
$Comp
L power:GND #PWR?
U 1 1 62CC4F0B
P 4200 5300
AR Path="/62CC4F0B" Ref="#PWR?"  Part="1" 
AR Path="/5FFAB9B8/62CC4F0B" Ref="#PWR?"  Part="1" 
AR Path="/6009AF74/62CC4F0B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4200 5050 50  0001 C CNN
F 1 "GND" H 4205 5127 50  0000 C CNN
F 2 "" H 4200 5300 50  0001 C CNN
F 3 "" H 4200 5300 50  0001 C CNN
	1    4200 5300
	0    1    1    0   
$EndComp
Text HLabel 4550 5500 0    50   Input ~ 0
FLOW_PUMP_PWM_B2
$Comp
L Components:NTK3134NT1G Q?
U 1 1 62CC4F18
P 3050 5200
AR Path="/5F938AED/62CC4F18" Ref="Q?"  Part="1" 
AR Path="/5FFAB9B8/62CC4F18" Ref="Q10"  Part="1" 
F 0 "Q10" H 3480 5346 50  0000 L CNN
F 1 "FDV304P" H 3480 5255 50  0000 L CNN
F 2 "Components:FDV304P" H 3500 5150 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 3500 5050 50  0001 L CNN
F 4 "Power MOSFET" H 3500 4950 50  0001 L CNN "Description"
F 5 "0.55" H 3500 4850 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 3500 4750 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 3500 4650 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 3500 4550 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 3500 4450 50  0001 L CNN "Manufacturer_Part_Number"
	1    3050 5200
	1    0    0    -1  
$EndComp
$Comp
L Components:NTK3134NT1G Q?
U 1 1 62CC4F24
P 4250 4900
AR Path="/5F938AED/62CC4F24" Ref="Q?"  Part="1" 
AR Path="/5FFAB9B8/62CC4F24" Ref="Q12"  Part="1" 
F 0 "Q12" H 4680 5046 50  0000 L CNN
F 1 "FDV304P" H 4680 4955 50  0000 L CNN
F 2 "Components:FDV304P" H 4700 4850 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 4700 4750 50  0001 L CNN
F 4 "Power MOSFET" H 4700 4650 50  0001 L CNN "Description"
F 5 "0.55" H 4700 4550 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 4700 4450 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 4700 4350 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 4700 4250 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 4700 4150 50  0001 L CNN "Manufacturer_Part_Number"
	1    4250 4900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 62CC4F2B
P 2800 5200
AR Path="/62CC4F2B" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/62CC4F2B" Ref="R?"  Part="1" 
AR Path="/6009AF74/62CC4F2B" Ref="R?"  Part="1" 
F 0 "R?" H 2870 5246 50  0000 L CNN
F 1 "1k" H 2870 5155 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2730 5200 50  0001 C CNN
F 3 "~" H 2800 5200 50  0001 C CNN
	1    2800 5200
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 62CC4F31
P 4000 4900
AR Path="/62CC4F31" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/62CC4F31" Ref="R?"  Part="1" 
AR Path="/6009AF74/62CC4F31" Ref="R?"  Part="1" 
F 0 "R?" H 4070 4946 50  0000 L CNN
F 1 "1k" H 4070 4855 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3930 4900 50  0001 C CNN
F 3 "~" H 4000 4900 50  0001 C CNN
	1    4000 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3050 5200 2950 5200
Wire Wire Line
	4150 4900 4250 4900
Wire Wire Line
	4550 5400 3350 5400
$Comp
L Device:R R?
U 1 1 62CC4F3A
P 3350 5550
AR Path="/62CC4F3A" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/62CC4F3A" Ref="R?"  Part="1" 
AR Path="/6009AF74/62CC4F3A" Ref="R?"  Part="1" 
F 0 "R?" H 3420 5596 50  0000 L CNN
F 1 "10k" H 3420 5505 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3280 5550 50  0001 C CNN
F 3 "~" H 3350 5550 50  0001 C CNN
	1    3350 5550
	1    0    0    -1  
$EndComp
Connection ~ 3350 5400
$Comp
L power:GND #PWR?
U 1 1 62CC4F41
P 3350 5700
AR Path="/62CC4F41" Ref="#PWR?"  Part="1" 
AR Path="/5FFAB9B8/62CC4F41" Ref="#PWR?"  Part="1" 
AR Path="/6009AF74/62CC4F41" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3350 5450 50  0001 C CNN
F 1 "GND" H 3355 5527 50  0000 C CNN
F 2 "" H 3350 5700 50  0001 C CNN
F 3 "" H 3350 5700 50  0001 C CNN
	1    3350 5700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 62CC4F47
P 4400 5200
AR Path="/62CC4F47" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/62CC4F47" Ref="R?"  Part="1" 
AR Path="/6009AF74/62CC4F47" Ref="R?"  Part="1" 
F 0 "R?" V 4300 5100 50  0000 L CNN
F 1 "10k" V 4300 5200 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4330 5200 50  0001 C CNN
F 3 "~" H 4400 5200 50  0001 C CNN
	1    4400 5200
	0    1    1    0   
$EndComp
Wire Wire Line
	4250 5200 4250 5300
Wire Wire Line
	4250 5300 4200 5300
Wire Wire Line
	4250 5300 4550 5300
Connection ~ 4250 5300
Text GLabel 3350 4700 1    50   Input ~ 0
5V
Wire Wire Line
	3350 4700 3350 4800
Text GLabel 4550 4400 1    50   Input ~ 0
5V
Wire Wire Line
	4550 4400 4550 4500
Text HLabel 2650 5200 1    50   Input ~ 0
FLOW_PUMP_PWM_B1
Text HLabel 3850 4900 1    50   Input ~ 0
FLOW_PUMP_PWM_B2
Wire Wire Line
	4550 5100 4550 5200
Connection ~ 4550 5200
Text Label 5850 5300 0    50   ~ 0
LOAD_SUPPLY
Text HLabel 6400 5300 2    50   Input ~ 0
VCC
Wire Wire Line
	5750 5300 6400 5300
Text HLabel 5750 5200 2    50   Output ~ 0
FLOW_PUMP_PHASE_B-
Text HLabel 5750 5400 2    50   Output ~ 0
FLOW_PUMP_PHASE_B+
Text HLabel 5750 5500 2    50   Input ~ 0
FLOW_PUMP_PWM_B1
$EndSCHEMATC
