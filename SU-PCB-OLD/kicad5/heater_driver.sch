EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 12 14
Title "Science Unit Main Board (Payload Board)"
Date "2022-03-13"
Rev "2.0 (EM)"
Comp "SpaceDot"
Comment1 "AcubeSAT"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 5F707E6D
P 5750 3600
AR Path="/5F707E6D" Ref="R?"  Part="1" 
AR Path="/5F700CE8/5F707E6D" Ref="R78"  Part="1" 
F 0 "R78" H 5820 3646 50  0000 L CNN
F 1 "10k" H 5820 3555 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5680 3600 50  0001 C CNN
F 3 "~" H 5750 3600 50  0001 C CNN
	1    5750 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F707E74
P 5750 3850
AR Path="/5F707E74" Ref="#PWR?"  Part="1" 
AR Path="/5F700CE8/5F707E74" Ref="#PWR0102"  Part="1" 
F 0 "#PWR0102" H 5750 3600 50  0001 C CNN
F 1 "GND" H 5755 3677 50  0000 C CNN
F 2 "" H 5750 3850 50  0001 C CNN
F 3 "" H 5750 3850 50  0001 C CNN
	1    5750 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3750 5750 3850
Text HLabel 6100 3000 2    50   Output ~ 0
HEATER_GND_CHIP
Text HLabel 5100 3400 0    50   Input ~ 0
HEATER_CONTROL_CHIP
$Comp
L Components:NTK3134NT1G Q43
U 1 1 5F6CF649
P 5800 3400
F 0 "Q43" H 6230 3546 50  0000 L CNN
F 1 "NTK3134NT1G" H 6230 3455 50  0000 L CNN
F 2 "Components:NTK3134NT1G" H 6250 3350 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 6250 3250 50  0001 L CNN
F 4 "Power MOSFET" H 6250 3150 50  0001 L CNN "Description"
F 5 "0.55" H 6250 3050 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 6250 2950 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 6250 2850 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 6250 2750 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 6250 2650 50  0001 L CNN "Manufacturer_Part_Number"
	1    5800 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F6D1264
P 6100 3650
AR Path="/5F6D1264" Ref="#PWR?"  Part="1" 
AR Path="/5F700CE8/5F6D1264" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 6100 3400 50  0001 C CNN
F 1 "GND" H 6105 3477 50  0000 C CNN
F 2 "" H 6100 3650 50  0001 C CNN
F 3 "" H 6100 3650 50  0001 C CNN
	1    6100 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 3650 6100 3600
Wire Wire Line
	5800 3400 5750 3400
Wire Wire Line
	5750 3450 5750 3400
Connection ~ 5750 3400
Wire Wire Line
	5750 3400 5100 3400
$Comp
L Device:R R?
U 1 1 5F6D5C80
P 5750 4750
AR Path="/5F6D5C80" Ref="R?"  Part="1" 
AR Path="/5F700CE8/5F6D5C80" Ref="R79"  Part="1" 
F 0 "R79" H 5820 4796 50  0000 L CNN
F 1 "10k" H 5820 4705 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5680 4750 50  0001 C CNN
F 3 "~" H 5750 4750 50  0001 C CNN
	1    5750 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F6D5C86
P 5750 5000
AR Path="/5F6D5C86" Ref="#PWR?"  Part="1" 
AR Path="/5F700CE8/5F6D5C86" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 5750 4750 50  0001 C CNN
F 1 "GND" H 5755 4827 50  0000 C CNN
F 2 "" H 5750 5000 50  0001 C CNN
F 3 "" H 5750 5000 50  0001 C CNN
	1    5750 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 4900 5750 5000
Text HLabel 6100 4150 2    50   Output ~ 0
HEATER_GND_1
Text HLabel 5100 4550 0    50   Input ~ 0
HEATER_CONTROL_1
$Comp
L Components:NTK3134NT1G Q44
U 1 1 5F6D5C95
P 5800 4550
F 0 "Q44" H 6230 4696 50  0000 L CNN
F 1 "NTK3134NT1G" H 6230 4605 50  0000 L CNN
F 2 "Components:NTK3134NT1G" H 6250 4500 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 6250 4400 50  0001 L CNN
F 4 "Power MOSFET" H 6250 4300 50  0001 L CNN "Description"
F 5 "0.55" H 6250 4200 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 6250 4100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 6250 4000 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 6250 3900 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 6250 3800 50  0001 L CNN "Manufacturer_Part_Number"
	1    5800 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F6D5C9B
P 6100 4800
AR Path="/5F6D5C9B" Ref="#PWR?"  Part="1" 
AR Path="/5F700CE8/5F6D5C9B" Ref="#PWR0105"  Part="1" 
F 0 "#PWR0105" H 6100 4550 50  0001 C CNN
F 1 "GND" H 6105 4627 50  0000 C CNN
F 2 "" H 6100 4800 50  0001 C CNN
F 3 "" H 6100 4800 50  0001 C CNN
	1    6100 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 4800 6100 4750
Wire Wire Line
	5800 4550 5750 4550
Wire Wire Line
	5750 4600 5750 4550
Connection ~ 5750 4550
Wire Wire Line
	5750 4550 5100 4550
Text Notes 5350 1850 0    50   ~ 0
Heaters for chip and bag?
$EndSCHEMATC
