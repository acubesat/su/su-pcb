EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 6 14
Title "Science Unit Main Board (Payload Board)"
Date "2022-03-13"
Rev "2.0 (EM)"
Comp "SpaceDot"
Comment1 "AcubeSAT"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Notes Line
	5250 950  8350 950 
Wire Notes Line
	5250 6050 8350 6050
Wire Notes Line
	5250 6050 5250 950 
Wire Notes Line
	4900 950  4900 9100
Wire Notes Line
	750  950  4900 950 
Wire Notes Line
	750  9100 4900 9100
Wire Notes Line
	750  950  750  9100
Text Label 6100 5750 2    50   ~ 0
FLOW_VALVE_8_REV+
Text Label 6100 5450 2    50   ~ 0
FLOW_VALVE_8_FWD+
Text Label 6100 5150 2    50   ~ 0
FLOW_VALVE_7_REV+
Text Label 6100 4850 2    50   ~ 0
FLOW_VALVE_7_FWD+
Text Label 6100 4550 2    50   ~ 0
FLOW_VALVE_6_REV+
Text Label 6100 4250 2    50   ~ 0
FLOW_VALVE_6_FWD+
Text Label 6100 3950 2    50   ~ 0
FLOW_VALVE_5_REV+
Text Label 6100 3650 2    50   ~ 0
FLOW_VALVE_5_FWD+
Text Label 6100 3350 2    50   ~ 0
FLOW_VALVE_4_REV+
Text Label 6100 3050 2    50   ~ 0
FLOW_VALVE_4_FWD+
Text Label 2050 7150 0    50   ~ 0
FLOW_VALVE_4_FWD+
Text Label 6100 2750 2    50   ~ 0
FLOW_VALVE_3_REV+
Text Label 6100 2450 2    50   ~ 0
FLOW_VALVE_3_FWD+
Text Label 6100 2150 2    50   ~ 0
FLOW_VALVE_2_REV+
Text Label 6100 1850 2    50   ~ 0
FLOW_VALVE_2_FWD+
Text Label 6100 1550 2    50   ~ 0
FLOW_VALVE_1_REV+
Text Label 6100 1250 2    50   ~ 0
FLOW_VALVE_1_FWD+
Wire Wire Line
	6400 5150 6450 5150
$Comp
L Device:R R?
U 1 1 5FA3A84E
P 6250 5150
AR Path="/5FA3A84E" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3A84E" Ref="R51"  Part="1" 
F 0 "R51" H 6320 5196 50  0000 L CNN
F 1 "10k" H 6320 5105 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6180 5150 50  0001 C CNN
F 3 "~" H 6250 5150 50  0001 C CNN
	1    6250 5150
	0    1    1    0   
$EndComp
Wire Wire Line
	4050 8750 4050 8800
$Comp
L Components:RYM002N05T2CL Q?
U 1 1 5FA3A85B
P 3750 8550
AR Path="/5FA3A85B" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3A85B" Ref="Q34"  Part="1" 
F 0 "Q34" H 4180 8696 50  0000 L CNN
F 1 "RU1J002YNTCL" H 4180 8605 50  0000 L CNN
F 2 "Components:RYM002N05T2CL" H 4200 8500 50  0001 L CNN
F 3 "https://www.rohm.com/datasheet/RYM002N05/rym002n05-e" H 4200 8400 50  0001 L CNN
F 4 "MOSFET .9V DRIVE N-Ch MOSFET" H 4200 8300 50  0001 L CNN "Description"
F 5 "0.55" H 4200 8200 50  0001 L CNN "Height"
F 6 "755-RYM002N05T2CL" H 4200 8100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ROHM-Semiconductor/RYM002N05T2CL?qs=IDm2WsMLa1UWVnvVQ8m33w%3D%3D" H 4200 8000 50  0001 L CNN "Mouser Price/Stock"
F 8 "ROHM Semiconductor" H 4200 7900 50  0001 L CNN "Manufacturer_Name"
F 9 "RYM002N05T2CL" H 4200 7800 50  0001 L CNN "Manufacturer_Part_Number"
	1    3750 8550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3A861
P 4050 8800
AR Path="/5FA3A861" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3A861" Ref="#PWR077"  Part="1" 
F 0 "#PWR077" H 4050 8550 50  0001 C CNN
F 1 "GND" H 4055 8627 50  0000 C CNN
F 2 "" H 4050 8800 50  0001 C CNN
F 3 "" H 4050 8800 50  0001 C CNN
	1    4050 8800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 7750 4050 7800
$Comp
L Components:RYM002N05T2CL Q?
U 1 1 5FA3A86E
P 3750 7550
AR Path="/5FA3A86E" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3A86E" Ref="Q33"  Part="1" 
F 0 "Q33" H 4180 7696 50  0000 L CNN
F 1 "RU1J002YNTCL" H 4180 7605 50  0000 L CNN
F 2 "Components:RYM002N05T2CL" H 4200 7500 50  0001 L CNN
F 3 "https://www.rohm.com/datasheet/RYM002N05/rym002n05-e" H 4200 7400 50  0001 L CNN
F 4 "MOSFET .9V DRIVE N-Ch MOSFET" H 4200 7300 50  0001 L CNN "Description"
F 5 "0.55" H 4200 7200 50  0001 L CNN "Height"
F 6 "755-RYM002N05T2CL" H 4200 7100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ROHM-Semiconductor/RYM002N05T2CL?qs=IDm2WsMLa1UWVnvVQ8m33w%3D%3D" H 4200 7000 50  0001 L CNN "Mouser Price/Stock"
F 8 "ROHM Semiconductor" H 4200 6900 50  0001 L CNN "Manufacturer_Name"
F 9 "RYM002N05T2CL" H 4200 6800 50  0001 L CNN "Manufacturer_Part_Number"
	1    3750 7550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3A874
P 4050 7800
AR Path="/5FA3A874" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3A874" Ref="#PWR076"  Part="1" 
F 0 "#PWR076" H 4050 7550 50  0001 C CNN
F 1 "GND" H 4055 7627 50  0000 C CNN
F 2 "" H 4050 7800 50  0001 C CNN
F 3 "" H 4050 7800 50  0001 C CNN
	1    4050 7800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 6750 4050 6800
$Comp
L Components:RYM002N05T2CL Q?
U 1 1 5FA3A881
P 3750 6550
AR Path="/5FA3A881" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3A881" Ref="Q32"  Part="1" 
F 0 "Q32" H 4180 6696 50  0000 L CNN
F 1 "RU1J002YNTCL" H 4180 6605 50  0000 L CNN
F 2 "Components:RYM002N05T2CL" H 4200 6500 50  0001 L CNN
F 3 "https://www.rohm.com/datasheet/RYM002N05/rym002n05-e" H 4200 6400 50  0001 L CNN
F 4 "MOSFET .9V DRIVE N-Ch MOSFET" H 4200 6300 50  0001 L CNN "Description"
F 5 "0.55" H 4200 6200 50  0001 L CNN "Height"
F 6 "755-RYM002N05T2CL" H 4200 6100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ROHM-Semiconductor/RYM002N05T2CL?qs=IDm2WsMLa1UWVnvVQ8m33w%3D%3D" H 4200 6000 50  0001 L CNN "Mouser Price/Stock"
F 8 "ROHM Semiconductor" H 4200 5900 50  0001 L CNN "Manufacturer_Name"
F 9 "RYM002N05T2CL" H 4200 5800 50  0001 L CNN "Manufacturer_Part_Number"
	1    3750 6550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3A887
P 4050 6800
AR Path="/5FA3A887" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3A887" Ref="#PWR075"  Part="1" 
F 0 "#PWR075" H 4050 6550 50  0001 C CNN
F 1 "GND" H 4055 6627 50  0000 C CNN
F 2 "" H 4050 6800 50  0001 C CNN
F 3 "" H 4050 6800 50  0001 C CNN
	1    4050 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 5750 4050 5800
$Comp
L Components:RYM002N05T2CL Q?
U 1 1 5FA3A894
P 3750 5550
AR Path="/5FA3A894" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3A894" Ref="Q31"  Part="1" 
F 0 "Q31" H 4180 5696 50  0000 L CNN
F 1 "RU1J002YNTCL" H 4180 5605 50  0000 L CNN
F 2 "Components:RYM002N05T2CL" H 4200 5500 50  0001 L CNN
F 3 "https://www.rohm.com/datasheet/RYM002N05/rym002n05-e" H 4200 5400 50  0001 L CNN
F 4 "MOSFET .9V DRIVE N-Ch MOSFET" H 4200 5300 50  0001 L CNN "Description"
F 5 "0.55" H 4200 5200 50  0001 L CNN "Height"
F 6 "755-RYM002N05T2CL" H 4200 5100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ROHM-Semiconductor/RYM002N05T2CL?qs=IDm2WsMLa1UWVnvVQ8m33w%3D%3D" H 4200 5000 50  0001 L CNN "Mouser Price/Stock"
F 8 "ROHM Semiconductor" H 4200 4900 50  0001 L CNN "Manufacturer_Name"
F 9 "RYM002N05T2CL" H 4200 4800 50  0001 L CNN "Manufacturer_Part_Number"
	1    3750 5550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3A89A
P 4050 5800
AR Path="/5FA3A89A" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3A89A" Ref="#PWR074"  Part="1" 
F 0 "#PWR074" H 4050 5550 50  0001 C CNN
F 1 "GND" H 4055 5627 50  0000 C CNN
F 2 "" H 4050 5800 50  0001 C CNN
F 3 "" H 4050 5800 50  0001 C CNN
	1    4050 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 4750 4050 4800
$Comp
L Components:RYM002N05T2CL Q?
U 1 1 5FA3A8A7
P 3750 4550
AR Path="/5FA3A8A7" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3A8A7" Ref="Q30"  Part="1" 
F 0 "Q30" H 4180 4696 50  0000 L CNN
F 1 "RU1J002YNTCL" H 4180 4605 50  0000 L CNN
F 2 "Components:RYM002N05T2CL" H 4200 4500 50  0001 L CNN
F 3 "https://www.rohm.com/datasheet/RYM002N05/rym002n05-e" H 4200 4400 50  0001 L CNN
F 4 "MOSFET .9V DRIVE N-Ch MOSFET" H 4200 4300 50  0001 L CNN "Description"
F 5 "0.55" H 4200 4200 50  0001 L CNN "Height"
F 6 "755-RYM002N05T2CL" H 4200 4100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ROHM-Semiconductor/RYM002N05T2CL?qs=IDm2WsMLa1UWVnvVQ8m33w%3D%3D" H 4200 4000 50  0001 L CNN "Mouser Price/Stock"
F 8 "ROHM Semiconductor" H 4200 3900 50  0001 L CNN "Manufacturer_Name"
F 9 "RYM002N05T2CL" H 4200 3800 50  0001 L CNN "Manufacturer_Part_Number"
	1    3750 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3A8AD
P 4050 4800
AR Path="/5FA3A8AD" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3A8AD" Ref="#PWR073"  Part="1" 
F 0 "#PWR073" H 4050 4550 50  0001 C CNN
F 1 "GND" H 4055 4627 50  0000 C CNN
F 2 "" H 4050 4800 50  0001 C CNN
F 3 "" H 4050 4800 50  0001 C CNN
	1    4050 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 3750 4050 3800
$Comp
L Components:RYM002N05T2CL Q?
U 1 1 5FA3A8BA
P 3750 3550
AR Path="/5FA3A8BA" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3A8BA" Ref="Q29"  Part="1" 
F 0 "Q29" H 4180 3696 50  0000 L CNN
F 1 "RU1J002YNTCL" H 4180 3605 50  0000 L CNN
F 2 "Components:RYM002N05T2CL" H 4200 3500 50  0001 L CNN
F 3 "https://www.rohm.com/datasheet/RYM002N05/rym002n05-e" H 4200 3400 50  0001 L CNN
F 4 "MOSFET .9V DRIVE N-Ch MOSFET" H 4200 3300 50  0001 L CNN "Description"
F 5 "0.55" H 4200 3200 50  0001 L CNN "Height"
F 6 "755-RYM002N05T2CL" H 4200 3100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ROHM-Semiconductor/RYM002N05T2CL?qs=IDm2WsMLa1UWVnvVQ8m33w%3D%3D" H 4200 3000 50  0001 L CNN "Mouser Price/Stock"
F 8 "ROHM Semiconductor" H 4200 2900 50  0001 L CNN "Manufacturer_Name"
F 9 "RYM002N05T2CL" H 4200 2800 50  0001 L CNN "Manufacturer_Part_Number"
	1    3750 3550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3A8C0
P 4050 3800
AR Path="/5FA3A8C0" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3A8C0" Ref="#PWR072"  Part="1" 
F 0 "#PWR072" H 4050 3550 50  0001 C CNN
F 1 "GND" H 4055 3627 50  0000 C CNN
F 2 "" H 4050 3800 50  0001 C CNN
F 3 "" H 4050 3800 50  0001 C CNN
	1    4050 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 2750 4050 2800
$Comp
L Components:RYM002N05T2CL Q?
U 1 1 5FA3A8CD
P 3750 2550
AR Path="/5FA3A8CD" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3A8CD" Ref="Q28"  Part="1" 
F 0 "Q28" H 4180 2696 50  0000 L CNN
F 1 "RU1J002YNTCL" H 4180 2605 50  0000 L CNN
F 2 "Components:RYM002N05T2CL" H 4200 2500 50  0001 L CNN
F 3 "https://www.rohm.com/datasheet/RYM002N05/rym002n05-e" H 4200 2400 50  0001 L CNN
F 4 "MOSFET .9V DRIVE N-Ch MOSFET" H 4200 2300 50  0001 L CNN "Description"
F 5 "0.55" H 4200 2200 50  0001 L CNN "Height"
F 6 "755-RYM002N05T2CL" H 4200 2100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ROHM-Semiconductor/RYM002N05T2CL?qs=IDm2WsMLa1UWVnvVQ8m33w%3D%3D" H 4200 2000 50  0001 L CNN "Mouser Price/Stock"
F 8 "ROHM Semiconductor" H 4200 1900 50  0001 L CNN "Manufacturer_Name"
F 9 "RYM002N05T2CL" H 4200 1800 50  0001 L CNN "Manufacturer_Part_Number"
	1    3750 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3A8D3
P 4050 2800
AR Path="/5FA3A8D3" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3A8D3" Ref="#PWR071"  Part="1" 
F 0 "#PWR071" H 4050 2550 50  0001 C CNN
F 1 "GND" H 4055 2627 50  0000 C CNN
F 2 "" H 4050 2800 50  0001 C CNN
F 3 "" H 4050 2800 50  0001 C CNN
	1    4050 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 1750 4050 1800
$Comp
L Components:RYM002N05T2CL Q?
U 1 1 5FA3A8E0
P 3750 1550
AR Path="/5FA3A8E0" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3A8E0" Ref="Q27"  Part="1" 
F 0 "Q27" H 4180 1696 50  0000 L CNN
F 1 "RU1J002YNTCL" H 4180 1605 50  0000 L CNN
F 2 "Components:RYM002N05T2CL" H 4200 1500 50  0001 L CNN
F 3 "https://www.rohm.com/datasheet/RYM002N05/rym002n05-e" H 4200 1400 50  0001 L CNN
F 4 "MOSFET .9V DRIVE N-Ch MOSFET" H 4200 1300 50  0001 L CNN "Description"
F 5 "0.55" H 4200 1200 50  0001 L CNN "Height"
F 6 "755-RYM002N05T2CL" H 4200 1100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ROHM-Semiconductor/RYM002N05T2CL?qs=IDm2WsMLa1UWVnvVQ8m33w%3D%3D" H 4200 1000 50  0001 L CNN "Mouser Price/Stock"
F 8 "ROHM Semiconductor" H 4200 900 50  0001 L CNN "Manufacturer_Name"
F 9 "RYM002N05T2CL" H 4200 800 50  0001 L CNN "Manufacturer_Part_Number"
	1    3750 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3A8E6
P 4050 1800
AR Path="/5FA3A8E6" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3A8E6" Ref="#PWR070"  Part="1" 
F 0 "#PWR070" H 4050 1550 50  0001 C CNN
F 1 "GND" H 4055 1627 50  0000 C CNN
F 2 "" H 4050 1800 50  0001 C CNN
F 3 "" H 4050 1800 50  0001 C CNN
	1    4050 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 8750 2050 8800
$Comp
L Components:RYM002N05T2CL Q?
U 1 1 5FA3A8F3
P 1750 8550
AR Path="/5FA3A8F3" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3A8F3" Ref="Q26"  Part="1" 
F 0 "Q26" H 2180 8696 50  0000 L CNN
F 1 "RU1J002YNTCL" H 2180 8605 50  0000 L CNN
F 2 "Components:RYM002N05T2CL" H 2200 8500 50  0001 L CNN
F 3 "https://www.rohm.com/datasheet/RYM002N05/rym002n05-e" H 2200 8400 50  0001 L CNN
F 4 "MOSFET .9V DRIVE N-Ch MOSFET" H 2200 8300 50  0001 L CNN "Description"
F 5 "0.55" H 2200 8200 50  0001 L CNN "Height"
F 6 "755-RYM002N05T2CL" H 2200 8100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ROHM-Semiconductor/RYM002N05T2CL?qs=IDm2WsMLa1UWVnvVQ8m33w%3D%3D" H 2200 8000 50  0001 L CNN "Mouser Price/Stock"
F 8 "ROHM Semiconductor" H 2200 7900 50  0001 L CNN "Manufacturer_Name"
F 9 "RYM002N05T2CL" H 2200 7800 50  0001 L CNN "Manufacturer_Part_Number"
	1    1750 8550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3A8F9
P 2050 8800
AR Path="/5FA3A8F9" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3A8F9" Ref="#PWR069"  Part="1" 
F 0 "#PWR069" H 2050 8550 50  0001 C CNN
F 1 "GND" H 2055 8627 50  0000 C CNN
F 2 "" H 2050 8800 50  0001 C CNN
F 3 "" H 2050 8800 50  0001 C CNN
	1    2050 8800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 7750 2050 7800
$Comp
L Components:RYM002N05T2CL Q?
U 1 1 5FA3A906
P 1750 7550
AR Path="/5FA3A906" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3A906" Ref="Q25"  Part="1" 
F 0 "Q25" H 2180 7696 50  0000 L CNN
F 1 "RU1J002YNTCL" H 2180 7605 50  0000 L CNN
F 2 "Components:RYM002N05T2CL" H 2200 7500 50  0001 L CNN
F 3 "https://www.rohm.com/datasheet/RYM002N05/rym002n05-e" H 2200 7400 50  0001 L CNN
F 4 "MOSFET .9V DRIVE N-Ch MOSFET" H 2200 7300 50  0001 L CNN "Description"
F 5 "0.55" H 2200 7200 50  0001 L CNN "Height"
F 6 "755-RYM002N05T2CL" H 2200 7100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ROHM-Semiconductor/RYM002N05T2CL?qs=IDm2WsMLa1UWVnvVQ8m33w%3D%3D" H 2200 7000 50  0001 L CNN "Mouser Price/Stock"
F 8 "ROHM Semiconductor" H 2200 6900 50  0001 L CNN "Manufacturer_Name"
F 9 "RYM002N05T2CL" H 2200 6800 50  0001 L CNN "Manufacturer_Part_Number"
	1    1750 7550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3A90C
P 2050 7800
AR Path="/5FA3A90C" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3A90C" Ref="#PWR068"  Part="1" 
F 0 "#PWR068" H 2050 7550 50  0001 C CNN
F 1 "GND" H 2055 7627 50  0000 C CNN
F 2 "" H 2050 7800 50  0001 C CNN
F 3 "" H 2050 7800 50  0001 C CNN
	1    2050 7800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 6750 2050 6800
$Comp
L Components:RYM002N05T2CL Q?
U 1 1 5FA3A919
P 1750 6550
AR Path="/5FA3A919" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3A919" Ref="Q24"  Part="1" 
F 0 "Q24" H 2180 6696 50  0000 L CNN
F 1 "RU1J002YNTCL" H 2180 6605 50  0000 L CNN
F 2 "Components:RYM002N05T2CL" H 2200 6500 50  0001 L CNN
F 3 "https://www.rohm.com/datasheet/RYM002N05/rym002n05-e" H 2200 6400 50  0001 L CNN
F 4 "MOSFET .9V DRIVE N-Ch MOSFET" H 2200 6300 50  0001 L CNN "Description"
F 5 "0.55" H 2200 6200 50  0001 L CNN "Height"
F 6 "755-RYM002N05T2CL" H 2200 6100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ROHM-Semiconductor/RYM002N05T2CL?qs=IDm2WsMLa1UWVnvVQ8m33w%3D%3D" H 2200 6000 50  0001 L CNN "Mouser Price/Stock"
F 8 "ROHM Semiconductor" H 2200 5900 50  0001 L CNN "Manufacturer_Name"
F 9 "RYM002N05T2CL" H 2200 5800 50  0001 L CNN "Manufacturer_Part_Number"
	1    1750 6550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3A91F
P 2050 6800
AR Path="/5FA3A91F" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3A91F" Ref="#PWR067"  Part="1" 
F 0 "#PWR067" H 2050 6550 50  0001 C CNN
F 1 "GND" H 2055 6627 50  0000 C CNN
F 2 "" H 2050 6800 50  0001 C CNN
F 3 "" H 2050 6800 50  0001 C CNN
	1    2050 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 5750 2050 5800
$Comp
L Components:RYM002N05T2CL Q?
U 1 1 5FA3A92C
P 1750 5550
AR Path="/5FA3A92C" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3A92C" Ref="Q23"  Part="1" 
F 0 "Q23" H 2180 5696 50  0000 L CNN
F 1 "RU1J002YNTCL" H 2180 5605 50  0000 L CNN
F 2 "Components:RYM002N05T2CL" H 2200 5500 50  0001 L CNN
F 3 "https://www.rohm.com/datasheet/RYM002N05/rym002n05-e" H 2200 5400 50  0001 L CNN
F 4 "MOSFET .9V DRIVE N-Ch MOSFET" H 2200 5300 50  0001 L CNN "Description"
F 5 "0.55" H 2200 5200 50  0001 L CNN "Height"
F 6 "755-RYM002N05T2CL" H 2200 5100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ROHM-Semiconductor/RYM002N05T2CL?qs=IDm2WsMLa1UWVnvVQ8m33w%3D%3D" H 2200 5000 50  0001 L CNN "Mouser Price/Stock"
F 8 "ROHM Semiconductor" H 2200 4900 50  0001 L CNN "Manufacturer_Name"
F 9 "RYM002N05T2CL" H 2200 4800 50  0001 L CNN "Manufacturer_Part_Number"
	1    1750 5550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3A932
P 2050 5800
AR Path="/5FA3A932" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3A932" Ref="#PWR066"  Part="1" 
F 0 "#PWR066" H 2050 5550 50  0001 C CNN
F 1 "GND" H 2055 5627 50  0000 C CNN
F 2 "" H 2050 5800 50  0001 C CNN
F 3 "" H 2050 5800 50  0001 C CNN
	1    2050 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 4750 2050 4800
$Comp
L Components:RYM002N05T2CL Q?
U 1 1 5FA3A93F
P 1750 4550
AR Path="/5FA3A93F" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3A93F" Ref="Q22"  Part="1" 
F 0 "Q22" H 2180 4696 50  0000 L CNN
F 1 "RU1J002YNTCL" H 2180 4605 50  0000 L CNN
F 2 "Components:RYM002N05T2CL" H 2200 4500 50  0001 L CNN
F 3 "https://www.rohm.com/datasheet/RYM002N05/rym002n05-e" H 2200 4400 50  0001 L CNN
F 4 "MOSFET .9V DRIVE N-Ch MOSFET" H 2200 4300 50  0001 L CNN "Description"
F 5 "0.55" H 2200 4200 50  0001 L CNN "Height"
F 6 "755-RYM002N05T2CL" H 2200 4100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ROHM-Semiconductor/RYM002N05T2CL?qs=IDm2WsMLa1UWVnvVQ8m33w%3D%3D" H 2200 4000 50  0001 L CNN "Mouser Price/Stock"
F 8 "ROHM Semiconductor" H 2200 3900 50  0001 L CNN "Manufacturer_Name"
F 9 "RYM002N05T2CL" H 2200 3800 50  0001 L CNN "Manufacturer_Part_Number"
	1    1750 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3A945
P 2050 4800
AR Path="/5FA3A945" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3A945" Ref="#PWR065"  Part="1" 
F 0 "#PWR065" H 2050 4550 50  0001 C CNN
F 1 "GND" H 2055 4627 50  0000 C CNN
F 2 "" H 2050 4800 50  0001 C CNN
F 3 "" H 2050 4800 50  0001 C CNN
	1    2050 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 3750 2050 3800
$Comp
L Components:RYM002N05T2CL Q?
U 1 1 5FA3A952
P 1750 3550
AR Path="/5FA3A952" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3A952" Ref="Q21"  Part="1" 
F 0 "Q21" H 2180 3696 50  0000 L CNN
F 1 "RU1J002YNTCL" H 2180 3605 50  0000 L CNN
F 2 "Components:RYM002N05T2CL" H 2200 3500 50  0001 L CNN
F 3 "https://www.rohm.com/datasheet/RYM002N05/rym002n05-e" H 2200 3400 50  0001 L CNN
F 4 "MOSFET .9V DRIVE N-Ch MOSFET" H 2200 3300 50  0001 L CNN "Description"
F 5 "0.55" H 2200 3200 50  0001 L CNN "Height"
F 6 "755-RYM002N05T2CL" H 2200 3100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ROHM-Semiconductor/RYM002N05T2CL?qs=IDm2WsMLa1UWVnvVQ8m33w%3D%3D" H 2200 3000 50  0001 L CNN "Mouser Price/Stock"
F 8 "ROHM Semiconductor" H 2200 2900 50  0001 L CNN "Manufacturer_Name"
F 9 "RYM002N05T2CL" H 2200 2800 50  0001 L CNN "Manufacturer_Part_Number"
	1    1750 3550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3A958
P 2050 3800
AR Path="/5FA3A958" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3A958" Ref="#PWR064"  Part="1" 
F 0 "#PWR064" H 2050 3550 50  0001 C CNN
F 1 "GND" H 2055 3627 50  0000 C CNN
F 2 "" H 2050 3800 50  0001 C CNN
F 3 "" H 2050 3800 50  0001 C CNN
	1    2050 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 2750 2050 2800
$Comp
L Components:RYM002N05T2CL Q?
U 1 1 5FA3A965
P 1750 2550
AR Path="/5FA3A965" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3A965" Ref="Q20"  Part="1" 
F 0 "Q20" H 2180 2696 50  0000 L CNN
F 1 "RU1J002YNTCL" H 2180 2605 50  0000 L CNN
F 2 "Components:RYM002N05T2CL" H 2200 2500 50  0001 L CNN
F 3 "https://www.rohm.com/datasheet/RYM002N05/rym002n05-e" H 2200 2400 50  0001 L CNN
F 4 "MOSFET .9V DRIVE N-Ch MOSFET" H 2200 2300 50  0001 L CNN "Description"
F 5 "0.55" H 2200 2200 50  0001 L CNN "Height"
F 6 "755-RYM002N05T2CL" H 2200 2100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ROHM-Semiconductor/RYM002N05T2CL?qs=IDm2WsMLa1UWVnvVQ8m33w%3D%3D" H 2200 2000 50  0001 L CNN "Mouser Price/Stock"
F 8 "ROHM Semiconductor" H 2200 1900 50  0001 L CNN "Manufacturer_Name"
F 9 "RYM002N05T2CL" H 2200 1800 50  0001 L CNN "Manufacturer_Part_Number"
	1    1750 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3A96B
P 2050 2800
AR Path="/5FA3A96B" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3A96B" Ref="#PWR063"  Part="1" 
F 0 "#PWR063" H 2050 2550 50  0001 C CNN
F 1 "GND" H 2055 2627 50  0000 C CNN
F 2 "" H 2050 2800 50  0001 C CNN
F 3 "" H 2050 2800 50  0001 C CNN
	1    2050 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5FA3A971
P 6250 1550
AR Path="/5FA3A971" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3A971" Ref="R39"  Part="1" 
F 0 "R39" H 6320 1596 50  0000 L CNN
F 1 "10k" H 6320 1505 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6180 1550 50  0001 C CNN
F 3 "~" H 6250 1550 50  0001 C CNN
	1    6250 1550
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 1550 6450 1550
Text Label 2050 1150 0    50   ~ 0
FLOW_VALVE_1_FWD+
Wire Wire Line
	2050 1750 2050 1800
$Comp
L Components:RYM002N05T2CL Q?
U 1 1 5FA3A987
P 1750 1550
AR Path="/5FA3A987" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3A987" Ref="Q19"  Part="1" 
F 0 "Q19" H 2180 1696 50  0000 L CNN
F 1 "RU1J002YNTCL" H 2180 1605 50  0000 L CNN
F 2 "Components:RYM002N05T2CL" H 2200 1500 50  0001 L CNN
F 3 "https://www.rohm.com/datasheet/RYM002N05/rym002n05-e" H 2200 1400 50  0001 L CNN
F 4 "MOSFET .9V DRIVE N-Ch MOSFET" H 2200 1300 50  0001 L CNN "Description"
F 5 "0.55" H 2200 1200 50  0001 L CNN "Height"
F 6 "755-RYM002N05T2CL" H 2200 1100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ROHM-Semiconductor/RYM002N05T2CL?qs=IDm2WsMLa1UWVnvVQ8m33w%3D%3D" H 2200 1000 50  0001 L CNN "Mouser Price/Stock"
F 8 "ROHM Semiconductor" H 2200 900 50  0001 L CNN "Manufacturer_Name"
F 9 "RYM002N05T2CL" H 2200 800 50  0001 L CNN "Manufacturer_Part_Number"
	1    1750 1550
	1    0    0    -1  
$EndComp
Wire Notes Line
	8600 5850 14300 5850
Wire Notes Line
	14300 4500 14300 5850
Wire Notes Line
	8600 4500 14300 4500
Wire Notes Line
	8600 5850 8600 4500
Wire Wire Line
	13800 4800 13850 4800
Wire Wire Line
	13800 4900 13800 4800
Wire Wire Line
	14200 4800 14150 4800
Wire Wire Line
	14200 5200 14200 4800
Text Label 14200 5200 3    50   ~ 0
FLOW_VALVE_8-
Text Label 13800 5200 3    50   ~ 0
FLOW_VALVE_8+
$Comp
L Device:C C?
U 1 1 5FA3A997
P 14000 4800
AR Path="/5FA3A997" Ref="C?"  Part="1" 
AR Path="/5F9F1786/5FA3A997" Ref="C16"  Part="1" 
F 0 "C16" H 14115 4846 50  0000 L CNN
F 1 "1u" H 14115 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 14038 4650 50  0001 C CNN
F 3 "~" H 14000 4800 50  0001 C CNN
	1    14000 4800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FA3A99D
P 13800 5050
AR Path="/5FA3A99D" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3A99D" Ref="R77"  Part="1" 
F 0 "R77" V 13593 5050 50  0000 C CNN
F 1 "1" V 13684 5050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 13730 5050 50  0001 C CNN
F 3 "~" H 13800 5050 50  0001 C CNN
	1    13800 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	13100 4800 13150 4800
Wire Wire Line
	13100 4900 13100 4800
Wire Wire Line
	13500 4800 13450 4800
Wire Wire Line
	13500 5200 13500 4800
Text Label 13500 5200 3    50   ~ 0
FLOW_VALVE_7-
Text Label 13100 5200 3    50   ~ 0
FLOW_VALVE_7+
$Comp
L Device:C C?
U 1 1 5FA3A9A9
P 13300 4800
AR Path="/5FA3A9A9" Ref="C?"  Part="1" 
AR Path="/5F9F1786/5FA3A9A9" Ref="C15"  Part="1" 
F 0 "C15" H 13415 4846 50  0000 L CNN
F 1 "1u" H 13415 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 13338 4650 50  0001 C CNN
F 3 "~" H 13300 4800 50  0001 C CNN
	1    13300 4800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FA3A9AF
P 13100 5050
AR Path="/5FA3A9AF" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3A9AF" Ref="R76"  Part="1" 
F 0 "R76" V 12893 5050 50  0000 C CNN
F 1 "1" V 12984 5050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 13030 5050 50  0001 C CNN
F 3 "~" H 13100 5050 50  0001 C CNN
	1    13100 5050
	1    0    0    -1  
$EndComp
Text Label 2050 6150 0    50   ~ 0
FLOW_VALVE_3_REV+
Wire Wire Line
	6400 4550 6450 4550
$Comp
L Device:R R?
U 1 1 5FA3A9BD
P 6250 4550
AR Path="/5FA3A9BD" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3A9BD" Ref="R49"  Part="1" 
F 0 "R49" H 6320 4596 50  0000 L CNN
F 1 "10k" H 6320 4505 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6180 4550 50  0001 C CNN
F 3 "~" H 6250 4550 50  0001 C CNN
	1    6250 4550
	0    1    1    0   
$EndComp
Text Label 2050 5150 0    50   ~ 0
FLOW_VALVE_3_FWD+
$Comp
L Device:R R?
U 1 1 5FA3A9F3
P 6250 4250
AR Path="/5FA3A9F3" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3A9F3" Ref="R48"  Part="1" 
F 0 "R48" H 6320 4296 50  0000 L CNN
F 1 "10k" H 6320 4205 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6180 4250 50  0001 C CNN
F 3 "~" H 6250 4250 50  0001 C CNN
	1    6250 4250
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 4250 6450 4250
$Comp
L Device:R R?
U 1 1 5FA3AA00
P 6250 5750
AR Path="/5FA3AA00" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3AA00" Ref="R53"  Part="1" 
F 0 "R53" H 6320 5796 50  0000 L CNN
F 1 "10k" H 6320 5705 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6180 5750 50  0001 C CNN
F 3 "~" H 6250 5750 50  0001 C CNN
	1    6250 5750
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 5750 6450 5750
Text Label 2050 8150 0    50   ~ 0
FLOW_VALVE_4_REV+
Text Label 4050 5150 0    50   ~ 0
FLOW_VALVE_7_FWD+
$Comp
L Device:R R?
U 1 1 5FA3AA0F
P 6250 3950
AR Path="/5FA3AA0F" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3AA0F" Ref="R47"  Part="1" 
F 0 "R47" H 6320 3996 50  0000 L CNN
F 1 "10k" H 6320 3905 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6180 3950 50  0001 C CNN
F 3 "~" H 6250 3950 50  0001 C CNN
	1    6250 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 3950 6450 3950
$Comp
L Device:R R?
U 1 1 5FA3AA1C
P 6250 4850
AR Path="/5FA3AA1C" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3AA1C" Ref="R50"  Part="1" 
F 0 "R50" H 6320 4896 50  0000 L CNN
F 1 "10k" H 6320 4805 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6180 4850 50  0001 C CNN
F 3 "~" H 6250 4850 50  0001 C CNN
	1    6250 4850
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 4850 6450 4850
Text Label 4050 6150 0    50   ~ 0
FLOW_VALVE_7_REV+
Text Label 4050 7150 0    50   ~ 0
FLOW_VALVE_8_FWD+
$Comp
L Device:R R?
U 1 1 5FA3AA2B
P 6250 5450
AR Path="/5FA3AA2B" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3AA2B" Ref="R52"  Part="1" 
F 0 "R52" H 6320 5496 50  0000 L CNN
F 1 "10k" H 6320 5405 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6180 5450 50  0001 C CNN
F 3 "~" H 6250 5450 50  0001 C CNN
	1    6250 5450
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 5450 6450 5450
Text Label 4050 8150 0    50   ~ 0
FLOW_VALVE_8_REV+
$Comp
L power:GND #PWR?
U 1 1 5FA3AA39
P 2050 1800
AR Path="/5FA3AA39" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AA39" Ref="#PWR062"  Part="1" 
F 0 "#PWR062" H 2050 1550 50  0001 C CNN
F 1 "GND" H 2055 1627 50  0000 C CNN
F 2 "" H 2050 1800 50  0001 C CNN
F 3 "" H 2050 1800 50  0001 C CNN
	1    2050 1800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5FA3AA3F
P 6250 1250
AR Path="/5FA3AA3F" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3AA3F" Ref="R38"  Part="1" 
F 0 "R38" H 6320 1296 50  0000 L CNN
F 1 "10k" H 6320 1205 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6180 1250 50  0001 C CNN
F 3 "~" H 6250 1250 50  0001 C CNN
	1    6250 1250
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 1250 6450 1250
$Comp
L Device:R R?
U 1 1 5FA3AA4C
P 6250 2450
AR Path="/5FA3AA4C" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3AA4C" Ref="R42"  Part="1" 
F 0 "R42" H 6320 2496 50  0000 L CNN
F 1 "10k" H 6320 2405 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6180 2450 50  0001 C CNN
F 3 "~" H 6250 2450 50  0001 C CNN
	1    6250 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 2450 6450 2450
Text Label 2050 2150 0    50   ~ 0
FLOW_VALVE_1_REV+
Text Label 2050 3150 0    50   ~ 0
FLOW_VALVE_2_FWD+
$Comp
L Device:R R?
U 1 1 5FA3AA5B
P 6250 3050
AR Path="/5FA3AA5B" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3AA5B" Ref="R44"  Part="1" 
F 0 "R44" H 6320 3096 50  0000 L CNN
F 1 "10k" H 6320 3005 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6180 3050 50  0001 C CNN
F 3 "~" H 6250 3050 50  0001 C CNN
	1    6250 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 3050 6450 3050
$Comp
L Device:R R?
U 1 1 5FA3AA68
P 6250 3650
AR Path="/5FA3AA68" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3AA68" Ref="R46"  Part="1" 
F 0 "R46" H 6320 3696 50  0000 L CNN
F 1 "10k" H 6320 3605 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6180 3650 50  0001 C CNN
F 3 "~" H 6250 3650 50  0001 C CNN
	1    6250 3650
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 3650 6450 3650
Text Label 2050 4150 0    50   ~ 0
FLOW_VALVE_2_REV+
Text Label 4050 1150 0    50   ~ 0
FLOW_VALVE_5_FWD+
$Comp
L Device:R R?
U 1 1 5FA3AA77
P 6250 1850
AR Path="/5FA3AA77" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3AA77" Ref="R40"  Part="1" 
F 0 "R40" H 6320 1896 50  0000 L CNN
F 1 "10k" H 6320 1805 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6180 1850 50  0001 C CNN
F 3 "~" H 6250 1850 50  0001 C CNN
	1    6250 1850
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 1850 6450 1850
$Comp
L Device:R R?
U 1 1 5FA3AA84
P 6250 2150
AR Path="/5FA3AA84" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3AA84" Ref="R41"  Part="1" 
F 0 "R41" H 6320 2196 50  0000 L CNN
F 1 "10k" H 6320 2105 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6180 2150 50  0001 C CNN
F 3 "~" H 6250 2150 50  0001 C CNN
	1    6250 2150
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 2150 6450 2150
Text Label 4050 2150 0    50   ~ 0
FLOW_VALVE_5_REV+
Text Label 4050 3150 0    50   ~ 0
FLOW_VALVE_6_FWD+
$Comp
L Device:R R?
U 1 1 5FA3AA93
P 6250 2750
AR Path="/5FA3AA93" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3AA93" Ref="R43"  Part="1" 
F 0 "R43" H 6320 2796 50  0000 L CNN
F 1 "10k" H 6320 2705 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6180 2750 50  0001 C CNN
F 3 "~" H 6250 2750 50  0001 C CNN
	1    6250 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 2750 6450 2750
$Comp
L Device:R R?
U 1 1 5FA3AAA0
P 6250 3350
AR Path="/5FA3AAA0" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3AAA0" Ref="R45"  Part="1" 
F 0 "R45" H 6320 3396 50  0000 L CNN
F 1 "10k" H 6320 3305 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6180 3350 50  0001 C CNN
F 3 "~" H 6250 3350 50  0001 C CNN
	1    6250 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 3350 6450 3350
Text Label 4050 4150 0    50   ~ 0
FLOW_VALVE_6_REV+
Text Notes 11600 950  0    50   ~ 0
Flow Valves Driving
$Comp
L power:GND #PWR?
U 1 1 5FA3AAAF
P 9600 2350
AR Path="/5FA3AAAF" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AAAF" Ref="#PWR095"  Part="1" 
F 0 "#PWR095" H 9600 2100 50  0001 C CNN
F 1 "GND" H 9605 2177 50  0000 C CNN
F 2 "" H 9600 2350 50  0001 C CNN
F 3 "" H 9600 2350 50  0001 C CNN
	1    9600 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	10950 2100 11100 2100
Wire Wire Line
	9600 2100 9600 2350
Wire Wire Line
	9750 2100 9600 2100
Wire Wire Line
	10950 2200 11100 2200
Text Label 11100 2200 0    50   ~ 0
FLOW_VALVE_2-
Text Label 9750 2300 2    50   ~ 0
FLOW_VALVE_2_FWD+
Text Label 10950 2000 0    50   ~ 0
FLOW_VALVE_2+
Text Label 10950 2300 0    50   ~ 0
FLOW_VALVE_2_REV+
$Comp
L power:GND #PWR?
U 1 1 5FA3AAD1
P 9600 3200
AR Path="/5FA3AAD1" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AAD1" Ref="#PWR096"  Part="1" 
F 0 "#PWR096" H 9600 2950 50  0001 C CNN
F 1 "GND" H 9605 3027 50  0000 C CNN
F 2 "" H 9600 3200 50  0001 C CNN
F 3 "" H 9600 3200 50  0001 C CNN
	1    9600 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	10950 2900 11100 2900
$Comp
L Components:ZXMHC3A01T8TA Q?
U 1 1 5FA3AAE7
P 9750 2800
AR Path="/5FA3AAE7" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3AAE7" Ref="Q37"  Part="1" 
F 0 "Q37" H 10450 3065 50  0000 C CNN
F 1 "ZXMHC3A01T8TA" H 10450 2974 50  0000 C CNN
F 2 "Components:SOT153P700X170-8N" H 11000 2900 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ZXMHC3A01T8TA.pdf" H 11000 2800 50  0001 L CNN
F 4 "DIODES INC. - ZXMHC3A01T8TA - Dual MOSFET, Enhancement Mode, N and P Channel, 3.1 A, 30 V, 0.12 ohm, 10 V, 1 V" H 11000 2700 50  0001 L CNN "Description"
F 5 "1.7" H 11000 2600 50  0001 L CNN "Height"
F 6 "522-ZXMHC3A01T8TA" H 11000 2500 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Diodes-Incorporated/ZXMHC3A01T8TA?qs=Xl4UVSSad%2Fqyu39NiNDXJg%3D%3D" H 11000 2400 50  0001 L CNN "Mouser Price/Stock"
F 8 "Diodes Inc." H 11000 2300 50  0001 L CNN "Manufacturer_Name"
F 9 "ZXMHC3A01T8TA" H 11000 2200 50  0001 L CNN "Manufacturer_Part_Number"
	1    9750 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 2900 9600 2900
Wire Wire Line
	10950 3000 11100 3000
Text Label 11100 3000 0    50   ~ 0
FLOW_VALVE_3-
Text Label 9750 3100 2    50   ~ 0
FLOW_VALVE_3_FWD+
Text Label 10950 2800 0    50   ~ 0
FLOW_VALVE_3+
Text Label 10950 3100 0    50   ~ 0
FLOW_VALVE_3_REV+
$Comp
L power:GND #PWR?
U 1 1 5FA3AAF3
P 9600 3850
AR Path="/5FA3AAF3" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AAF3" Ref="#PWR097"  Part="1" 
F 0 "#PWR097" H 9600 3600 50  0001 C CNN
F 1 "GND" H 9605 3677 50  0000 C CNN
F 2 "" H 9600 3850 50  0001 C CNN
F 3 "" H 9600 3850 50  0001 C CNN
	1    9600 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10950 3650 11100 3650
Wire Wire Line
	9750 3650 9600 3650
Wire Wire Line
	10950 3750 11100 3750
Text Label 11100 3750 0    50   ~ 0
FLOW_VALVE_4-
Text Label 9750 3850 2    50   ~ 0
FLOW_VALVE_4_FWD+
Text Label 10950 3550 0    50   ~ 0
FLOW_VALVE_4+
Text Label 10950 3850 0    50   ~ 0
FLOW_VALVE_4_REV+
$Comp
L power:GND #PWR?
U 1 1 5FA3AB15
P 13000 1600
AR Path="/5FA3AB15" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AB15" Ref="#PWR098"  Part="1" 
F 0 "#PWR098" H 13000 1350 50  0001 C CNN
F 1 "GND" H 13005 1427 50  0000 C CNN
F 2 "" H 13000 1600 50  0001 C CNN
F 3 "" H 13000 1600 50  0001 C CNN
	1    13000 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	14350 1400 14500 1400
$Comp
L Components:ZXMHC3A01T8TA Q?
U 1 1 5FA3AB2B
P 13150 1300
AR Path="/5FA3AB2B" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3AB2B" Ref="Q39"  Part="1" 
F 0 "Q39" H 13850 1565 50  0000 C CNN
F 1 "ZXMHC3A01T8TA" H 13850 1474 50  0000 C CNN
F 2 "Components:SOT153P700X170-8N" H 14400 1400 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ZXMHC3A01T8TA.pdf" H 14400 1300 50  0001 L CNN
F 4 "DIODES INC. - ZXMHC3A01T8TA - Dual MOSFET, Enhancement Mode, N and P Channel, 3.1 A, 30 V, 0.12 ohm, 10 V, 1 V" H 14400 1200 50  0001 L CNN "Description"
F 5 "1.7" H 14400 1100 50  0001 L CNN "Height"
F 6 "522-ZXMHC3A01T8TA" H 14400 1000 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Diodes-Incorporated/ZXMHC3A01T8TA?qs=Xl4UVSSad%2Fqyu39NiNDXJg%3D%3D" H 14400 900 50  0001 L CNN "Mouser Price/Stock"
F 8 "Diodes Inc." H 14400 800 50  0001 L CNN "Manufacturer_Name"
F 9 "ZXMHC3A01T8TA" H 14400 700 50  0001 L CNN "Manufacturer_Part_Number"
	1    13150 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	13150 1400 13000 1400
Wire Wire Line
	14350 1500 14500 1500
Text Label 14500 1500 0    50   ~ 0
FLOW_VALVE_5-
Text Label 13150 1600 2    50   ~ 0
FLOW_VALVE_5_FWD+
Text Label 14350 1300 0    50   ~ 0
FLOW_VALVE_5+
Text Label 14350 1600 0    50   ~ 0
FLOW_VALVE_5_REV+
$Comp
L power:GND #PWR?
U 1 1 5FA3AB37
P 13000 2350
AR Path="/5FA3AB37" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AB37" Ref="#PWR099"  Part="1" 
F 0 "#PWR099" H 13000 2100 50  0001 C CNN
F 1 "GND" H 13005 2177 50  0000 C CNN
F 2 "" H 13000 2350 50  0001 C CNN
F 3 "" H 13000 2350 50  0001 C CNN
	1    13000 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	14350 2150 14500 2150
Wire Wire Line
	13000 2150 13000 2350
$Comp
L Components:ZXMHC3A01T8TA Q?
U 1 1 5FA3AB4D
P 13150 2050
AR Path="/5FA3AB4D" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3AB4D" Ref="Q40"  Part="1" 
F 0 "Q40" H 13850 2315 50  0000 C CNN
F 1 "ZXMHC3A01T8TA" H 13850 2224 50  0000 C CNN
F 2 "Components:SOT153P700X170-8N" H 14400 2150 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ZXMHC3A01T8TA.pdf" H 14400 2050 50  0001 L CNN
F 4 "DIODES INC. - ZXMHC3A01T8TA - Dual MOSFET, Enhancement Mode, N and P Channel, 3.1 A, 30 V, 0.12 ohm, 10 V, 1 V" H 14400 1950 50  0001 L CNN "Description"
F 5 "1.7" H 14400 1850 50  0001 L CNN "Height"
F 6 "522-ZXMHC3A01T8TA" H 14400 1750 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Diodes-Incorporated/ZXMHC3A01T8TA?qs=Xl4UVSSad%2Fqyu39NiNDXJg%3D%3D" H 14400 1650 50  0001 L CNN "Mouser Price/Stock"
F 8 "Diodes Inc." H 14400 1550 50  0001 L CNN "Manufacturer_Name"
F 9 "ZXMHC3A01T8TA" H 14400 1450 50  0001 L CNN "Manufacturer_Part_Number"
	1    13150 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	13150 2150 13000 2150
Wire Wire Line
	14350 2250 14500 2250
Text Label 14500 2250 0    50   ~ 0
FLOW_VALVE_6-
Text Label 13150 2350 2    50   ~ 0
FLOW_VALVE_6_FWD+
Text Label 14350 2050 0    50   ~ 0
FLOW_VALVE_6+
$Comp
L power:GND #PWR?
U 1 1 5FA3AB58
P 13000 3100
AR Path="/5FA3AB58" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AB58" Ref="#PWR0100"  Part="1" 
F 0 "#PWR0100" H 13000 2850 50  0001 C CNN
F 1 "GND" H 13005 2927 50  0000 C CNN
F 2 "" H 13000 3100 50  0001 C CNN
F 3 "" H 13000 3100 50  0001 C CNN
	1    13000 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	14350 2900 14500 2900
$Comp
L Components:ZXMHC3A01T8TA Q?
U 1 1 5FA3AB6E
P 13150 2800
AR Path="/5FA3AB6E" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3AB6E" Ref="Q41"  Part="1" 
F 0 "Q41" H 13850 3065 50  0000 C CNN
F 1 "ZXMHC3A01T8TA" H 13850 2974 50  0000 C CNN
F 2 "Components:SOT153P700X170-8N" H 14400 2900 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ZXMHC3A01T8TA.pdf" H 14400 2800 50  0001 L CNN
F 4 "DIODES INC. - ZXMHC3A01T8TA - Dual MOSFET, Enhancement Mode, N and P Channel, 3.1 A, 30 V, 0.12 ohm, 10 V, 1 V" H 14400 2700 50  0001 L CNN "Description"
F 5 "1.7" H 14400 2600 50  0001 L CNN "Height"
F 6 "522-ZXMHC3A01T8TA" H 14400 2500 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Diodes-Incorporated/ZXMHC3A01T8TA?qs=Xl4UVSSad%2Fqyu39NiNDXJg%3D%3D" H 14400 2400 50  0001 L CNN "Mouser Price/Stock"
F 8 "Diodes Inc." H 14400 2300 50  0001 L CNN "Manufacturer_Name"
F 9 "ZXMHC3A01T8TA" H 14400 2200 50  0001 L CNN "Manufacturer_Part_Number"
	1    13150 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	13150 2900 13000 2900
Wire Wire Line
	14350 3000 14500 3000
Text Label 14500 3000 0    50   ~ 0
FLOW_VALVE_7-
Text Label 14350 2800 0    50   ~ 0
FLOW_VALVE_7+
Text Label 13150 3100 2    50   ~ 0
FLOW_VALVE_7_FWD+
Text Label 14350 3100 0    50   ~ 0
FLOW_VALVE_7_REV+
$Comp
L power:GND #PWR?
U 1 1 5FA3AB7A
P 13000 3850
AR Path="/5FA3AB7A" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AB7A" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 13000 3600 50  0001 C CNN
F 1 "GND" H 13005 3677 50  0000 C CNN
F 2 "" H 13000 3850 50  0001 C CNN
F 3 "" H 13000 3850 50  0001 C CNN
	1    13000 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	14350 3650 14500 3650
Wire Wire Line
	13000 3650 13000 3850
$Comp
L Components:ZXMHC3A01T8TA Q?
U 1 1 5FA3AB90
P 13150 3550
AR Path="/5FA3AB90" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3AB90" Ref="Q42"  Part="1" 
F 0 "Q42" H 13850 3815 50  0000 C CNN
F 1 "ZXMHC3A01T8TA" H 13850 3724 50  0000 C CNN
F 2 "Components:SOT153P700X170-8N" H 14400 3650 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ZXMHC3A01T8TA.pdf" H 14400 3550 50  0001 L CNN
F 4 "DIODES INC. - ZXMHC3A01T8TA - Dual MOSFET, Enhancement Mode, N and P Channel, 3.1 A, 30 V, 0.12 ohm, 10 V, 1 V" H 14400 3450 50  0001 L CNN "Description"
F 5 "1.7" H 14400 3350 50  0001 L CNN "Height"
F 6 "522-ZXMHC3A01T8TA" H 14400 3250 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Diodes-Incorporated/ZXMHC3A01T8TA?qs=Xl4UVSSad%2Fqyu39NiNDXJg%3D%3D" H 14400 3150 50  0001 L CNN "Mouser Price/Stock"
F 8 "Diodes Inc." H 14400 3050 50  0001 L CNN "Manufacturer_Name"
F 9 "ZXMHC3A01T8TA" H 14400 2950 50  0001 L CNN "Manufacturer_Part_Number"
	1    13150 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	13150 3650 13000 3650
Wire Wire Line
	14350 3750 14500 3750
Text Label 14500 3750 0    50   ~ 0
FLOW_VALVE_8-
Text Label 14350 3550 0    50   ~ 0
FLOW_VALVE_8+
Text Label 13150 3850 2    50   ~ 0
FLOW_VALVE_8_FWD+
Text Label 14350 3850 0    50   ~ 0
FLOW_VALVE_8_REV+
Text Label 14350 2350 0    50   ~ 0
FLOW_VALVE_6_REV+
Wire Notes Line
	15500 950  15500 4250
Wire Notes Line
	15500 4250 8600 4250
Wire Notes Line
	8600 4250 8600 950 
Wire Notes Line
	8600 950  15500 950 
$Comp
L Device:R R?
U 1 1 5FA3ABA1
P 7850 1200
AR Path="/5FA3ABA1" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ABA1" Ref="R54"  Part="1" 
F 0 "R54" H 7920 1246 50  0000 L CNN
F 1 "10k" H 7920 1155 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7780 1200 50  0001 C CNN
F 3 "~" H 7850 1200 50  0001 C CNN
	1    7850 1200
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 1200 8000 1200
$Comp
L Device:R R?
U 1 1 5FA3ABA8
P 7850 1500
AR Path="/5FA3ABA8" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ABA8" Ref="R55"  Part="1" 
F 0 "R55" H 7920 1546 50  0000 L CNN
F 1 "10k" H 7920 1455 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7780 1500 50  0001 C CNN
F 3 "~" H 7850 1500 50  0001 C CNN
	1    7850 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 1500 8000 1500
$Comp
L Device:R R?
U 1 1 5FA3ABAF
P 7850 1800
AR Path="/5FA3ABAF" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ABAF" Ref="R56"  Part="1" 
F 0 "R56" H 7920 1846 50  0000 L CNN
F 1 "10k" H 7920 1755 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7780 1800 50  0001 C CNN
F 3 "~" H 7850 1800 50  0001 C CNN
	1    7850 1800
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 1800 8000 1800
$Comp
L Device:R R?
U 1 1 5FA3ABB6
P 7850 2100
AR Path="/5FA3ABB6" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ABB6" Ref="R57"  Part="1" 
F 0 "R57" H 7920 2146 50  0000 L CNN
F 1 "10k" H 7920 2055 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7780 2100 50  0001 C CNN
F 3 "~" H 7850 2100 50  0001 C CNN
	1    7850 2100
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 2100 8000 2100
$Comp
L Device:R R?
U 1 1 5FA3ABBD
P 7850 2400
AR Path="/5FA3ABBD" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ABBD" Ref="R58"  Part="1" 
F 0 "R58" H 7920 2446 50  0000 L CNN
F 1 "10k" H 7920 2355 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7780 2400 50  0001 C CNN
F 3 "~" H 7850 2400 50  0001 C CNN
	1    7850 2400
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 2400 8000 2400
$Comp
L Device:R R?
U 1 1 5FA3ABC4
P 7850 2700
AR Path="/5FA3ABC4" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ABC4" Ref="R59"  Part="1" 
F 0 "R59" H 7920 2746 50  0000 L CNN
F 1 "10k" H 7920 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7780 2700 50  0001 C CNN
F 3 "~" H 7850 2700 50  0001 C CNN
	1    7850 2700
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 2700 8000 2700
$Comp
L Device:R R?
U 1 1 5FA3ABCB
P 7850 3000
AR Path="/5FA3ABCB" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ABCB" Ref="R60"  Part="1" 
F 0 "R60" H 7920 3046 50  0000 L CNN
F 1 "10k" H 7920 2955 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7780 3000 50  0001 C CNN
F 3 "~" H 7850 3000 50  0001 C CNN
	1    7850 3000
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 3000 8000 3000
$Comp
L Device:R R?
U 1 1 5FA3ABD2
P 7850 3300
AR Path="/5FA3ABD2" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ABD2" Ref="R61"  Part="1" 
F 0 "R61" H 7920 3346 50  0000 L CNN
F 1 "10k" H 7920 3255 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7780 3300 50  0001 C CNN
F 3 "~" H 7850 3300 50  0001 C CNN
	1    7850 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 3300 8000 3300
$Comp
L Device:R R?
U 1 1 5FA3ABD9
P 7850 3600
AR Path="/5FA3ABD9" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ABD9" Ref="R62"  Part="1" 
F 0 "R62" H 7920 3646 50  0000 L CNN
F 1 "10k" H 7920 3555 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7780 3600 50  0001 C CNN
F 3 "~" H 7850 3600 50  0001 C CNN
	1    7850 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 3600 8000 3600
$Comp
L Device:R R?
U 1 1 5FA3ABE0
P 7850 3900
AR Path="/5FA3ABE0" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ABE0" Ref="R63"  Part="1" 
F 0 "R63" H 7920 3946 50  0000 L CNN
F 1 "10k" H 7920 3855 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7780 3900 50  0001 C CNN
F 3 "~" H 7850 3900 50  0001 C CNN
	1    7850 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 3900 8000 3900
$Comp
L Device:R R?
U 1 1 5FA3ABE7
P 7850 4200
AR Path="/5FA3ABE7" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ABE7" Ref="R64"  Part="1" 
F 0 "R64" H 7920 4246 50  0000 L CNN
F 1 "10k" H 7920 4155 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7780 4200 50  0001 C CNN
F 3 "~" H 7850 4200 50  0001 C CNN
	1    7850 4200
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 4200 8000 4200
$Comp
L Device:R R?
U 1 1 5FA3ABEE
P 7850 4500
AR Path="/5FA3ABEE" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ABEE" Ref="R65"  Part="1" 
F 0 "R65" H 7920 4546 50  0000 L CNN
F 1 "10k" H 7920 4455 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7780 4500 50  0001 C CNN
F 3 "~" H 7850 4500 50  0001 C CNN
	1    7850 4500
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 4500 8000 4500
$Comp
L Device:R R?
U 1 1 5FA3ABF5
P 7850 4800
AR Path="/5FA3ABF5" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ABF5" Ref="R66"  Part="1" 
F 0 "R66" H 7920 4846 50  0000 L CNN
F 1 "10k" H 7920 4755 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7780 4800 50  0001 C CNN
F 3 "~" H 7850 4800 50  0001 C CNN
	1    7850 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 4800 8000 4800
$Comp
L Device:R R?
U 1 1 5FA3ABFC
P 7850 5100
AR Path="/5FA3ABFC" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ABFC" Ref="R67"  Part="1" 
F 0 "R67" H 7920 5146 50  0000 L CNN
F 1 "10k" H 7920 5055 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7780 5100 50  0001 C CNN
F 3 "~" H 7850 5100 50  0001 C CNN
	1    7850 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 5100 8000 5100
$Comp
L Device:R R?
U 1 1 5FA3AC03
P 7850 5400
AR Path="/5FA3AC03" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3AC03" Ref="R68"  Part="1" 
F 0 "R68" H 7920 5446 50  0000 L CNN
F 1 "10k" H 7920 5355 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7780 5400 50  0001 C CNN
F 3 "~" H 7850 5400 50  0001 C CNN
	1    7850 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 5400 8000 5400
$Comp
L Device:R R?
U 1 1 5FA3AC0A
P 7850 5700
AR Path="/5FA3AC0A" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3AC0A" Ref="R69"  Part="1" 
F 0 "R69" H 7920 5746 50  0000 L CNN
F 1 "10k" H 7920 5655 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7780 5700 50  0001 C CNN
F 3 "~" H 7850 5700 50  0001 C CNN
	1    7850 5700
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 5700 8000 5700
$Comp
L power:GND #PWR?
U 1 1 5FA3AC11
P 8050 5700
AR Path="/5FA3AC11" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AC11" Ref="#PWR093"  Part="1" 
F 0 "#PWR093" H 8050 5450 50  0001 C CNN
F 1 "GND" H 8055 5527 50  0000 C CNN
F 2 "" H 8050 5700 50  0001 C CNN
F 3 "" H 8050 5700 50  0001 C CNN
	1    8050 5700
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3AC17
P 8050 5400
AR Path="/5FA3AC17" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AC17" Ref="#PWR092"  Part="1" 
F 0 "#PWR092" H 8050 5150 50  0001 C CNN
F 1 "GND" H 8055 5227 50  0000 C CNN
F 2 "" H 8050 5400 50  0001 C CNN
F 3 "" H 8050 5400 50  0001 C CNN
	1    8050 5400
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3AC1D
P 8050 5100
AR Path="/5FA3AC1D" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AC1D" Ref="#PWR091"  Part="1" 
F 0 "#PWR091" H 8050 4850 50  0001 C CNN
F 1 "GND" H 8055 4927 50  0000 C CNN
F 2 "" H 8050 5100 50  0001 C CNN
F 3 "" H 8050 5100 50  0001 C CNN
	1    8050 5100
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3AC23
P 8050 4800
AR Path="/5FA3AC23" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AC23" Ref="#PWR090"  Part="1" 
F 0 "#PWR090" H 8050 4550 50  0001 C CNN
F 1 "GND" H 8055 4627 50  0000 C CNN
F 2 "" H 8050 4800 50  0001 C CNN
F 3 "" H 8050 4800 50  0001 C CNN
	1    8050 4800
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3AC29
P 8050 4500
AR Path="/5FA3AC29" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AC29" Ref="#PWR089"  Part="1" 
F 0 "#PWR089" H 8050 4250 50  0001 C CNN
F 1 "GND" H 8055 4327 50  0000 C CNN
F 2 "" H 8050 4500 50  0001 C CNN
F 3 "" H 8050 4500 50  0001 C CNN
	1    8050 4500
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3AC2F
P 8050 4200
AR Path="/5FA3AC2F" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AC2F" Ref="#PWR088"  Part="1" 
F 0 "#PWR088" H 8050 3950 50  0001 C CNN
F 1 "GND" H 8055 4027 50  0000 C CNN
F 2 "" H 8050 4200 50  0001 C CNN
F 3 "" H 8050 4200 50  0001 C CNN
	1    8050 4200
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3AC35
P 8050 3900
AR Path="/5FA3AC35" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AC35" Ref="#PWR087"  Part="1" 
F 0 "#PWR087" H 8050 3650 50  0001 C CNN
F 1 "GND" H 8055 3727 50  0000 C CNN
F 2 "" H 8050 3900 50  0001 C CNN
F 3 "" H 8050 3900 50  0001 C CNN
	1    8050 3900
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3AC3B
P 8050 3600
AR Path="/5FA3AC3B" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AC3B" Ref="#PWR086"  Part="1" 
F 0 "#PWR086" H 8050 3350 50  0001 C CNN
F 1 "GND" H 8055 3427 50  0000 C CNN
F 2 "" H 8050 3600 50  0001 C CNN
F 3 "" H 8050 3600 50  0001 C CNN
	1    8050 3600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3AC41
P 8050 3300
AR Path="/5FA3AC41" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AC41" Ref="#PWR085"  Part="1" 
F 0 "#PWR085" H 8050 3050 50  0001 C CNN
F 1 "GND" H 8055 3127 50  0000 C CNN
F 2 "" H 8050 3300 50  0001 C CNN
F 3 "" H 8050 3300 50  0001 C CNN
	1    8050 3300
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3AC47
P 8050 3000
AR Path="/5FA3AC47" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AC47" Ref="#PWR084"  Part="1" 
F 0 "#PWR084" H 8050 2750 50  0001 C CNN
F 1 "GND" H 8055 2827 50  0000 C CNN
F 2 "" H 8050 3000 50  0001 C CNN
F 3 "" H 8050 3000 50  0001 C CNN
	1    8050 3000
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3AC4D
P 8050 2700
AR Path="/5FA3AC4D" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AC4D" Ref="#PWR083"  Part="1" 
F 0 "#PWR083" H 8050 2450 50  0001 C CNN
F 1 "GND" H 8055 2527 50  0000 C CNN
F 2 "" H 8050 2700 50  0001 C CNN
F 3 "" H 8050 2700 50  0001 C CNN
	1    8050 2700
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3AC53
P 8050 2400
AR Path="/5FA3AC53" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AC53" Ref="#PWR082"  Part="1" 
F 0 "#PWR082" H 8050 2150 50  0001 C CNN
F 1 "GND" H 8055 2227 50  0000 C CNN
F 2 "" H 8050 2400 50  0001 C CNN
F 3 "" H 8050 2400 50  0001 C CNN
	1    8050 2400
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3AC59
P 8050 2100
AR Path="/5FA3AC59" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AC59" Ref="#PWR081"  Part="1" 
F 0 "#PWR081" H 8050 1850 50  0001 C CNN
F 1 "GND" H 8055 1927 50  0000 C CNN
F 2 "" H 8050 2100 50  0001 C CNN
F 3 "" H 8050 2100 50  0001 C CNN
	1    8050 2100
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3AC5F
P 8050 1800
AR Path="/5FA3AC5F" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AC5F" Ref="#PWR080"  Part="1" 
F 0 "#PWR080" H 8050 1550 50  0001 C CNN
F 1 "GND" H 8055 1627 50  0000 C CNN
F 2 "" H 8050 1800 50  0001 C CNN
F 3 "" H 8050 1800 50  0001 C CNN
	1    8050 1800
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3AC65
P 8050 1500
AR Path="/5FA3AC65" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AC65" Ref="#PWR079"  Part="1" 
F 0 "#PWR079" H 8050 1250 50  0001 C CNN
F 1 "GND" H 8055 1327 50  0000 C CNN
F 2 "" H 8050 1500 50  0001 C CNN
F 3 "" H 8050 1500 50  0001 C CNN
	1    8050 1500
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FA3AC6B
P 8050 1200
AR Path="/5FA3AC6B" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/5FA3AC6B" Ref="#PWR078"  Part="1" 
F 0 "#PWR078" H 8050 950 50  0001 C CNN
F 1 "GND" H 8055 1027 50  0000 C CNN
F 2 "" H 8050 1200 50  0001 C CNN
F 3 "" H 8050 1200 50  0001 C CNN
	1    8050 1200
	0    -1   -1   0   
$EndComp
Text Notes 6500 950  0    50   ~ 0
Control resistors
$Comp
L Components:ZXMHC3A01T8TA Q?
U 1 1 5FA3AC88
P 9750 1300
AR Path="/5FA3AC88" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3AC88" Ref="Q35"  Part="1" 
F 0 "Q35" H 10400 1550 50  0000 C CNN
F 1 "ZXMHC3A01T8TA" H 10350 1450 50  0000 C CNN
F 2 "Components:ZXMHC3A01T8TA" H 11000 1400 50  0001 L CNN
F 3 "https://www.diodes.com/assets/Datasheets/ZXMHC3F381N8.pdf" H 11000 1300 50  0001 L CNN
F 4 "MOSFET Dual N/P-Ch 30V 4.98A/4.13A SOIC8 Diodes Inc ZXMHC3F381N8TC Quad N/P-channel MOSFET Transistor, 8-Pin SO" H 11000 1200 50  0001 L CNN "Description"
F 5 "1.75" H 11000 1100 50  0001 L CNN "Height"
F 6 "522-ZXMHC3F381N8TC" H 11000 1000 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Diodes-Incorporated/ZXMHC3F381N8TC?qs=7E5rbXJDVJoJr15tcLBfrA%3D%3D" H 11000 900 50  0001 L CNN "Mouser Price/Stock"
F 8 "Diodes Inc." H 11000 800 50  0001 L CNN "Manufacturer_Name"
F 9 "ZXMHC3F381N8TC" H 11000 700 50  0001 L CNN "Manufacturer_Part_Number"
	1    9750 1300
	1    0    0    -1  
$EndComp
Wire Notes Line
	8350 950  8350 6050
Text Notes 2150 950  0    39   Italic 0
P-channel transistor drivers (Flow valves)
Text Notes 11100 4500 0    50   ~ 0
Low pass filters
Wire Wire Line
	12400 4800 12450 4800
Wire Wire Line
	12400 4900 12400 4800
Wire Wire Line
	12800 4800 12750 4800
Wire Wire Line
	12800 5200 12800 4800
Text Label 12800 5200 3    50   ~ 0
FLOW_VALVE_6-
Text Label 12400 5200 3    50   ~ 0
FLOW_VALVE_6+
$Comp
L Device:C C?
U 1 1 5FA3AC9D
P 12600 4800
AR Path="/5FA3AC9D" Ref="C?"  Part="1" 
AR Path="/5F9F1786/5FA3AC9D" Ref="C14"  Part="1" 
F 0 "C14" H 12715 4846 50  0000 L CNN
F 1 "1u" H 12715 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 12638 4650 50  0001 C CNN
F 3 "~" H 12600 4800 50  0001 C CNN
	1    12600 4800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FA3ACA3
P 12400 5050
AR Path="/5FA3ACA3" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ACA3" Ref="R75"  Part="1" 
F 0 "R75" V 12193 5050 50  0000 C CNN
F 1 "1" V 12284 5050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 12330 5050 50  0001 C CNN
F 3 "~" H 12400 5050 50  0001 C CNN
	1    12400 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	11700 4800 11750 4800
Wire Wire Line
	11700 4900 11700 4800
Wire Wire Line
	12100 4800 12050 4800
Wire Wire Line
	12100 5200 12100 4800
Text Label 12100 5200 3    50   ~ 0
FLOW_VALVE_5-
Text Label 11700 5200 3    50   ~ 0
FLOW_VALVE_5+
$Comp
L Device:C C?
U 1 1 5FA3ACAF
P 11900 4800
AR Path="/5FA3ACAF" Ref="C?"  Part="1" 
AR Path="/5F9F1786/5FA3ACAF" Ref="C13"  Part="1" 
F 0 "C13" H 12015 4846 50  0000 L CNN
F 1 "1u" H 12015 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 11938 4650 50  0001 C CNN
F 3 "~" H 11900 4800 50  0001 C CNN
	1    11900 4800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FA3ACB5
P 11700 5050
AR Path="/5FA3ACB5" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ACB5" Ref="R74"  Part="1" 
F 0 "R74" V 11493 5050 50  0000 C CNN
F 1 "1" V 11584 5050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 11630 5050 50  0001 C CNN
F 3 "~" H 11700 5050 50  0001 C CNN
	1    11700 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 4800 11050 4800
Wire Wire Line
	11000 4900 11000 4800
Wire Wire Line
	11400 4800 11350 4800
Wire Wire Line
	11400 5200 11400 4800
Text Label 11400 5200 3    50   ~ 0
FLOW_VALVE_4-
Text Label 11000 5200 3    50   ~ 0
FLOW_VALVE_4+
$Comp
L Device:C C?
U 1 1 5FA3ACC1
P 11200 4800
AR Path="/5FA3ACC1" Ref="C?"  Part="1" 
AR Path="/5F9F1786/5FA3ACC1" Ref="C12"  Part="1" 
F 0 "C12" H 11315 4846 50  0000 L CNN
F 1 "1u" H 11315 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 11238 4650 50  0001 C CNN
F 3 "~" H 11200 4800 50  0001 C CNN
	1    11200 4800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FA3ACC7
P 11000 5050
AR Path="/5FA3ACC7" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ACC7" Ref="R73"  Part="1" 
F 0 "R73" V 10793 5050 50  0000 C CNN
F 1 "1" V 10884 5050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10930 5050 50  0001 C CNN
F 3 "~" H 11000 5050 50  0001 C CNN
	1    11000 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	10300 4800 10350 4800
Wire Wire Line
	10300 4900 10300 4800
Wire Wire Line
	10700 4800 10650 4800
Wire Wire Line
	10700 5200 10700 4800
Text Label 10700 5200 3    50   ~ 0
FLOW_VALVE_3-
Text Label 10300 5200 3    50   ~ 0
FLOW_VALVE_3+
$Comp
L Device:C C?
U 1 1 5FA3ACD3
P 10500 4800
AR Path="/5FA3ACD3" Ref="C?"  Part="1" 
AR Path="/5F9F1786/5FA3ACD3" Ref="C11"  Part="1" 
F 0 "C11" H 10615 4846 50  0000 L CNN
F 1 "1u" H 10615 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10538 4650 50  0001 C CNN
F 3 "~" H 10500 4800 50  0001 C CNN
	1    10500 4800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FA3ACD9
P 10300 5050
AR Path="/5FA3ACD9" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ACD9" Ref="R72"  Part="1" 
F 0 "R72" V 10093 5050 50  0000 C CNN
F 1 "1" V 10184 5050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10230 5050 50  0001 C CNN
F 3 "~" H 10300 5050 50  0001 C CNN
	1    10300 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 4800 9650 4800
Wire Wire Line
	9600 4900 9600 4800
Wire Wire Line
	10000 4800 9950 4800
Wire Wire Line
	10000 5200 10000 4800
Text Label 10000 5200 3    50   ~ 0
FLOW_VALVE_2-
Text Label 9600 5200 3    50   ~ 0
FLOW_VALVE_2+
$Comp
L Device:C C?
U 1 1 5FA3ACE5
P 9800 4800
AR Path="/5FA3ACE5" Ref="C?"  Part="1" 
AR Path="/5F9F1786/5FA3ACE5" Ref="C10"  Part="1" 
F 0 "C10" H 9915 4846 50  0000 L CNN
F 1 "1u" H 9915 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9838 4650 50  0001 C CNN
F 3 "~" H 9800 4800 50  0001 C CNN
	1    9800 4800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FA3ACEB
P 9600 5050
AR Path="/5FA3ACEB" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ACEB" Ref="R71"  Part="1" 
F 0 "R71" V 9393 5050 50  0000 C CNN
F 1 "1" V 9484 5050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9530 5050 50  0001 C CNN
F 3 "~" H 9600 5050 50  0001 C CNN
	1    9600 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 4800 8950 4800
Wire Wire Line
	8900 4900 8900 4800
Wire Wire Line
	9300 4800 9250 4800
Wire Wire Line
	9300 5200 9300 4800
Text Label 9300 5200 3    50   ~ 0
FLOW_VALVE_1-
Text Label 8900 5200 3    50   ~ 0
FLOW_VALVE_1+
$Comp
L Device:C C?
U 1 1 5FA3ACF7
P 9100 4800
AR Path="/5FA3ACF7" Ref="C?"  Part="1" 
AR Path="/5F9F1786/5FA3ACF7" Ref="C9"  Part="1" 
F 0 "C9" H 9215 4846 50  0000 L CNN
F 1 "1u" H 9215 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9138 4650 50  0001 C CNN
F 3 "~" H 9100 4800 50  0001 C CNN
	1    9100 4800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FA3ACFD
P 8900 5050
AR Path="/5FA3ACFD" Ref="R?"  Part="1" 
AR Path="/5F9F1786/5FA3ACFD" Ref="R70"  Part="1" 
F 0 "R70" V 8693 5050 50  0000 C CNN
F 1 "1" V 8784 5050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8830 5050 50  0001 C CNN
F 3 "~" H 8900 5050 50  0001 C CNN
	1    8900 5050
	1    0    0    -1  
$EndComp
Text Label 6650 8450 2    50   ~ 0
FLOW_VALVE_8-
Text Label 6650 8350 2    50   ~ 0
FLOW_VALVE_8+
Text Label 6650 8200 2    50   ~ 0
FLOW_VALVE_7-
Text Label 6650 8100 2    50   ~ 0
FLOW_VALVE_7+
Text Label 6650 7950 2    50   ~ 0
FLOW_VALVE_6-
Text Label 6650 7850 2    50   ~ 0
FLOW_VALVE_6+
Text Label 6650 7700 2    50   ~ 0
FLOW_VALVE_5-
Text Label 6650 7600 2    50   ~ 0
FLOW_VALVE_5+
Text Label 6650 7450 2    50   ~ 0
FLOW_VALVE_4-
Text Label 6650 7350 2    50   ~ 0
FLOW_VALVE_4+
Text Label 6650 7200 2    50   ~ 0
FLOW_VALVE_3-
Text Label 6650 7100 2    50   ~ 0
FLOW_VALVE_3+
Text Label 6650 6950 2    50   ~ 0
FLOW_VALVE_2-
Text Label 6650 6850 2    50   ~ 0
FLOW_VALVE_2+
Text Label 6650 6700 2    50   ~ 0
FLOW_VALVE_1-
Text Label 6650 6600 2    50   ~ 0
FLOW_VALVE_1+
Text HLabel 6650 6600 2    50   Output ~ 0
FLOW_VALVE_1+
Text HLabel 6650 6700 2    50   Output ~ 0
FLOW_VALVE_1-
Text HLabel 6650 6850 2    50   Output ~ 0
FLOW_VALVE_2+
Text HLabel 6650 6950 2    50   Output ~ 0
FLOW_VALVE_2-
Text HLabel 6650 7100 2    50   Output ~ 0
FLOW_VALVE_3+
Text HLabel 6650 7200 2    50   Output ~ 0
FLOW_VALVE_3-
Text HLabel 6650 7350 2    50   Output ~ 0
FLOW_VALVE_4+
Text HLabel 6650 7450 2    50   Output ~ 0
FLOW_VALVE_4-
Text HLabel 6650 7600 2    50   Output ~ 0
FLOW_VALVE_5+
Text HLabel 6650 7700 2    50   Output ~ 0
FLOW_VALVE_5-
Text HLabel 6650 7850 2    50   Output ~ 0
FLOW_VALVE_6+
Text HLabel 6650 7950 2    50   Output ~ 0
FLOW_VALVE_6-
Text HLabel 6650 8100 2    50   Output ~ 0
FLOW_VALVE_7+
Text HLabel 6650 8200 2    50   Output ~ 0
FLOW_VALVE_7-
Text HLabel 6650 8350 2    50   Output ~ 0
FLOW_VALVE_8+
Text HLabel 6650 8450 2    50   Output ~ 0
FLOW_VALVE_8-
Wire Wire Line
	6450 1250 6450 1550
Connection ~ 6450 1550
Wire Wire Line
	6450 1550 6450 1850
Connection ~ 6450 1850
Wire Wire Line
	6450 1850 6450 2150
Connection ~ 6450 2150
Wire Wire Line
	6450 2150 6450 2450
Connection ~ 6450 2450
Wire Wire Line
	6450 2450 6450 2750
Connection ~ 6450 2750
Wire Wire Line
	6450 2750 6450 3050
Connection ~ 6450 3050
Wire Wire Line
	6450 3050 6450 3350
Connection ~ 6450 3350
Wire Wire Line
	6450 3350 6450 3650
Connection ~ 6450 3650
Wire Wire Line
	6450 3650 6450 3950
Connection ~ 6450 3950
Wire Wire Line
	6450 3950 6450 4250
Connection ~ 6450 4250
Wire Wire Line
	6450 4250 6450 4550
Connection ~ 6450 4550
Wire Wire Line
	6450 4550 6450 4850
Connection ~ 6450 4850
Wire Wire Line
	6450 4850 6450 5150
Connection ~ 6450 5150
Wire Wire Line
	6450 5150 6450 5450
Connection ~ 6450 5450
Wire Wire Line
	6450 5450 6450 5750
Wire Wire Line
	6450 1250 6450 1200
Connection ~ 6450 1250
Text HLabel 1750 1550 0    50   Input ~ 0
FLOW_VALVE_1_OPEN
Text HLabel 1750 2550 0    50   Input ~ 0
FLOW_VALVE_1_CLOSE
Text HLabel 1750 3550 0    50   Input ~ 0
FLOW_VALVE_2_OPEN
Text HLabel 1750 4550 0    50   Input ~ 0
FLOW_VALVE_2_CLOSE
Text HLabel 1750 5550 0    50   Input ~ 0
FLOW_VALVE_3_OPEN
Text HLabel 1750 6550 0    50   Input ~ 0
FLOW_VALVE_3_CLOSE
Text HLabel 1750 7550 0    50   Input ~ 0
FLOW_VALVE_4_OPEN
Text HLabel 1750 8550 0    50   Input ~ 0
FLOW_VALVE_4_CLOSE
Text HLabel 3750 1550 0    50   Input ~ 0
FLOW_VALVE_5_OPEN
Text HLabel 3750 2550 0    50   Input ~ 0
FLOW_VALVE_5_CLOSE
Text HLabel 3750 3550 0    50   Input ~ 0
FLOW_VALVE_6_OPEN
Text HLabel 3750 4550 0    50   Input ~ 0
FLOW_VALVE_6_CLOSE
Text HLabel 3750 5550 0    50   Input ~ 0
FLOW_VALVE_7_OPEN
Text HLabel 3750 6550 0    50   Input ~ 0
FLOW_VALVE_7_CLOSE
Text HLabel 3750 7550 0    50   Input ~ 0
FLOW_VALVE_8_OPEN
Text HLabel 3750 8550 0    50   Input ~ 0
FLOW_VALVE_8_CLOSE
Text HLabel 7700 1200 0    50   Input ~ 0
FLOW_VALVE_1_OPEN
Text HLabel 7700 1500 0    50   Input ~ 0
FLOW_VALVE_1_CLOSE
Text HLabel 7700 1800 0    50   Input ~ 0
FLOW_VALVE_2_OPEN
Text HLabel 7700 2100 0    50   Input ~ 0
FLOW_VALVE_2_CLOSE
Text HLabel 7700 2400 0    50   Input ~ 0
FLOW_VALVE_3_OPEN
Text HLabel 7700 2700 0    50   Input ~ 0
FLOW_VALVE_3_CLOSE
Text HLabel 7700 3000 0    50   Input ~ 0
FLOW_VALVE_4_OPEN
Text HLabel 7700 3300 0    50   Input ~ 0
FLOW_VALVE_4_CLOSE
Text HLabel 7700 3600 0    50   Input ~ 0
FLOW_VALVE_5_OPEN
Text HLabel 7700 3900 0    50   Input ~ 0
FLOW_VALVE_5_CLOSE
Text HLabel 7700 4200 0    50   Input ~ 0
FLOW_VALVE_6_OPEN
Text HLabel 7700 4500 0    50   Input ~ 0
FLOW_VALVE_6_CLOSE
Text HLabel 7700 4800 0    50   Input ~ 0
FLOW_VALVE_7_OPEN
Text HLabel 7700 5100 0    50   Input ~ 0
FLOW_VALVE_7_CLOSE
Text HLabel 7700 5400 0    50   Input ~ 0
FLOW_VALVE_8_OPEN
Text HLabel 7700 5700 0    50   Input ~ 0
FLOW_VALVE_8_CLOSE
Text HLabel 9750 2200 0    50   Input ~ 0
FLOW_VALVE_2_CLOSE
Text HLabel 9750 2000 0    50   Input ~ 0
FLOW_VALVE_2_OPEN
Text HLabel 9750 2800 0    50   Input ~ 0
FLOW_VALVE_3_OPEN
Text HLabel 9750 3000 0    50   Input ~ 0
FLOW_VALVE_3_CLOSE
Text HLabel 9750 3750 0    50   Input ~ 0
FLOW_VALVE_4_CLOSE
Text HLabel 9750 3550 0    50   Input ~ 0
FLOW_VALVE_4_OPEN
Text HLabel 13150 1300 0    50   Input ~ 0
FLOW_VALVE_5_OPEN
Text HLabel 13150 1500 0    50   Input ~ 0
FLOW_VALVE_5_CLOSE
Text HLabel 13150 2050 0    50   Input ~ 0
FLOW_VALVE_6_OPEN
Text HLabel 13150 2250 0    50   Input ~ 0
FLOW_VALVE_6_CLOSE
Text HLabel 13150 2800 0    50   Input ~ 0
FLOW_VALVE_7_OPEN
Text HLabel 13150 3000 0    50   Input ~ 0
FLOW_VALVE_7_CLOSE
Text HLabel 13150 3750 0    50   Input ~ 0
FLOW_VALVE_8_CLOSE
Text HLabel 13150 3550 0    50   Input ~ 0
FLOW_VALVE_8_OPEN
Text GLabel 11100 2100 2    50   Input ~ 0
5V
Text GLabel 11100 2900 2    50   Input ~ 0
5V
Text GLabel 11100 3650 2    50   Input ~ 0
5V
Text GLabel 14500 3650 2    50   Input ~ 0
5V
Text GLabel 14500 2900 2    50   Input ~ 0
5V
Text GLabel 14500 2150 2    50   Input ~ 0
5V
Text GLabel 14500 1400 2    50   Input ~ 0
5V
Text GLabel 6450 1200 1    50   Input ~ 0
5V
$Comp
L Components:ZXMHC3A01T8TA Q?
U 1 1 5FA3AAC5
P 9750 2000
AR Path="/5FA3AAC5" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3AAC5" Ref="Q36"  Part="1" 
F 0 "Q36" H 10450 2265 50  0000 C CNN
F 1 "ZXMHC3A01T8TA" H 10450 2174 50  0000 C CNN
F 2 "Components:ZXMHC3A01T8TA" H 11000 2100 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ZXMHC3A01T8TA.pdf" H 11000 2000 50  0001 L CNN
F 4 "DIODES INC. - ZXMHC3A01T8TA - Dual MOSFET, Enhancement Mode, N and P Channel, 3.1 A, 30 V, 0.12 ohm, 10 V, 1 V" H 11000 1900 50  0001 L CNN "Description"
F 5 "1.7" H 11000 1800 50  0001 L CNN "Height"
F 6 "522-ZXMHC3A01T8TA" H 11000 1700 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Diodes-Incorporated/ZXMHC3A01T8TA?qs=Xl4UVSSad%2Fqyu39NiNDXJg%3D%3D" H 11000 1600 50  0001 L CNN "Mouser Price/Stock"
F 8 "Diodes Inc." H 11000 1500 50  0001 L CNN "Manufacturer_Name"
F 9 "ZXMHC3A01T8TA" H 11000 1400 50  0001 L CNN "Manufacturer_Part_Number"
	1    9750 2000
	1    0    0    -1  
$EndComp
Text Label 10950 1300 0    50   ~ 0
FLOW_VALVE_1+
Text GLabel 10950 1400 2    50   Input ~ 0
5V
Text Label 11100 1500 0    50   ~ 0
FLOW_VALVE_1-
Text Label 10950 1600 0    50   ~ 0
FLOW_VALVE_1_REV+
Text Label 9750 1600 2    50   ~ 0
FLOW_VALVE_1_FWD+
Text HLabel 9750 1500 0    50   Input ~ 0
FLOW_VALVE_1_CLOSE
$Comp
L power:GND #PWR?
U 1 1 61D79921
P 9550 1650
AR Path="/61D79921" Ref="#PWR?"  Part="1" 
AR Path="/5F9F1786/61D79921" Ref="#PWR0159"  Part="1" 
F 0 "#PWR0159" H 9550 1400 50  0001 C CNN
F 1 "GND" H 9555 1477 50  0000 C CNN
F 2 "" H 9550 1650 50  0001 C CNN
F 3 "" H 9550 1650 50  0001 C CNN
	1    9550 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 1650 9550 1400
Wire Wire Line
	9550 1400 9750 1400
Text HLabel 9750 1300 0    50   Input ~ 0
FLOW_VALVE_1_OPEN
Wire Wire Line
	9600 2900 9600 3200
Wire Wire Line
	11100 1500 10950 1500
Wire Wire Line
	9600 3650 9600 3850
Wire Wire Line
	13000 1400 13000 1600
Wire Wire Line
	13000 2900 13000 3100
$Comp
L Components:ZXMHC3A01T8TA Q?
U 1 1 5FA3AB09
P 9750 3550
AR Path="/5FA3AB09" Ref="Q?"  Part="1" 
AR Path="/5F9F1786/5FA3AB09" Ref="Q38"  Part="1" 
F 0 "Q38" H 10450 3815 50  0000 C CNN
F 1 "ZXMHC3A01T8TA" H 10450 3724 50  0000 C CNN
F 2 "Components:SOT153P700X170-8N" H 11000 3650 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ZXMHC3A01T8TA.pdf" H 11000 3550 50  0001 L CNN
F 4 "DIODES INC. - ZXMHC3A01T8TA - Dual MOSFET, Enhancement Mode, N and P Channel, 3.1 A, 30 V, 0.12 ohm, 10 V, 1 V" H 11000 3450 50  0001 L CNN "Description"
F 5 "1.7" H 11000 3350 50  0001 L CNN "Height"
F 6 "522-ZXMHC3A01T8TA" H 11000 3250 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Diodes-Incorporated/ZXMHC3A01T8TA?qs=Xl4UVSSad%2Fqyu39NiNDXJg%3D%3D" H 11000 3150 50  0001 L CNN "Mouser Price/Stock"
F 8 "Diodes Inc." H 11000 3050 50  0001 L CNN "Manufacturer_Name"
F 9 "ZXMHC3A01T8TA" H 11000 2950 50  0001 L CNN "Manufacturer_Part_Number"
	1    9750 3550
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 62502729
P 9750 1300
AR Path="/60E3C945/62502729" Ref="TP?"  Part="1" 
AR Path="/5F9F1786/62502729" Ref="TP6"  Part="1" 
F 0 "TP6" H 9808 1418 50  0000 L CNN
F 1 "TestPoint" H 9808 1327 50  0001 L CNN
F 2 "digikey-footprints:Test_Point_D1.02mmMOD" H 9950 1300 50  0001 C CNN
F 3 "~" H 9950 1300 50  0001 C CNN
	1    9750 1300
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 62506736
P 10950 1600
AR Path="/60E3C945/62506736" Ref="TP?"  Part="1" 
AR Path="/5F9F1786/62506736" Ref="TP10"  Part="1" 
F 0 "TP10" H 11008 1718 50  0000 L CNN
F 1 "TestPoint" H 11008 1627 50  0001 L CNN
F 2 "digikey-footprints:Test_Point_D1.02mmMOD" H 11150 1600 50  0001 C CNN
F 3 "~" H 11150 1600 50  0001 C CNN
	1    10950 1600
	-1   0    0    1   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 62505DC4
P 9750 1600
AR Path="/60E3C945/62505DC4" Ref="TP?"  Part="1" 
AR Path="/5F9F1786/62505DC4" Ref="TP8"  Part="1" 
F 0 "TP8" H 9808 1718 50  0000 L CNN
F 1 " " H 9808 1627 50  0000 L CNN
F 2 "digikey-footprints:Test_Point_D1.02mmMOD" H 9950 1600 50  0001 C CNN
F 3 "~" H 9950 1600 50  0001 C CNN
	1    9750 1600
	-1   0    0    1   
$EndComp
Text Notes 2100 850  0    50   ~ 0
UPDATE: Changed to N-Channel ones
$EndSCHEMATC
