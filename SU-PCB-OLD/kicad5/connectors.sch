EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 13 14
Title "Science Unit Main Board (Payload Board)"
Date "2022-03-13"
Rev "2.1 (EM)"
Comp "SpaceDot"
Comment1 "AcubeSAT"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Notes Line
	6600 1000 9400 1000
Wire Notes Line
	6600 2250 6600 1000
Wire Notes Line
	9400 2250 6600 2250
Wire Notes Line
	9400 1000 9400 2250
Text Notes 7750 1000 0    50   ~ 0
Flow pump
Wire Notes Line
	6550 3650 6550 2400
Text Notes 7750 2400 0    50   ~ 0
Control pump
Wire Notes Line
	2200 3650 4800 3650
Wire Notes Line
	2200 1200 4800 1200
Wire Notes Line
	4800 1200 4800 3650
Wire Notes Line
	2200 1200 2200 3650
Text Notes 3000 1200 0    50   ~ 0
AD590 Chip Temp Sensors
$Comp
L Device:R R?
U 1 1 5FB76DF5
P 3650 2015
AR Path="/5FB76DF5" Ref="R?"  Part="1" 
AR Path="/5FB2AE1D/5FB76DF5" Ref="R25"  Part="1" 
F 0 "R25" V 3650 2230 50  0000 L CNN
F 1 "TBD" V 3650 1950 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3580 2015 50  0001 C CNN
F 3 "~" H 3650 2015 50  0001 C CNN
	1    3650 2015
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FB76E16
P 3070 1765
AR Path="/5FB76E16" Ref="#PWR?"  Part="1" 
AR Path="/5FB2AE1D/5FB76E16" Ref="#PWR029"  Part="1" 
F 0 "#PWR029" H 3070 1515 50  0001 C CNN
F 1 "GND" H 3075 1592 50  0000 C CNN
F 2 "" H 3070 1765 50  0001 C CNN
F 3 "" H 3070 1765 50  0001 C CNN
	1    3070 1765
	-1   0    0    1   
$EndComp
Wire Wire Line
	3070 1765 3070 1815
Wire Wire Line
	3070 1815 3450 1815
$Comp
L Components:203556-0407 J?
U 1 1 5FB76E61
P 3015 6105
AR Path="/5FB76E61" Ref="J?"  Part="1" 
AR Path="/5FB2AE1D/5FB76E61" Ref="J1"  Part="1" 
F 0 "J1" H 3415 6370 50  0000 C CNN
F 1 "203556-0407" H 3415 6279 50  0000 C CNN
F 2 "Components:2035560407" H 3665 6205 50  0001 L CNN
F 3 "http://www.molex.com/webdocs/datasheets/pdf/en-us/5040500491_PCB_HEADERS.pdf" H 3665 6105 50  0001 L CNN
F 4 "Molex 504050 Series, 1.5mm Pitch 4 Way 1 Row Right Angle PCB Header, SMT Termination, 3A" H 3665 6005 50  0001 L CNN "Description"
F 5 "538-504050-0491" H 3665 5805 50  0001 L CNN "Mouser Part Number"
F 6 "https://www.mouser.co.uk/ProductDetail/Molex/504050-0491?qs=yXHQdf%252Bjwlut3yyE1%252ByhMA%3D%3D" H 3665 5705 50  0001 L CNN "Mouser Price/Stock"
F 7 "Molex" H 3665 5605 50  0001 L CNN "Manufacturer_Name"
F 8 "504050-0491" H 3665 5505 50  0001 L CNN "Manufacturer_Part_Number"
	1    3015 6105
	1    0    0    -1  
$EndComp
Text Notes 3250 5800 0    50   ~ 0
LED Strip
Wire Wire Line
	3865 6105 3815 6105
Text Notes 5050 5800 0    50   ~ 0
CAN Bus Connectors
Wire Notes Line
	6150 5800 6150 7000
Wire Notes Line
	6150 7000 4700 7000
Wire Notes Line
	4700 7000 4700 5800
Wire Notes Line
	4700 5800 6150 5800
Wire Notes Line
	6350 1200 4950 1200
Wire Notes Line
	6350 3150 6350 1200
Wire Notes Line
	4950 3150 6350 3150
Wire Notes Line
	4950 3150 4950 1200
Wire Wire Line
	5600 2850 6050 2850
Wire Wire Line
	5600 2900 5600 2850
Wire Wire Line
	6050 2600 6050 2850
Connection ~ 5600 2850
Wire Wire Line
	5150 2850 5600 2850
Wire Wire Line
	5150 2700 5150 2850
$Comp
L power:GND #PWR?
U 1 1 5FB76EB6
P 5600 2900
AR Path="/5FB76EB6" Ref="#PWR?"  Part="1" 
AR Path="/5FB2AE1D/5FB76EB6" Ref="#PWR031"  Part="1" 
F 0 "#PWR031" H 5600 2650 50  0001 C CNN
F 1 "GND" H 5605 2727 50  0000 C CNN
F 2 "" H 5600 2900 50  0001 C CNN
F 3 "" H 5600 2900 50  0001 C CNN
	1    5600 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 2600 6050 2600
Wire Wire Line
	5150 2700 5200 2700
Connection ~ 5150 2700
Wire Wire Line
	5150 2500 5150 2700
Wire Wire Line
	5200 2500 5150 2500
Wire Wire Line
	6200 2700 6200 2600
Wire Wire Line
	6000 2700 6200 2700
Wire Wire Line
	6100 2500 6100 2400
Wire Wire Line
	6000 2500 6100 2500
Wire Wire Line
	5100 2600 5100 2450
Wire Wire Line
	5200 2600 5100 2600
$Comp
L SU_connectors:504050-0691 J?
U 1 1 5FB76EDE
P 5200 2500
AR Path="/5FB76EDE" Ref="J?"  Part="1" 
AR Path="/5FB2AE1D/5FB76EDE" Ref="J7"  Part="1" 
F 0 "J7" H 5600 2765 50  0000 C CNN
F 1 "504050-0691" H 5600 2674 50  0000 C CNN
F 2 "SU_connectors:504050-0691_2" H 5850 2600 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/504050-0691.pdfhttp://www.molex.com/pdm_docs/sd/5040500691_sd.pdf" H 5850 2500 50  0001 L CNN
F 4 "Pico-Lock SMT Wire-to-Board Header 6 way Molex Pico-Lock Series, Series Number 504050, 1.5mm Pitch 6 Way 1 Row Shrouded Right Angle PCB Header, Surface Mount" H 5850 2400 50  0001 L CNN "Description"
F 5 "538-504050-0691" H 5850 2200 50  0001 L CNN "Mouser Part Number"
F 6 "https://www.mouser.co.uk/ProductDetail/Molex/504050-0691?qs=bvCPb%252BE7ys2K1LQC9e%2FvRg%3D%3D" H 5850 2100 50  0001 L CNN "Mouser Price/Stock"
F 7 "Molex" H 5850 2000 50  0001 L CNN "Manufacturer_Name"
F 8 "504050-0691" H 5850 1900 50  0001 L CNN "Manufacturer_Part_Number"
	1    5200 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 1900 6050 1900
Wire Wire Line
	5600 1950 5600 1900
Wire Wire Line
	6050 1650 6050 1900
Connection ~ 5600 1900
Wire Wire Line
	5150 1900 5600 1900
Wire Wire Line
	5150 1750 5150 1900
$Comp
L power:GND #PWR?
U 1 1 5FB76EEA
P 5600 1950
AR Path="/5FB76EEA" Ref="#PWR?"  Part="1" 
AR Path="/5FB2AE1D/5FB76EEA" Ref="#PWR030"  Part="1" 
F 0 "#PWR030" H 5600 1700 50  0001 C CNN
F 1 "GND" H 5605 1777 50  0000 C CNN
F 2 "" H 5600 1950 50  0001 C CNN
F 3 "" H 5600 1950 50  0001 C CNN
	1    5600 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 1650 6050 1650
Wire Wire Line
	5150 1750 5200 1750
Connection ~ 5150 1750
Wire Wire Line
	5150 1550 5150 1750
Wire Wire Line
	5200 1550 5150 1550
Wire Wire Line
	8465 4980 8555 4980
Wire Wire Line
	8465 4980 8465 5030
Connection ~ 8465 4980
Wire Wire Line
	8465 4930 8465 4980
Wire Wire Line
	6200 1750 6200 1650
Wire Wire Line
	6000 1750 6200 1750
Wire Wire Line
	6100 1550 6100 1450
Wire Wire Line
	6000 1550 6100 1550
Wire Wire Line
	5100 1650 5100 1500
Wire Wire Line
	5200 1650 5100 1650
Text Notes 4350 3750 0    50   ~ 0
Valve Connectors
Wire Notes Line
	2200 3750 7300 3750
Text Notes 5250 1200 0    50   ~ 0
Platform connector (R)
$Comp
L SU_connectors:504050-0691 J?
U 1 1 5FB76F44
P 5200 1550
AR Path="/5FB76F44" Ref="J?"  Part="1" 
AR Path="/5FB2AE1D/5FB76F44" Ref="J6"  Part="1" 
F 0 "J6" H 5600 1815 50  0000 C CNN
F 1 "504050-0691" H 5600 1724 50  0000 C CNN
F 2 "SU_connectors:504050-0691_2" H 5850 1650 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/504050-0691.pdfhttp://www.molex.com/pdm_docs/sd/5040500691_sd.pdf" H 5850 1550 50  0001 L CNN
F 4 "Pico-Lock SMT Wire-to-Board Header 6 way Molex Pico-Lock Series, Series Number 504050, 1.5mm Pitch 6 Way 1 Row Shrouded Right Angle PCB Header, Surface Mount" H 5850 1450 50  0001 L CNN "Description"
F 5 "538-504050-0691" H 5850 1250 50  0001 L CNN "Mouser Part Number"
F 6 "https://www.mouser.co.uk/ProductDetail/Molex/504050-0691?qs=bvCPb%252BE7ys2K1LQC9e%2FvRg%3D%3D" H 5850 1150 50  0001 L CNN "Mouser Price/Stock"
F 7 "Molex" H 5850 1050 50  0001 L CNN "Manufacturer_Name"
F 8 "504050-0691" H 5850 950 50  0001 L CNN "Manufacturer_Part_Number"
	1    5200 1550
	1    0    0    -1  
$EndComp
Text Notes 8150 3850 0    50   ~ 0
Heater (R)
Wire Notes Line
	7300 5700 2200 5700
Wire Notes Line
	7300 3750 7300 5700
Wire Notes Line
	2200 3750 2200 5700
$Comp
L Components:203556-0407 J?
U 1 1 5FB76FAD
P 6650 2750
AR Path="/5FB76FAD" Ref="J?"  Part="1" 
AR Path="/5FB2AE1D/5FB76FAD" Ref="J11"  Part="1" 
F 0 "J11" H 7050 3015 50  0000 C CNN
F 1 "203556-0407" H 7050 2924 50  0000 C CNN
F 2 "Components:2035560407" H 7300 2850 50  0001 L CNN
F 3 "https://www.molex.com/webdocs/datasheets/pdf/en-us/2035560407_PCB_HEADERS.pdf" H 7300 2750 50  0001 L CNN
F 4 "Headers & Wire Housings PicoClasp Hdr SMT SR Vrt 4Ckt W/FL Au0.38" H 7300 2650 50  0001 L CNN "Description"
F 5 "5.41" H 7300 2550 50  0001 L CNN "Height"
F 6 "538-203556-0407" H 7300 2450 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Molex/203556-0407?qs=F5EMLAvA7IDG5KHyS4NlWw%3D%3D" H 7300 2350 50  0001 L CNN "Mouser Price/Stock"
F 8 "Molex" H 7300 2250 50  0001 L CNN "Manufacturer_Name"
F 9 "203556-0407" H 7300 2150 50  0001 L CNN "Manufacturer_Part_Number"
	1    6650 2750
	1    0    0    -1  
$EndComp
$Comp
L Components:203556-0407 J?
U 1 1 5FB76FB9
P 9350 3500
AR Path="/5FB76FB9" Ref="J?"  Part="1" 
AR Path="/5FB2AE1D/5FB76FB9" Ref="J16"  Part="1" 
F 0 "J16" H 9750 2935 50  0000 C CNN
F 1 "203556-0407" H 9750 3026 50  0000 C CNN
F 2 "Components:2035560407" H 10000 3600 50  0001 L CNN
F 3 "https://www.molex.com/webdocs/datasheets/pdf/en-us/2035560407_PCB_HEADERS.pdf" H 10000 3500 50  0001 L CNN
F 4 "Headers & Wire Housings PicoClasp Hdr SMT SR Vrt 4Ckt W/FL Au0.38" H 10000 3400 50  0001 L CNN "Description"
F 5 "5.41" H 10000 3300 50  0001 L CNN "Height"
F 6 "538-203556-0407" H 10000 3200 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Molex/203556-0407?qs=F5EMLAvA7IDG5KHyS4NlWw%3D%3D" H 10000 3100 50  0001 L CNN "Mouser Price/Stock"
F 8 "Molex" H 10000 3000 50  0001 L CNN "Manufacturer_Name"
F 9 "203556-0407" H 10000 2900 50  0001 L CNN "Manufacturer_Part_Number"
	1    9350 3500
	-1   0    0    1   
$EndComp
$Comp
L Components:203556-0407 J?
U 1 1 5FB76FC7
P 6750 1350
AR Path="/5FB76FC7" Ref="J?"  Part="1" 
AR Path="/5FB2AE1D/5FB76FC7" Ref="J12"  Part="1" 
F 0 "J12" H 7150 1615 50  0000 C CNN
F 1 "203556-0407" H 7150 1524 50  0000 C CNN
F 2 "Components:2035560407" H 7400 1450 50  0001 L CNN
F 3 "https://www.molex.com/webdocs/datasheets/pdf/en-us/2035560407_PCB_HEADERS.pdf" H 7400 1350 50  0001 L CNN
F 4 "Headers & Wire Housings PicoClasp Hdr SMT SR Vrt 4Ckt W/FL Au0.38" H 7400 1250 50  0001 L CNN "Description"
F 5 "5.41" H 7400 1150 50  0001 L CNN "Height"
F 6 "538-203556-0407" H 7400 1050 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Molex/203556-0407?qs=F5EMLAvA7IDG5KHyS4NlWw%3D%3D" H 7400 950 50  0001 L CNN "Mouser Price/Stock"
F 8 "Molex" H 7400 850 50  0001 L CNN "Manufacturer_Name"
F 9 "203556-0407" H 7400 750 50  0001 L CNN "Manufacturer_Part_Number"
	1    6750 1350
	1    0    0    -1  
$EndComp
$Comp
L Components:203556-0407 J?
U 1 1 5FB76FD3
P 9300 2100
AR Path="/5FB76FD3" Ref="J?"  Part="1" 
AR Path="/5FB2AE1D/5FB76FD3" Ref="J15"  Part="1" 
F 0 "J15" H 9700 1535 50  0000 C CNN
F 1 "203556-0407" H 9700 1626 50  0000 C CNN
F 2 "Components:2035560407" H 9950 2200 50  0001 L CNN
F 3 "https://www.molex.com/webdocs/datasheets/pdf/en-us/2035560407_PCB_HEADERS.pdf" H 9950 2100 50  0001 L CNN
F 4 "Headers & Wire Housings PicoClasp Hdr SMT SR Vrt 4Ckt W/FL Au0.38" H 9950 2000 50  0001 L CNN "Description"
F 5 "5.41" H 9950 1900 50  0001 L CNN "Height"
F 6 "538-203556-0407" H 9950 1800 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Molex/203556-0407?qs=F5EMLAvA7IDG5KHyS4NlWw%3D%3D" H 9950 1700 50  0001 L CNN "Mouser Price/Stock"
F 8 "Molex" H 9950 1600 50  0001 L CNN "Manufacturer_Name"
F 9 "203556-0407" H 9950 1500 50  0001 L CNN "Manufacturer_Part_Number"
	1    9300 2100
	-1   0    0    1   
$EndComp
Text HLabel 3800 4050 0    50   Input ~ 0
FLOW_VALVE_1+
Text HLabel 3800 4150 0    50   Input ~ 0
FLOW_VALVE_1-
Text HLabel 3800 4250 0    50   Input ~ 0
FLOW_VALVE_2+
Text HLabel 3800 4350 0    50   Input ~ 0
FLOW_VALVE_2-
Text HLabel 3800 4550 0    50   Input ~ 0
FLOW_VALVE_3+
Text HLabel 3800 4450 0    50   Input ~ 0
FLOW_VALVE_3-
Text HLabel 3800 4650 0    50   Input ~ 0
FLOW_VALVE_4+
Text HLabel 3800 4750 0    50   Input ~ 0
FLOW_VALVE_4-
Text HLabel 5350 4050 0    50   Input ~ 0
FLOW_VALVE_5+
Text HLabel 5350 4150 0    50   Input ~ 0
FLOW_VALVE_5-
Text HLabel 5350 4250 0    50   Input ~ 0
FLOW_VALVE_6+
Text HLabel 5350 4350 0    50   Input ~ 0
FLOW_VALVE_6-
Text HLabel 5350 4450 0    50   Input ~ 0
FLOW_VALVE_7+
Text HLabel 5350 4550 0    50   Input ~ 0
FLOW_VALVE_7-
Text HLabel 5350 4650 0    50   Input ~ 0
FLOW_VALVE_8+
Text HLabel 5350 4750 0    50   Input ~ 0
FLOW_VALVE_8-
Text HLabel 3865 6105 2    50   Input ~ 0
LED_CONTROL_2
Text HLabel 3110 6620 2    50   Input ~ 0
LED_CONTROL_4
Text HLabel 3815 6405 2    50   Input ~ 0
LED_CONTROL_1
Text HLabel 3110 6920 2    50   Input ~ 0
LED_CONTROL_3
Wire Notes Line
	2200 5800 2200 7100
Wire Notes Line
	2200 7100 4600 7100
Wire Notes Line
	4600 7100 4600 5800
Wire Notes Line
	2200 5800 4600 5800
Wire Notes Line
	6550 2400 9400 2400
Wire Notes Line
	9400 2400 9400 3650
Wire Notes Line
	9400 3650 6550 3650
Text HLabel 5000 5300 2    50   Input ~ 0
CONTROL_VALVE_POSITIVE_3
Text HLabel 4200 5300 0    50   Input ~ 0
CONTROL_VALVE_POSITIVE_4
Text HLabel 4200 5400 0    50   Input ~ 0
CONTROL_VALVE_POSITIVE_2
Text HLabel 5000 5400 2    50   Input ~ 0
CONTROL_VALVE_POSITIVE_1
Text HLabel 4200 5200 0    50   Input ~ 0
CONTROL_VALVE_POSITIVE_6
$Comp
L SU_connectors:504050-0791 J4
U 1 1 5FBBD476
P 4200 5200
F 0 "J4" H 4600 5465 50  0000 C CNN
F 1 "504050-0791" H 4600 5374 50  0000 C CNN
F 2 "SU_connectors:504050-0791" H 4850 5300 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/504050-0691.pdfhttp://www.molex.com/pdm_docs/sd/5040500691_sd.pdf" H 4850 5200 50  0001 L CNN
F 4 "Pico-Lock SMT Wire-to-Board Header 6 way Molex Pico-Lock Series, Series Number 504050, 1.5mm Pitch 6 Way 1 Row Shrouded Right Angle PCB Header, Surface Mount" H 4850 5100 50  0001 L CNN "Description"
F 5 "538-504050-0691" H 4850 4900 50  0001 L CNN "Mouser Part Number"
F 6 "https://www.mouser.co.uk/ProductDetail/Molex/504050-0691?qs=bvCPb%252BE7ys2K1LQC9e%2FvRg%3D%3D" H 4850 4800 50  0001 L CNN "Mouser Price/Stock"
F 7 "Molex" H 4850 4700 50  0001 L CNN "Manufacturer_Name"
F 8 "504050-0691" H 4850 4600 50  0001 L CNN "Manufacturer_Part_Number"
	1    4200 5200
	1    0    0    -1  
$EndComp
Text HLabel 5000 5200 2    50   Input ~ 0
CONTROL_VALVE_POSITIVE_5
Text HLabel 7550 1550 2    50   Input ~ 0
FLOW_PUMP_PHASE_A-
Text HLabel 7550 1650 2    50   Input ~ 0
FLOW_PUMP_PHASE_A+
Text HLabel 7550 1450 2    50   Input ~ 0
FLOW_PUMP_PHASE_B-
Text HLabel 7550 1350 2    50   Input ~ 0
FLOW_PUMP_PHASE_B+
Text HLabel 8500 2000 0    50   Input ~ 0
FLOW_PUMP_PHASE_A-
Text HLabel 8500 2100 0    50   Input ~ 0
FLOW_PUMP_PHASE_A+
Text HLabel 8500 1900 0    50   Input ~ 0
FLOW_PUMP_PHASE_B-
Text HLabel 8500 1800 0    50   Input ~ 0
FLOW_PUMP_PHASE_B+
Text HLabel 7450 2750 2    50   Input ~ 0
CONTROL_PUMP_PHASE_B+
Text HLabel 7450 2850 2    50   Input ~ 0
CONTROL_PUMP_PHASE_B-
Text HLabel 7450 2950 2    50   Input ~ 0
CONTROL_PUMP_PHASE_A+
Text HLabel 7450 3050 2    50   Input ~ 0
CONTROL_PUMP_PHASE_A-
Text HLabel 8550 3200 0    50   Input ~ 0
CONTROL_PUMP_PHASE_B+
Text HLabel 8550 3300 0    50   Input ~ 0
CONTROL_PUMP_PHASE_B-
Text HLabel 8550 3400 0    50   Input ~ 0
CONTROL_PUMP_PHASE_A+
Text HLabel 8550 3500 0    50   Input ~ 0
CONTROL_PUMP_PHASE_A-
Text GLabel 5100 1500 1    50   Input ~ 0
5V
Text GLabel 5100 2450 1    50   Input ~ 0
5V
Text GLabel 6200 1650 1    50   Input ~ 0
12V
Text GLabel 6200 2600 1    50   Input ~ 0
12V
Text GLabel 6100 1450 1    50   Input ~ 0
3.3V
Text GLabel 6100 2400 1    50   Input ~ 0
3.3V
Text GLabel 8555 4980 2    50   Input ~ 0
12V
$Comp
L Device:R R?
U 1 1 5F6F1FA5
P 3450 2015
AR Path="/5F6F1FA5" Ref="R?"  Part="1" 
AR Path="/5FB2AE1D/5F6F1FA5" Ref="R24"  Part="1" 
F 0 "R24" V 3450 2230 50  0000 L CNN
F 1 "TBD" V 3445 1945 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3380 2015 50  0001 C CNN
F 3 "~" H 3450 2015 50  0001 C CNN
	1    3450 2015
	1    0    0    -1  
$EndComp
Text GLabel 3810 2215 2    50   Input ~ 0
5V
Text HLabel 3810 2315 2    50   Output ~ 0
CHIP_TEMP_3_IN
Text HLabel 3810 2515 2    50   Output ~ 0
CHIP_TEMP_1_IN
Text HLabel 3810 2415 2    50   Output ~ 0
CHIP_TEMP_2_IN
$Comp
L SU_connectors:504050-0691 J18
U 1 1 61F2720C
P 7915 5880
F 0 "J18" H 8315 6145 50  0000 C CNN
F 1 "504050-0691" H 8315 6054 50  0000 C CNN
F 2 "SU_connectors:504050-0691_2" H 8565 5980 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/504050-0691.pdfhttp://www.molex.com/pdm_docs/sd/5040500691_sd.pdf" H 8565 5880 50  0001 L CNN
F 4 "Pico-Lock SMT Wire-to-Board Header 6 way Molex Pico-Lock Series, Series Number 504050, 1.5mm Pitch 6 Way 1 Row Shrouded Right Angle PCB Header, Surface Mount" H 8565 5780 50  0001 L CNN "Description"
F 5 "538-504050-0691" H 8565 5580 50  0001 L CNN "Mouser Part Number"
F 6 "https://www.mouser.co.uk/ProductDetail/Molex/504050-0691?qs=bvCPb%252BE7ys2K1LQC9e%2FvRg%3D%3D" H 8565 5480 50  0001 L CNN "Mouser Price/Stock"
F 7 "Molex" H 8565 5380 50  0001 L CNN "Manufacturer_Name"
F 8 "504050-0691" H 8565 5280 50  0001 L CNN "Manufacturer_Part_Number"
	1    7915 5880
	1    0    0    -1  
$EndComp
Wire Notes Line
	7400 5550 7400 6400
Wire Notes Line
	7400 6400 9550 6400
Wire Notes Line
	9550 6400 9550 5550
Wire Notes Line
	9550 5550 7400 5550
Wire Notes Line
	7400 5400 9350 5400
Text Notes 7850 5550 0    50   ~ 0
Conn for Manifold Pressure Sensors
Text HLabel 8715 5980 2    50   Output ~ 0
OUT_MANIFOLD_1
$Comp
L power:GND #PWR?
U 1 1 61F3A058
P 7915 5880
AR Path="/61F3A058" Ref="#PWR?"  Part="1" 
AR Path="/5FB2AE1D/61F3A058" Ref="#PWR0176"  Part="1" 
F 0 "#PWR0176" H 7915 5630 50  0001 C CNN
F 1 "GND" H 7920 5707 50  0000 C CNN
F 2 "" H 7915 5880 50  0001 C CNN
F 3 "" H 7915 5880 50  0001 C CNN
	1    7915 5880
	0    1    1    0   
$EndComp
Text Notes 8415 6330 0    50   ~ 0
WHITE CABLES CONNECT \n      TO 5+6 TO GND
Text GLabel 8715 5880 2    50   Input ~ 0
5V
Text HLabel 8715 6080 2    50   Output ~ 0
OUT_MANIFOLD_2
Wire Notes Line
	9350 3750 9350 5400
Wire Notes Line
	7400 3750 9350 3750
Wire Notes Line
	7400 3750 7400 5400
Wire Wire Line
	8470 4255 8470 4305
Connection ~ 8470 4255
Wire Wire Line
	8470 4205 8470 4255
Text HLabel 8505 4455 2    50   Input ~ 0
HEATER_GND_CHIP
$Comp
L 203556-0507:203556-0507 J3
U 1 1 6245A51E
P 2570 2215
F 0 "J3" H 2970 2480 50  0000 C CNN
F 1 "203556-0507" H 2970 2389 50  0000 C CNN
F 2 "Components:2035560507" H 3220 2315 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/504050-0691.pdfhttp://www.molex.com/pdm_docs/sd/5040500691_sd.pdf" H 3220 2215 50  0001 L CNN
F 4 "Pico-Lock SMT Wire-to-Board Header 6 way Molex Pico-Lock Series, Series Number 504050, 1.5mm Pitch 6 Way 1 Row Shrouded Right Angle PCB Header, Surface Mount" H 3220 2115 50  0001 L CNN "Description"
F 5 "538-504050-0691" H 3220 1915 50  0001 L CNN "Mouser Part Number"
F 6 "https://www.mouser.co.uk/ProductDetail/Molex/504050-0691?qs=bvCPb%252BE7ys2K1LQC9e%2FvRg%3D%3D" H 3220 1815 50  0001 L CNN "Mouser Price/Stock"
F 7 "Molex" H 3220 1715 50  0001 L CNN "Manufacturer_Name"
F 8 "504050-0691" H 3220 1615 50  0001 L CNN "Manufacturer_Part_Number"
	1    2570 2215
	1    0    0    -1  
$EndComp
Text HLabel 3810 2615 2    50   Output ~ 0
VALVES_TEMP_IN
$Comp
L Device:R R?
U 1 1 62463CD7
P 3550 2015
AR Path="/62463CD7" Ref="R?"  Part="1" 
AR Path="/5FB2AE1D/62463CD7" Ref="R145"  Part="1" 
F 0 "R145" V 3545 1605 50  0000 L CNN
F 1 "TBD" V 3545 1940 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3480 2015 50  0001 C CNN
F 3 "~" H 3550 2015 50  0001 C CNN
	1    3550 2015
	-1   0    0    1   
$EndComp
Wire Wire Line
	3550 1865 3550 1815
Connection ~ 3550 1815
$Comp
L Connector_Generic:Conn_01x02 J22
U 1 1 62448684
P 4000 4650
F 0 "J22" H 4080 4642 50  0000 L CNN
F 1 "Conn_01x02" H 4080 4551 50  0000 L CNN
F 2 "digikey-footprints:PinHeader_1x2_P2.54mm" H 4000 4650 50  0001 C CNN
F 3 "~" H 4000 4650 50  0001 C CNN
	1    4000 4650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J21
U 1 1 62449254
P 4000 4450
F 0 "J21" H 4080 4442 50  0000 L CNN
F 1 "Conn_01x02" H 4080 4351 50  0000 L CNN
F 2 "digikey-footprints:PinHeader_1x2_P2.54mm" H 4000 4450 50  0001 C CNN
F 3 "~" H 4000 4450 50  0001 C CNN
	1    4000 4450
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J10
U 1 1 624494F4
P 4000 4250
F 0 "J10" H 4080 4242 50  0000 L CNN
F 1 "Conn_01x02" H 4080 4151 50  0000 L CNN
F 2 "digikey-footprints:PinHeader_1x2_P2.54mm" H 4000 4250 50  0001 C CNN
F 3 "~" H 4000 4250 50  0001 C CNN
	1    4000 4250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J5
U 1 1 62449749
P 4000 4050
F 0 "J5" H 4080 4042 50  0000 L CNN
F 1 "Conn_01x02" H 4080 3951 50  0000 L CNN
F 2 "digikey-footprints:PinHeader_1x2_P2.54mm" H 4000 4050 50  0001 C CNN
F 3 "~" H 4000 4050 50  0001 C CNN
	1    4000 4050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J26
U 1 1 6245E7A5
P 5550 4650
F 0 "J26" H 5630 4642 50  0000 L CNN
F 1 "Conn_01x02" H 5630 4551 50  0000 L CNN
F 2 "digikey-footprints:PinHeader_1x2_P2.54mm" H 5550 4650 50  0001 C CNN
F 3 "~" H 5550 4650 50  0001 C CNN
	1    5550 4650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J25
U 1 1 6245E7AB
P 5550 4450
F 0 "J25" H 5630 4442 50  0000 L CNN
F 1 "Conn_01x02" H 5630 4351 50  0000 L CNN
F 2 "digikey-footprints:PinHeader_1x2_P2.54mm" H 5550 4450 50  0001 C CNN
F 3 "~" H 5550 4450 50  0001 C CNN
	1    5550 4450
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J24
U 1 1 6245E7B1
P 5550 4250
F 0 "J24" H 5630 4242 50  0000 L CNN
F 1 "Conn_01x02" H 5630 4151 50  0000 L CNN
F 2 "digikey-footprints:PinHeader_1x2_P2.54mm" H 5550 4250 50  0001 C CNN
F 3 "~" H 5550 4250 50  0001 C CNN
	1    5550 4250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J23
U 1 1 6245E7B7
P 5550 4050
F 0 "J23" H 5630 4042 50  0000 L CNN
F 1 "Conn_01x02" H 5630 3951 50  0000 L CNN
F 2 "digikey-footprints:PinHeader_1x2_P2.54mm" H 5550 4050 50  0001 C CNN
F 3 "~" H 5550 4050 50  0001 C CNN
	1    5550 4050
	1    0    0    -1  
$EndComp
Text HLabel 8515 5180 2    50   Input ~ 0
HEATER_GND_1
$Comp
L Components:203556-0407 J?
U 1 1 628E8134
P 2310 6620
AR Path="/628E8134" Ref="J?"  Part="1" 
AR Path="/5FB2AE1D/628E8134" Ref="J2"  Part="1" 
F 0 "J2" H 2710 6885 50  0000 C CNN
F 1 "203556-0407" H 2710 6794 50  0000 C CNN
F 2 "Components:2035560407" H 2960 6720 50  0001 L CNN
F 3 "http://www.molex.com/webdocs/datasheets/pdf/en-us/5040500491_PCB_HEADERS.pdf" H 2960 6620 50  0001 L CNN
F 4 "Molex 504050 Series, 1.5mm Pitch 4 Way 1 Row Right Angle PCB Header, SMT Termination, 3A" H 2960 6520 50  0001 L CNN "Description"
F 5 "538-504050-0491" H 2960 6320 50  0001 L CNN "Mouser Part Number"
F 6 "https://www.mouser.co.uk/ProductDetail/Molex/504050-0491?qs=yXHQdf%252Bjwlut3yyE1%252ByhMA%3D%3D" H 2960 6220 50  0001 L CNN "Mouser Price/Stock"
F 7 "Molex" H 2960 6120 50  0001 L CNN "Manufacturer_Name"
F 8 "504050-0491" H 2960 6020 50  0001 L CNN "Manufacturer_Part_Number"
	1    2310 6620
	1    0    0    -1  
$EndComp
Wire Wire Line
	3815 6205 3815 6250
Wire Wire Line
	3110 6720 3110 6770
$Comp
L Components:203556-0407 J?
U 1 1 5FB76F16
P 7665 4930
AR Path="/5FB76F16" Ref="J?"  Part="1" 
AR Path="/5FB2AE1D/5FB76F16" Ref="J13"  Part="1" 
F 0 "J13" H 8065 5195 50  0000 C CNN
F 1 "203556-0407" H 8065 5104 50  0000 C CNN
F 2 "Components:2035560407" H 8315 5030 50  0001 L CNN
F 3 "http://www.molex.com/webdocs/datasheets/pdf/en-us/5040500491_PCB_HEADERS.pdf" H 8315 4930 50  0001 L CNN
F 4 "Molex 504050 Series, 1.5mm Pitch 4 Way 1 Row Right Angle PCB Header, SMT Termination, 3A" H 8315 4830 50  0001 L CNN "Description"
F 5 "538-504050-0491" H 8315 4630 50  0001 L CNN "Mouser Part Number"
F 6 "https://www.mouser.co.uk/ProductDetail/Molex/504050-0491?qs=yXHQdf%252Bjwlut3yyE1%252ByhMA%3D%3D" H 8315 4530 50  0001 L CNN "Mouser Price/Stock"
F 7 "Molex" H 8315 4430 50  0001 L CNN "Manufacturer_Name"
F 8 "504050-0491" H 8315 4330 50  0001 L CNN "Manufacturer_Part_Number"
	1    7665 4930
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 1865 3450 1815
Connection ~ 3450 1815
Wire Wire Line
	3450 1815 3550 1815
Wire Wire Line
	8505 4455 8470 4455
Wire Wire Line
	8470 4405 8470 4455
Connection ~ 8470 4455
Wire Wire Line
	8470 4455 8470 4505
$Comp
L Components:203556-0407 J?
U 1 1 62446C0D
P 7670 4205
AR Path="/62446C0D" Ref="J?"  Part="1" 
AR Path="/5FB2AE1D/62446C0D" Ref="J19"  Part="1" 
F 0 "J19" H 8070 4470 50  0000 C CNN
F 1 "203556-0407" H 8070 4379 50  0000 C CNN
F 2 "Components:2035560407" H 8320 4305 50  0001 L CNN
F 3 "http://www.molex.com/webdocs/datasheets/pdf/en-us/5040500491_PCB_HEADERS.pdf" H 8320 4205 50  0001 L CNN
F 4 "Molex 504050 Series, 1.5mm Pitch 4 Way 1 Row Right Angle PCB Header, SMT Termination, 3A" H 8320 4105 50  0001 L CNN "Description"
F 5 "538-504050-0491" H 8320 3905 50  0001 L CNN "Mouser Part Number"
F 6 "https://www.mouser.co.uk/ProductDetail/Molex/504050-0491?qs=yXHQdf%252Bjwlut3yyE1%252ByhMA%3D%3D" H 8320 3805 50  0001 L CNN "Mouser Price/Stock"
F 7 "Molex" H 8320 3705 50  0001 L CNN "Manufacturer_Name"
F 8 "504050-0491" H 8320 3605 50  0001 L CNN "Manufacturer_Part_Number"
	1    7670 4205
	1    0    0    -1  
$EndComp
Wire Wire Line
	8465 5130 8465 5180
Wire Wire Line
	8515 5180 8465 5180
Connection ~ 8465 5180
Wire Wire Line
	8465 5180 8465 5230
Wire Wire Line
	8470 4255 8550 4255
Text GLabel 8550 4255 2    50   Input ~ 0
12V
Text GLabel 3155 6770 2    50   Input ~ 0
3.3V
Wire Wire Line
	3155 6770 3110 6770
Connection ~ 3110 6770
Wire Wire Line
	3110 6770 3110 6820
Text GLabel 3880 6250 2    50   Input ~ 0
3.3V
Wire Wire Line
	3880 6250 3815 6250
Connection ~ 3815 6250
Wire Wire Line
	3815 6250 3815 6305
Wire Wire Line
	7915 6080 7915 5980
Connection ~ 7915 5880
Connection ~ 7915 5980
Wire Wire Line
	7915 5980 7915 5880
Wire Wire Line
	3470 2615 3750 2615
Wire Wire Line
	3470 2415 3550 2415
Wire Wire Line
	3750 2165 3750 2615
Connection ~ 3750 2615
Wire Wire Line
	3750 2615 3810 2615
Wire Wire Line
	3470 2215 3810 2215
Wire Wire Line
	3470 2515 3650 2515
Wire Wire Line
	3650 2165 3650 2515
Connection ~ 3650 2515
Wire Wire Line
	3650 2515 3810 2515
Wire Wire Line
	3550 1815 3650 1815
Wire Wire Line
	3650 1865 3650 1815
Connection ~ 3650 1815
Wire Wire Line
	3550 2165 3550 2415
Connection ~ 3550 2415
Wire Wire Line
	3550 2415 3810 2415
Wire Wire Line
	3450 2165 3450 2315
Wire Wire Line
	3450 2315 3470 2315
Connection ~ 3470 2315
Wire Wire Line
	3470 2315 3810 2315
$Comp
L Device:R R?
U 1 1 5FB76DFF
P 3750 2015
AR Path="/5FB76DFF" Ref="R?"  Part="1" 
AR Path="/5FB2AE1D/5FB76DFF" Ref="R23"  Part="1" 
F 0 "R23" V 3730 2230 50  0000 L CNN
F 1 "TBD" V 3750 1950 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3680 2015 50  0001 C CNN
F 3 "~" H 3750 2015 50  0001 C CNN
	1    3750 2015
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 1815 3750 1865
Wire Wire Line
	3650 1815 3750 1815
$Comp
L SU_connectors:504050-0291 J?
U 1 1 5FB76E9F
P 5250 6700
AR Path="/5FB76E9F" Ref="J?"  Part="1" 
AR Path="/5FB2AE1D/5FB76E9F" Ref="J9"  Part="1" 
F 0 "J9" H 5650 6965 50  0000 C CNN
F 1 "504050-0291" H 5650 6874 50  0000 C CNN
F 2 "SU_connectors:5040500291" H 5900 6800 50  0001 L CNN
F 3 "https://www.molex.com/pdm_docs/sd/5040500291_sd.pdf" H 5900 6700 50  0001 L CNN
F 4 "Headers & Wire Housings 1.5MM PicoLock SMT HDR RA POS SIDLCK 2P" H 5900 6600 50  0001 L CNN "Description"
F 5 "2.15" H 5900 6500 50  0001 L CNN "Height"
F 6 "538-504050-0291" H 5900 6400 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Molex/504050-0291?qs=d9U39LAeJF1REevhxxcTiQ%3D%3D" H 5900 6300 50  0001 L CNN "Mouser Price/Stock"
F 8 "Molex" H 5900 6200 50  0001 L CNN "Manufacturer_Name"
F 9 "504050-0291" H 5900 6100 50  0001 L CNN "Manufacturer_Part_Number"
	1    5250 6700
	1    0    0    -1  
$EndComp
Text HLabel 5250 6150 0    50   BiDi ~ 0
CAN1_P
Text HLabel 5250 6250 0    50   BiDi ~ 0
CAN1_N
Text HLabel 5250 6700 0    50   BiDi ~ 0
CAN2_P
Text HLabel 5250 6800 0    50   BiDi ~ 0
CAN2_N
NoConn ~ 6750 1350
NoConn ~ 6750 1450
NoConn ~ 9300 2000
NoConn ~ 9300 2100
NoConn ~ 9350 3400
NoConn ~ 9350 3500
NoConn ~ 6650 2750
NoConn ~ 6650 2850
NoConn ~ 7670 4305
NoConn ~ 7670 4205
NoConn ~ 7665 5030
NoConn ~ 7665 4930
$Comp
L SU_connectors:504050-0291 J?
U 1 1 5FB76E8F
P 5250 6150
AR Path="/5FB76E8F" Ref="J?"  Part="1" 
AR Path="/5FB2AE1D/5FB76E8F" Ref="J8"  Part="1" 
F 0 "J8" H 5650 6415 50  0000 C CNN
F 1 "504050-0291" H 5650 6324 50  0000 C CNN
F 2 "SU_connectors:5040500291" H 5900 6250 50  0001 L CNN
F 3 "https://www.molex.com/pdm_docs/sd/5040500291_sd.pdf" H 5900 6150 50  0001 L CNN
F 4 "Headers & Wire Housings 1.5MM PicoLock SMT HDR RA POS SIDLCK 2P" H 5900 6050 50  0001 L CNN "Description"
F 5 "2.15" H 5900 5950 50  0001 L CNN "Height"
F 6 "538-504050-0291" H 5900 5850 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Molex/504050-0291?qs=d9U39LAeJF1REevhxxcTiQ%3D%3D" H 5900 5750 50  0001 L CNN "Mouser Price/Stock"
F 8 "Molex" H 5900 5650 50  0001 L CNN "Manufacturer_Name"
F 9 "504050-0291" H 5900 5550 50  0001 L CNN "Manufacturer_Part_Number"
	1    5250 6150
	1    0    0    -1  
$EndComp
NoConn ~ 2310 6620
NoConn ~ 2310 6720
NoConn ~ 3015 6205
NoConn ~ 3015 6105
NoConn ~ 6050 6250
NoConn ~ 6050 6150
NoConn ~ 6050 6800
NoConn ~ 6050 6700
NoConn ~ 2570 2215
NoConn ~ 2570 2315
$Comp
L power:GND #PWR?
U 1 1 62C237D2
P 4200 5500
AR Path="/62C237D2" Ref="#PWR?"  Part="1" 
AR Path="/5FB2AE1D/62C237D2" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4200 5250 50  0001 C CNN
F 1 "GND" H 4205 5327 50  0000 C CNN
F 2 "" H 4200 5500 50  0001 C CNN
F 3 "" H 4200 5500 50  0001 C CNN
	1    4200 5500
	0    1    1    0   
$EndComp
$EndSCHEMATC
