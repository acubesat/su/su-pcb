EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 14 14
Title "Science Unit Main Board (Payload Board)"
Date "2022-03-13"
Rev "2.0 (EM)"
Comp "SpaceDot"
Comment1 "AcubeSAT"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR?
U 1 1 5FC61012
P 4600 4200
AR Path="/5FC61012" Ref="#PWR?"  Part="1" 
AR Path="/5FC54D9F/5FC61012" Ref="#PWR061"  Part="1" 
F 0 "#PWR061" H 4600 3950 50  0001 C CNN
F 1 "GND" V 4605 4072 50  0000 R CNN
F 2 "" H 4600 4200 50  0001 C CNN
F 3 "" H 4600 4200 50  0001 C CNN
	1    4600 4200
	0    1    1    0   
$EndComp
Wire Wire Line
	4900 3400 5250 3400
$Comp
L power:GND #PWR?
U 1 1 5FC61025
P 4900 3400
AR Path="/5FC61025" Ref="#PWR?"  Part="1" 
AR Path="/5FC54D9F/5FC61025" Ref="#PWR060"  Part="1" 
F 0 "#PWR060" H 4900 3150 50  0001 C CNN
F 1 "GND" V 4905 3272 50  0000 R CNN
F 2 "" H 4900 3400 50  0001 C CNN
F 3 "" H 4900 3400 50  0001 C CNN
	1    4900 3400
	0    1    1    0   
$EndComp
$Comp
L SamacSys_Parts:TCAN337GDCNR IC?
U 1 1 5FC61031
P 5250 4300
AR Path="/5FC61031" Ref="IC?"  Part="1" 
AR Path="/5FC54D9F/5FC61031" Ref="IC7"  Part="1" 
F 0 "IC7" H 5800 4565 50  0000 C CNN
F 1 "TCAN337GD" H 5800 4474 50  0000 C CNN
F 2 "Components:SOIC127P600X175-8N" H 6200 4400 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/tcan337gd/texas-instruments" H 6200 4300 50  0001 L CNN
F 4 "CAN Interface IC" H 6200 4200 50  0001 L CNN "Description"
F 5 "1.75" H 6200 4100 50  0001 L CNN "Height"
F 6 "595-TCAN337GD" H 6200 4000 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/TCAN337GD?qs=gMfM3zrbS4qmjQESf%252BTgJw%3D%3D" H 6200 3900 50  0001 L CNN "Mouser Price/Stock"
F 8 "Texas Instruments" H 6200 3800 50  0001 L CNN "Manufacturer_Name"
F 9 "TCAN337GD" H 6200 3700 50  0001 L CNN "Manufacturer_Part_Number"
	1    5250 4300
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:TCAN337GDCNR IC?
U 1 1 5FC6103D
P 5250 3300
AR Path="/5FC6103D" Ref="IC?"  Part="1" 
AR Path="/5FC54D9F/5FC6103D" Ref="IC6"  Part="1" 
F 0 "IC6" H 5800 3565 50  0000 C CNN
F 1 "TCAN337GD" H 5800 3474 50  0000 C CNN
F 2 "Components:SOIC127P600X175-8N" H 6200 3400 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/tcan337gd/texas-instruments" H 6200 3300 50  0001 L CNN
F 4 "CAN Interface IC" H 6200 3200 50  0001 L CNN "Description"
F 5 "1.75" H 6200 3100 50  0001 L CNN "Height"
F 6 "595-TCAN337GD" H 6200 3000 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/TCAN337GD?qs=gMfM3zrbS4qmjQESf%252BTgJw%3D%3D" H 6200 2900 50  0001 L CNN "Mouser Price/Stock"
F 8 "Texas Instruments" H 6200 2800 50  0001 L CNN "Manufacturer_Name"
F 9 "TCAN337GD" H 6200 2700 50  0001 L CNN "Manufacturer_Part_Number"
	1    5250 3300
	1    0    0    -1  
$EndComp
Text HLabel 6350 3300 2    50   Input ~ 0
CAN_SILENT_1
Text HLabel 6350 3600 2    50   Output ~ 0
CAN_FAULT_1
Text HLabel 5250 3600 0    50   Input ~ 0
CAN_RX_1
Text HLabel 5250 3300 0    50   Output ~ 0
CAN_TX_1
Text HLabel 5250 4300 0    50   Output ~ 0
CAN_TX_2
Text HLabel 6350 4300 2    50   Input ~ 0
CAN_SILENT_2
Text HLabel 6350 4600 2    50   Output ~ 0
CAN_FAULT_2
$Comp
L Device:R R36
U 1 1 5F6FCF00
P 7150 3450
F 0 "R36" H 7220 3496 50  0000 L CNN
F 1 "120" H 7220 3405 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7080 3450 50  0001 C CNN
F 3 "~" H 7150 3450 50  0001 C CNN
	1    7150 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R37
U 1 1 5F6FD8A5
P 7150 4450
F 0 "R37" H 7220 4496 50  0000 L CNN
F 1 "120" H 7220 4405 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7080 4450 50  0001 C CNN
F 3 "~" H 7150 4450 50  0001 C CNN
	1    7150 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 4400 7000 4400
Wire Wire Line
	7000 4400 7000 4300
Wire Wire Line
	7000 4300 7150 4300
Wire Wire Line
	6350 4500 7000 4500
Wire Wire Line
	7000 4500 7000 4600
Wire Wire Line
	7000 4600 7150 4600
Wire Wire Line
	6350 3400 7000 3400
Wire Wire Line
	7000 3400 7000 3300
Wire Wire Line
	7000 3300 7150 3300
Wire Wire Line
	6350 3500 7000 3500
Wire Wire Line
	7000 3500 7000 3600
Wire Wire Line
	7000 3600 7150 3600
$Comp
L Device:C C7
U 1 1 5F7CAA78
P 4600 4350
F 0 "C7" H 4400 4400 50  0000 L CNN
F 1 "0.1u" H 4350 4350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4638 4200 50  0001 C CNN
F 3 "~" H 4600 4350 50  0001 C CNN
	1    4600 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5F7CB28F
P 4050 4000
F 0 "C8" H 4165 4046 50  0000 L CNN
F 1 "0.1u" H 4165 3955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4088 3850 50  0001 C CNN
F 3 "~" H 4050 4000 50  0001 C CNN
	1    4050 4000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F7CB805
P 4050 4250
AR Path="/5F7CB805" Ref="#PWR?"  Part="1" 
AR Path="/5FC54D9F/5F7CB805" Ref="#PWR059"  Part="1" 
F 0 "#PWR059" H 4050 4000 50  0001 C CNN
F 1 "GND" V 4055 4122 50  0000 R CNN
F 2 "" H 4050 4250 50  0001 C CNN
F 3 "" H 4050 4250 50  0001 C CNN
	1    4050 4250
	1    0    0    -1  
$EndComp
Text HLabel 5250 3500 0    50   Input ~ 0
lclprotecteddvc3
Text HLabel 4050 3750 0    50   Input ~ 0
lclprotecteddvc3
Text GLabel 4600 4500 0    50   Input ~ 0
3.3V
Wire Wire Line
	5250 4500 4600 4500
Wire Wire Line
	4750 4400 4750 4200
Wire Wire Line
	4750 4200 4600 4200
Wire Wire Line
	4750 4400 5250 4400
Connection ~ 4600 4200
Wire Wire Line
	4050 4150 4050 4250
Wire Wire Line
	4050 3750 4050 3850
Text Label 7150 3300 0    50   ~ 0
CAN_LOW_1
Text Label 7150 3600 0    50   ~ 0
CAN_HIGH_1
Text Label 7150 4600 0    50   ~ 0
CAN_HIGH_2
Text Label 7150 4300 0    50   ~ 0
CAN_LOW_2
Text HLabel 7150 3600 3    50   BiDi ~ 0
CAN1_P
Text HLabel 7150 3300 1    50   BiDi ~ 0
CAN1_N
Text HLabel 7150 4600 3    50   BiDi ~ 0
CAN2_P
Text HLabel 7150 4300 1    50   BiDi ~ 0
CAN2_N
Text Notes 4825 4240 0    50   ~ 0
CAN_2_P
Text HLabel 5250 4600 0    50   Input ~ 0
CAN_RX_2
Text Notes 4970 4835 0    50   ~ 0
CAN_2_N
$EndSCHEMATC
