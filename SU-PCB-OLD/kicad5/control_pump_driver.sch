EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 14
Title "Science Unit Main Board (Payload Board)"
Date "2022-03-13"
Rev "2.0 (EM)"
Comp "SpaceDot"
Comment1 "AcubeSAT"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Components:ZXMHC3A01T8TA Q?
U 1 1 60016750
P 4400 5100
AR Path="/60016750" Ref="Q?"  Part="1" 
AR Path="/5FFAB9B8/60016750" Ref="Q?"  Part="1" 
AR Path="/6009AF74/60016750" Ref="Q14"  Part="1" 
F 0 "Q14" H 5100 5365 50  0000 C CNN
F 1 "ZXMHC3A01T8TA" H 5100 5274 50  0000 C CNN
F 2 "Components:SOT153P700X170-8N" H 5650 5200 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ZXMHC3A01T8TA.pdf" H 5650 5100 50  0001 L CNN
F 4 "DIODES INC. - ZXMHC3A01T8TA - Dual MOSFET, Enhancement Mode, N and P Channel, 3.1 A, 30 V, 0.12 ohm, 10 V, 1 V" H 5650 5000 50  0001 L CNN "Description"
F 5 "1.7" H 5650 4900 50  0001 L CNN "Height"
F 6 "522-ZXMHC3A01T8TA" H 5650 4800 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Diodes-Incorporated/ZXMHC3A01T8TA?qs=Xl4UVSSad%2Fqyu39NiNDXJg%3D%3D" H 5650 4700 50  0001 L CNN "Mouser Price/Stock"
F 8 "Diodes Inc." H 5650 4600 50  0001 L CNN "Manufacturer_Name"
F 9 "ZXMHC3A01T8TA" H 5650 4500 50  0001 L CNN "Manufacturer_Part_Number"
	1    4400 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3450 5700 3450
Wire Wire Line
	5700 3450 5700 3300
Connection ~ 5700 3150
Wire Wire Line
	5600 3150 5700 3150
Wire Wire Line
	5700 3150 5700 2850
Wire Wire Line
	5600 2850 5700 2850
Connection ~ 5700 3450
Wire Wire Line
	5700 3750 5700 3450
Wire Wire Line
	5600 3750 5700 3750
$Comp
L Device:R R?
U 1 1 60047865
P 5450 2850
AR Path="/60047865" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/60047865" Ref="R?"  Part="1" 
AR Path="/6009AF74/60047865" Ref="R15"  Part="1" 
F 0 "R15" H 5520 2896 50  0000 L CNN
F 1 "10k" H 5520 2805 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5380 2850 50  0001 C CNN
F 3 "~" H 5450 2850 50  0001 C CNN
	1    5450 2850
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 6004786B
P 5450 3150
AR Path="/6004786B" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/6004786B" Ref="R?"  Part="1" 
AR Path="/6009AF74/6004786B" Ref="R16"  Part="1" 
F 0 "R16" H 5520 3196 50  0000 L CNN
F 1 "10k" H 5520 3105 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5380 3150 50  0001 C CNN
F 3 "~" H 5450 3150 50  0001 C CNN
	1    5450 3150
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 60047872
P 5450 3450
AR Path="/60047872" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/60047872" Ref="R?"  Part="1" 
AR Path="/6009AF74/60047872" Ref="R17"  Part="1" 
F 0 "R17" H 5520 3496 50  0000 L CNN
F 1 "10k" H 5520 3405 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5380 3450 50  0001 C CNN
F 3 "~" H 5450 3450 50  0001 C CNN
	1    5450 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 60047879
P 5450 3750
AR Path="/60047879" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/60047879" Ref="R?"  Part="1" 
AR Path="/6009AF74/60047879" Ref="R18"  Part="1" 
F 0 "R18" H 5520 3796 50  0000 L CNN
F 1 "10k" H 5520 3705 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5380 3750 50  0001 C CNN
F 3 "~" H 5450 3750 50  0001 C CNN
	1    5450 3750
	0    -1   -1   0   
$EndComp
Connection ~ 5700 3300
Wire Wire Line
	5700 3300 5700 3150
Text HLabel 5300 3750 0    50   Input ~ 0
CONTROL_PUMP_PWM_B2
Text HLabel 5300 3450 0    50   Input ~ 0
CONTROL_PUMP_PWM_B1
Text HLabel 5300 3150 0    50   Input ~ 0
CONTROL_PUMP_PWM_A2
Text HLabel 5300 2850 0    50   Input ~ 0
CONTROL_PUMP_PWM_A1
$Comp
L Components:ZXMHC3A01T8TA Q?
U 1 1 60016794
P 4400 1800
AR Path="/60016794" Ref="Q?"  Part="1" 
AR Path="/5FFAB9B8/60016794" Ref="Q?"  Part="1" 
AR Path="/6009AF74/60016794" Ref="Q13"  Part="1" 
F 0 "Q13" H 5100 2065 50  0000 C CNN
F 1 "ZXMHC3A01T8TA" H 5100 1974 50  0000 C CNN
F 2 "Components:SOT153P700X170-8N" H 5650 1900 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/ZXMHC3A01T8TA.pdf" H 5650 1800 50  0001 L CNN
F 4 "DIODES INC. - ZXMHC3A01T8TA - Dual MOSFET, Enhancement Mode, N and P Channel, 3.1 A, 30 V, 0.12 ohm, 10 V, 1 V" H 5650 1700 50  0001 L CNN "Description"
F 5 "1.7" H 5650 1600 50  0001 L CNN "Height"
F 6 "522-ZXMHC3A01T8TA" H 5650 1500 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Diodes-Incorporated/ZXMHC3A01T8TA?qs=Xl4UVSSad%2Fqyu39NiNDXJg%3D%3D" H 5650 1400 50  0001 L CNN "Mouser Price/Stock"
F 8 "Diodes Inc." H 5650 1300 50  0001 L CNN "Manufacturer_Name"
F 9 "ZXMHC3A01T8TA" H 5650 1200 50  0001 L CNN "Manufacturer_Part_Number"
	1    4400 1800
	1    0    0    -1  
$EndComp
Text Notes 6060 3065 0    50   ~ 0
pull-up those to 2.2K
Text Notes 3690 2790 0    50   ~ 0
out0 -a1
Text Notes 3760 2970 0    50   ~ 0
out1 -a2
Text Notes 3760 3205 0    50   ~ 0
out2 -b1
Text Notes 3775 3545 0    50   ~ 0
out3 -b2
Text GLabel 6200 3300 2    50   Input ~ 0
5V
Text Notes 6400 3200 0    50   ~ 0
or 5V
$Comp
L power:GND #PWR?
U 1 1 62CDA337
P 4050 1900
AR Path="/62CDA337" Ref="#PWR?"  Part="1" 
AR Path="/5FFAB9B8/62CDA337" Ref="#PWR?"  Part="1" 
AR Path="/6009AF74/62CDA337" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4050 1650 50  0001 C CNN
F 1 "GND" H 4055 1727 50  0000 C CNN
F 2 "" H 4050 1900 50  0001 C CNN
F 3 "" H 4050 1900 50  0001 C CNN
	1    4050 1900
	0    1    1    0   
$EndComp
$Comp
L Components:NTK3134NT1G Q?
U 1 1 62CDA344
P 2900 1800
AR Path="/5F938AED/62CDA344" Ref="Q?"  Part="1" 
AR Path="/5FFAB9B8/62CDA344" Ref="Q?"  Part="1" 
AR Path="/6009AF74/62CDA344" Ref="Q15"  Part="1" 
F 0 "Q15" H 3330 1946 50  0000 L CNN
F 1 "FDV304P" H 3330 1855 50  0000 L CNN
F 2 "Components:FDV304P" H 3350 1750 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 3350 1650 50  0001 L CNN
F 4 "Power MOSFET" H 3350 1550 50  0001 L CNN "Description"
F 5 "0.55" H 3350 1450 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 3350 1350 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 3350 1250 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 3350 1150 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 3350 1050 50  0001 L CNN "Manufacturer_Part_Number"
	1    2900 1800
	1    0    0    -1  
$EndComp
$Comp
L Components:NTK3134NT1G Q?
U 1 1 62CDA350
P 4100 1500
AR Path="/5F938AED/62CDA350" Ref="Q?"  Part="1" 
AR Path="/5FFAB9B8/62CDA350" Ref="Q?"  Part="1" 
AR Path="/6009AF74/62CDA350" Ref="Q17"  Part="1" 
F 0 "Q17" H 4530 1646 50  0000 L CNN
F 1 "FDV304P" H 4530 1555 50  0000 L CNN
F 2 "Components:FDV304P" H 4550 1450 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 4550 1350 50  0001 L CNN
F 4 "Power MOSFET" H 4550 1250 50  0001 L CNN "Description"
F 5 "0.55" H 4550 1150 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 4550 1050 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 4550 950 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 4550 850 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 4550 750 50  0001 L CNN "Manufacturer_Part_Number"
	1    4100 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 62CDA357
P 2650 1800
AR Path="/62CDA357" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/62CDA357" Ref="R?"  Part="1" 
AR Path="/6009AF74/62CDA357" Ref="R?"  Part="1" 
F 0 "R?" H 2720 1846 50  0000 L CNN
F 1 "1k" H 2720 1755 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2580 1800 50  0001 C CNN
F 3 "~" H 2650 1800 50  0001 C CNN
	1    2650 1800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 62CDA35D
P 3850 1500
AR Path="/62CDA35D" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/62CDA35D" Ref="R?"  Part="1" 
AR Path="/6009AF74/62CDA35D" Ref="R?"  Part="1" 
F 0 "R?" H 3920 1546 50  0000 L CNN
F 1 "1k" H 3920 1455 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3780 1500 50  0001 C CNN
F 3 "~" H 3850 1500 50  0001 C CNN
	1    3850 1500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2900 1800 2800 1800
Wire Wire Line
	4000 1500 4100 1500
Wire Wire Line
	4400 2000 3200 2000
Wire Wire Line
	4400 1800 4400 1700
$Comp
L Device:R R?
U 1 1 62CDA367
P 3200 2150
AR Path="/62CDA367" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/62CDA367" Ref="R?"  Part="1" 
AR Path="/6009AF74/62CDA367" Ref="R?"  Part="1" 
F 0 "R?" H 3270 2196 50  0000 L CNN
F 1 "10k" H 3270 2105 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3130 2150 50  0001 C CNN
F 3 "~" H 3200 2150 50  0001 C CNN
	1    3200 2150
	1    0    0    -1  
$EndComp
Connection ~ 3200 2000
$Comp
L power:GND #PWR?
U 1 1 62CDA36E
P 3200 2300
AR Path="/62CDA36E" Ref="#PWR?"  Part="1" 
AR Path="/5FFAB9B8/62CDA36E" Ref="#PWR?"  Part="1" 
AR Path="/6009AF74/62CDA36E" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3200 2050 50  0001 C CNN
F 1 "GND" H 3205 2127 50  0000 C CNN
F 2 "" H 3200 2300 50  0001 C CNN
F 3 "" H 3200 2300 50  0001 C CNN
	1    3200 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 62CDA374
P 4250 1800
AR Path="/62CDA374" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/62CDA374" Ref="R?"  Part="1" 
AR Path="/6009AF74/62CDA374" Ref="R?"  Part="1" 
F 0 "R?" V 4150 1700 50  0000 L CNN
F 1 "10k" V 4150 1800 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4180 1800 50  0001 C CNN
F 3 "~" H 4250 1800 50  0001 C CNN
	1    4250 1800
	0    1    1    0   
$EndComp
Wire Wire Line
	4100 1800 4100 1900
Wire Wire Line
	4100 1900 4050 1900
Wire Wire Line
	4100 1900 4400 1900
Connection ~ 4100 1900
Text Label 5700 1900 0    50   ~ 0
LOAD_SUPPLY
Text HLabel 6250 1900 2    50   Input ~ 0
VCC
Wire Wire Line
	5600 1900 6250 1900
Text GLabel 3200 1300 1    50   Input ~ 0
5V
Wire Wire Line
	3200 1300 3200 1400
Text GLabel 4400 1000 1    50   Input ~ 0
5V
Wire Wire Line
	4400 1000 4400 1100
Text HLabel 5600 2000 2    50   Output ~ 0
CONTROL_PUMP_PHASE_A+
Text HLabel 5600 1800 2    50   Output ~ 0
CONTROL_PUMP_PHASE_A-
Text HLabel 2500 1800 1    50   Input ~ 0
CONTROL_PUMP_PWM_A1
Text HLabel 4400 2100 0    50   Input ~ 0
CONTROL_PUMP_PWM_A2
Text HLabel 3700 1500 1    50   Input ~ 0
CONTROL_PUMP_PWM_A2
Text HLabel 5600 2100 2    50   Input ~ 0
CONTROL_PUMP_PWM_A1
Connection ~ 4400 1800
$Comp
L power:GND #PWR?
U 1 1 62CFD094
P 4050 5200
AR Path="/62CFD094" Ref="#PWR?"  Part="1" 
AR Path="/5FFAB9B8/62CFD094" Ref="#PWR?"  Part="1" 
AR Path="/6009AF74/62CFD094" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4050 4950 50  0001 C CNN
F 1 "GND" H 4055 5027 50  0000 C CNN
F 2 "" H 4050 5200 50  0001 C CNN
F 3 "" H 4050 5200 50  0001 C CNN
	1    4050 5200
	0    1    1    0   
$EndComp
$Comp
L Components:NTK3134NT1G Q?
U 1 1 62CFD0A0
P 2900 5100
AR Path="/5F938AED/62CFD0A0" Ref="Q?"  Part="1" 
AR Path="/5FFAB9B8/62CFD0A0" Ref="Q?"  Part="1" 
AR Path="/6009AF74/62CFD0A0" Ref="Q16"  Part="1" 
F 0 "Q16" H 3330 5246 50  0000 L CNN
F 1 "FDV304P" H 3330 5155 50  0000 L CNN
F 2 "Components:FDV304P" H 3350 5050 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 3350 4950 50  0001 L CNN
F 4 "Power MOSFET" H 3350 4850 50  0001 L CNN "Description"
F 5 "0.55" H 3350 4750 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 3350 4650 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 3350 4550 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 3350 4450 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 3350 4350 50  0001 L CNN "Manufacturer_Part_Number"
	1    2900 5100
	1    0    0    -1  
$EndComp
$Comp
L Components:NTK3134NT1G Q?
U 1 1 62CFD0AC
P 4100 4800
AR Path="/5F938AED/62CFD0AC" Ref="Q?"  Part="1" 
AR Path="/5FFAB9B8/62CFD0AC" Ref="Q?"  Part="1" 
AR Path="/6009AF74/62CFD0AC" Ref="Q18"  Part="1" 
F 0 "Q18" H 4530 4946 50  0000 L CNN
F 1 "FDV304P" H 4530 4855 50  0000 L CNN
F 2 "Components:FDV304P" H 4550 4750 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 4550 4650 50  0001 L CNN
F 4 "Power MOSFET" H 4550 4550 50  0001 L CNN "Description"
F 5 "0.55" H 4550 4450 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 4550 4350 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 4550 4250 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 4550 4150 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 4550 4050 50  0001 L CNN "Manufacturer_Part_Number"
	1    4100 4800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 62CFD0B2
P 2650 5100
AR Path="/62CFD0B2" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/62CFD0B2" Ref="R?"  Part="1" 
AR Path="/6009AF74/62CFD0B2" Ref="R?"  Part="1" 
F 0 "R?" H 2720 5146 50  0000 L CNN
F 1 "1k" H 2720 5055 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2580 5100 50  0001 C CNN
F 3 "~" H 2650 5100 50  0001 C CNN
	1    2650 5100
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 62CFD0B8
P 3850 4800
AR Path="/62CFD0B8" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/62CFD0B8" Ref="R?"  Part="1" 
AR Path="/6009AF74/62CFD0B8" Ref="R?"  Part="1" 
F 0 "R?" H 3920 4846 50  0000 L CNN
F 1 "1k" H 3920 4755 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3780 4800 50  0001 C CNN
F 3 "~" H 3850 4800 50  0001 C CNN
	1    3850 4800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2900 5100 2800 5100
Wire Wire Line
	4000 4800 4100 4800
Wire Wire Line
	4400 5300 3200 5300
Wire Wire Line
	4400 5100 4400 5000
$Comp
L Device:R R?
U 1 1 62CFD0C2
P 3200 5450
AR Path="/62CFD0C2" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/62CFD0C2" Ref="R?"  Part="1" 
AR Path="/6009AF74/62CFD0C2" Ref="R?"  Part="1" 
F 0 "R?" H 3270 5496 50  0000 L CNN
F 1 "10k" H 3270 5405 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3130 5450 50  0001 C CNN
F 3 "~" H 3200 5450 50  0001 C CNN
	1    3200 5450
	1    0    0    -1  
$EndComp
Connection ~ 3200 5300
$Comp
L power:GND #PWR?
U 1 1 62CFD0C9
P 3200 5600
AR Path="/62CFD0C9" Ref="#PWR?"  Part="1" 
AR Path="/5FFAB9B8/62CFD0C9" Ref="#PWR?"  Part="1" 
AR Path="/6009AF74/62CFD0C9" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3200 5350 50  0001 C CNN
F 1 "GND" H 3205 5427 50  0000 C CNN
F 2 "" H 3200 5600 50  0001 C CNN
F 3 "" H 3200 5600 50  0001 C CNN
	1    3200 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 62CFD0CF
P 4250 5100
AR Path="/62CFD0CF" Ref="R?"  Part="1" 
AR Path="/5FFAB9B8/62CFD0CF" Ref="R?"  Part="1" 
AR Path="/6009AF74/62CFD0CF" Ref="R?"  Part="1" 
F 0 "R?" V 4150 5000 50  0000 L CNN
F 1 "10k" V 4150 5100 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4180 5100 50  0001 C CNN
F 3 "~" H 4250 5100 50  0001 C CNN
	1    4250 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	4100 5100 4100 5200
Wire Wire Line
	4100 5200 4050 5200
Wire Wire Line
	4100 5200 4400 5200
Connection ~ 4100 5200
Text Label 5700 5200 0    50   ~ 0
LOAD_SUPPLY
Text HLabel 6250 5200 2    50   Input ~ 0
VCC
Wire Wire Line
	5600 5200 6250 5200
Text GLabel 3200 4600 1    50   Input ~ 0
5V
Wire Wire Line
	3200 4600 3200 4700
Text GLabel 4400 4300 1    50   Input ~ 0
5V
Wire Wire Line
	4400 4300 4400 4400
Text HLabel 5600 5300 2    50   Output ~ 0
CONTROL_PUMP_PHASE_B+
Text HLabel 5600 5100 2    50   Output ~ 0
CONTROL_PUMP_PHASE_B-
Text HLabel 2500 5100 1    50   Input ~ 0
CONTROL_PUMP_PWM_B1
Text HLabel 4400 5400 0    50   Input ~ 0
CONTROL_PUMP_PWM_B2
Text HLabel 3700 4800 1    50   Input ~ 0
CONTROL_PUMP_PWM_B2
Text HLabel 5600 5400 2    50   Input ~ 0
CONTROL_PUMP_PWM_B1
Wire Wire Line
	5700 3300 6200 3300
Connection ~ 4400 5100
$EndSCHEMATC
