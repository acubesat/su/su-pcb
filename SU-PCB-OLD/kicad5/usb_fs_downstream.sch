EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 14
Title "Science Unit Main Board (Payload Board)"
Date "2022-03-13"
Rev "2.0 (EM)"
Comp "SpaceDot"
Comment1 "AcubeSAT"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR?
U 1 1 5F78DCA0
P 4650 4400
AR Path="/5F78DCA0" Ref="#PWR?"  Part="1" 
AR Path="/5F784C18/5F78DCA0" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 4650 4150 50  0001 C CNN
F 1 "GND" V 4650 4300 50  0000 R CNN
F 2 "" H 4650 4400 50  0001 C CNN
F 3 "" H 4650 4400 50  0001 C CNN
	1    4650 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	4400 4300 4650 4300
Wire Wire Line
	4400 4500 4650 4500
$Comp
L Components:USBLC6-2P6 IC?
U 1 1 5F78DCB6
P 4650 4300
AR Path="/5F78DCB6" Ref="IC?"  Part="1" 
AR Path="/5F784C18/5F78DCB6" Ref="IC8"  Part="1" 
F 0 "IC8" H 5250 4565 50  0000 C CNN
F 1 "USBLC6-2M6" H 5250 4474 50  0000 C CNN
F 2 "SamacSys_Parts:SON50P100X145X60-6N-D" H 5700 4400 50  0001 L CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00050750.pdf" H 5700 4300 50  0001 L CNN
F 4 "TVS Diode Array Uni-Directional USBLC6-2P6 17V, SOT-666 6-Pin" H 5700 4200 50  0001 L CNN "Description"
F 5 "0.6" H 5700 4100 50  0001 L CNN "Height"
F 6 "STMicroelectronics" H 5700 4000 50  0001 L CNN "Manufacturer_Name"
F 7 "USBLC6-2P6" H 5700 3900 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "511-USBLC6-2P6" H 5700 3800 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=511-USBLC6-2P6" H 5700 3700 50  0001 L CNN "Mouser Price/Stock"
F 10 "6247687P" H 5700 3600 50  0001 L CNN "RS Part Number"
F 11 "http://uk.rs-online.com/web/p/products/6247687P" H 5700 3500 50  0001 L CNN "RS Price/Stock"
F 12 "70389688" H 5700 3400 50  0001 L CNN "Allied_Number"
F 13 "http://www.alliedelec.com/stmicroelectronics-usblc6-2p6/70389688/" H 5700 3300 50  0001 L CNN "Allied Price/Stock"
	1    4650 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 4400 6950 4400
Text Notes 4900 4700 0    50   ~ 0
ESD Protection IC
$Comp
L power:GND #PWR?
U 1 1 5F78DCE0
P 7600 4100
AR Path="/5F78DCE0" Ref="#PWR?"  Part="1" 
AR Path="/5F784C18/5F78DCE0" Ref="#PWR0117"  Part="1" 
F 0 "#PWR0117" H 7600 3850 50  0001 C CNN
F 1 "GND" V 7600 4000 50  0000 R CNN
F 2 "" H 7600 4100 50  0001 C CNN
F 3 "" H 7600 4100 50  0001 C CNN
	1    7600 4100
	0    1    1    0   
$EndComp
Text HLabel 4400 4500 0    50   BiDi ~ 0
USB_D-
Text HLabel 4400 4300 0    50   BiDi ~ 0
USB_D+
Wire Wire Line
	5850 4500 7550 4500
Text Label 6300 4500 0    50   ~ 0
USB_DEVICE_D-
Text Label 6300 4300 0    50   ~ 0
USB_DEVICE_D+
Text Notes 5045 3700 0    50   ~ 0
remove OCP IC and connecto usb_en to lcl set..Maybe use usb_fault as lcl reset
Connection ~ 6950 4400
Wire Wire Line
	6950 4150 6950 4400
Text HLabel 6950 4150 0    50   Input ~ 0
lclprotecteddvc2
Text Notes 6640 3820 0    50   ~ 0
OK
Wire Wire Line
	6950 4400 8115 4400
Wire Wire Line
	7600 4100 8115 4100
Wire Wire Line
	5850 4300 8115 4300
Wire Wire Line
	7550 4500 7550 4200
Wire Wire Line
	7550 4200 8115 4200
$Comp
L Components:203556-0407 J?
U 1 1 5F78DCCF
P 8915 4400
AR Path="/5F78DCCF" Ref="J?"  Part="1" 
AR Path="/5F784C18/5F78DCCF" Ref="J17"  Part="1" 
F 0 "J17" H 9365 4665 50  0000 C CNN
F 1 "2035560-407" H 9365 4574 50  0000 C CNN
F 2 "Components:2035560407" H 9665 4500 50  0001 L CNN
F 3 "https://www.molex.com/pdm_docs/sd/5040500591_sd.pdf" H 9665 4400 50  0001 L CNN
F 4 "Headers & Wire Housings Pico-Lock 1.5 SMT HDR RA POSLCK 5CKT" H 9665 4300 50  0001 L CNN "Description"
F 5 "2.15" H 9665 4200 50  0001 L CNN "Height"
F 6 "538-504050-0591" H 9665 4100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Molex/504050-0591?qs=OAhjpuo3Vu7FoUIT4KH7%2Fg%3D%3D" H 9665 4000 50  0001 L CNN "Mouser Price/Stock"
F 8 "Molex" H 9665 3900 50  0001 L CNN "Manufacturer_Name"
F 9 "504050-0591" H 9665 3800 50  0001 L CNN "Manufacturer_Part_Number"
	1    8915 4400
	-1   0    0    1   
$EndComp
NoConn ~ 8915 4300
NoConn ~ 8915 4400
$EndSCHEMATC
