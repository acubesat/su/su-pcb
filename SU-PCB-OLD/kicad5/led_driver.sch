EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 14
Title "Science Unit Main Board (Payload Board)"
Date "2022-03-13"
Rev "2.0 (EM)"
Comp "SpaceDot"
Comment1 "AcubeSAT"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 4650 3300 0    50   Input ~ 0
LED_1_SIGNAL
Text HLabel 4650 4450 0    50   Input ~ 0
LED_2_SIGNAL
Text HLabel 6400 3300 0    50   Input ~ 0
LED_3_SIGNAL
Text HLabel 6400 4450 0    50   Input ~ 0
LED_4_SIGNAL
Text HLabel 5100 4050 2    50   Output ~ 0
LED_CONTROL_2
Text HLabel 6850 2900 2    50   Output ~ 0
LED_CONTROL_3
Text HLabel 6850 4050 2    50   Output ~ 0
LED_CONTROL_4
$Comp
L Device:R R?
U 1 1 5F6B673C
P 4750 3550
AR Path="/5F938AED/5F6B673C" Ref="R?"  Part="1" 
AR Path="/5F714E00/5F6B673C" Ref="R80"  Part="1" 
F 0 "R80" H 4820 3596 50  0000 L CNN
F 1 "10k" H 4820 3505 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4680 3550 50  0001 C CNN
F 3 "~" H 4750 3550 50  0001 C CNN
	1    4750 3550
	1    0    0    -1  
$EndComp
$Comp
L Components:NTK3134NT1G Q?
U 1 1 5F6B674A
P 4800 3300
AR Path="/5F938AED/5F6B674A" Ref="Q?"  Part="1" 
AR Path="/5F714E00/5F6B674A" Ref="Q45"  Part="1" 
F 0 "Q45" H 5230 3446 50  0000 L CNN
F 1 "NTK3134NT1G" H 5230 3355 50  0000 L CNN
F 2 "Components:NTK3134NT1G" H 5250 3250 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 5250 3150 50  0001 L CNN
F 4 "Power MOSFET" H 5250 3050 50  0001 L CNN "Description"
F 5 "0.55" H 5250 2950 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 5250 2850 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 5250 2750 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 5250 2650 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 5250 2550 50  0001 L CNN "Manufacturer_Part_Number"
	1    4800 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F6B6750
P 5100 3550
AR Path="/5F938AED/5F6B6750" Ref="#PWR?"  Part="1" 
AR Path="/5F714E00/5F6B6750" Ref="#PWR0108"  Part="1" 
F 0 "#PWR0108" H 5100 3300 50  0001 C CNN
F 1 "GND" H 5105 3377 50  0000 C CNN
F 2 "" H 5100 3550 50  0001 C CNN
F 3 "" H 5100 3550 50  0001 C CNN
	1    5100 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 3500 5100 3550
Wire Wire Line
	4650 3300 4750 3300
Wire Wire Line
	4750 3400 4750 3300
Connection ~ 4750 3300
Wire Wire Line
	4750 3300 4800 3300
$Comp
L power:GND #PWR?
U 1 1 5F6B675B
P 4750 3750
AR Path="/5F938AED/5F6B675B" Ref="#PWR?"  Part="1" 
AR Path="/5F714E00/5F6B675B" Ref="#PWR0106"  Part="1" 
F 0 "#PWR0106" H 4750 3500 50  0001 C CNN
F 1 "GND" H 4755 3577 50  0000 C CNN
F 2 "" H 4750 3750 50  0001 C CNN
F 3 "" H 4750 3750 50  0001 C CNN
	1    4750 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 3700 4750 3750
Text HLabel 5100 2900 2    50   Output ~ 0
LED_CONTROL_1
$Comp
L Device:R R?
U 1 1 5F6BE975
P 4750 4700
AR Path="/5F938AED/5F6BE975" Ref="R?"  Part="1" 
AR Path="/5F714E00/5F6BE975" Ref="R81"  Part="1" 
F 0 "R81" H 4820 4746 50  0000 L CNN
F 1 "10k" H 4820 4655 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4680 4700 50  0001 C CNN
F 3 "~" H 4750 4700 50  0001 C CNN
	1    4750 4700
	1    0    0    -1  
$EndComp
$Comp
L Components:NTK3134NT1G Q?
U 1 1 5F6BE981
P 4800 4450
AR Path="/5F938AED/5F6BE981" Ref="Q?"  Part="1" 
AR Path="/5F714E00/5F6BE981" Ref="Q46"  Part="1" 
F 0 "Q46" H 5230 4596 50  0000 L CNN
F 1 "NTK3134NT1G" H 5230 4505 50  0000 L CNN
F 2 "Components:NTK3134NT1G" H 5250 4400 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 5250 4300 50  0001 L CNN
F 4 "Power MOSFET" H 5250 4200 50  0001 L CNN "Description"
F 5 "0.55" H 5250 4100 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 5250 4000 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 5250 3900 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 5250 3800 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 5250 3700 50  0001 L CNN "Manufacturer_Part_Number"
	1    4800 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F6BE987
P 5100 4700
AR Path="/5F938AED/5F6BE987" Ref="#PWR?"  Part="1" 
AR Path="/5F714E00/5F6BE987" Ref="#PWR0109"  Part="1" 
F 0 "#PWR0109" H 5100 4450 50  0001 C CNN
F 1 "GND" H 5105 4527 50  0000 C CNN
F 2 "" H 5100 4700 50  0001 C CNN
F 3 "" H 5100 4700 50  0001 C CNN
	1    5100 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 4650 5100 4700
Wire Wire Line
	4650 4450 4750 4450
Wire Wire Line
	4750 4550 4750 4450
Connection ~ 4750 4450
Wire Wire Line
	4750 4450 4800 4450
$Comp
L power:GND #PWR?
U 1 1 5F6BE992
P 4750 4900
AR Path="/5F938AED/5F6BE992" Ref="#PWR?"  Part="1" 
AR Path="/5F714E00/5F6BE992" Ref="#PWR0107"  Part="1" 
F 0 "#PWR0107" H 4750 4650 50  0001 C CNN
F 1 "GND" H 4755 4727 50  0000 C CNN
F 2 "" H 4750 4900 50  0001 C CNN
F 3 "" H 4750 4900 50  0001 C CNN
	1    4750 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 4850 4750 4900
$Comp
L Device:R R?
U 1 1 5F6C3EB3
P 6500 3550
AR Path="/5F938AED/5F6C3EB3" Ref="R?"  Part="1" 
AR Path="/5F714E00/5F6C3EB3" Ref="R82"  Part="1" 
F 0 "R82" H 6570 3596 50  0000 L CNN
F 1 "10k" H 6570 3505 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6430 3550 50  0001 C CNN
F 3 "~" H 6500 3550 50  0001 C CNN
	1    6500 3550
	1    0    0    -1  
$EndComp
$Comp
L Components:NTK3134NT1G Q?
U 1 1 5F6C3EBF
P 6550 3300
AR Path="/5F938AED/5F6C3EBF" Ref="Q?"  Part="1" 
AR Path="/5F714E00/5F6C3EBF" Ref="Q47"  Part="1" 
F 0 "Q47" H 6980 3446 50  0000 L CNN
F 1 "NTK3134NT1G" H 6980 3355 50  0000 L CNN
F 2 "Components:NTK3134NT1G" H 7000 3250 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 7000 3150 50  0001 L CNN
F 4 "Power MOSFET" H 7000 3050 50  0001 L CNN "Description"
F 5 "0.55" H 7000 2950 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 7000 2850 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 7000 2750 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 7000 2650 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 7000 2550 50  0001 L CNN "Manufacturer_Part_Number"
	1    6550 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F6C3EC5
P 6850 3550
AR Path="/5F938AED/5F6C3EC5" Ref="#PWR?"  Part="1" 
AR Path="/5F714E00/5F6C3EC5" Ref="#PWR0112"  Part="1" 
F 0 "#PWR0112" H 6850 3300 50  0001 C CNN
F 1 "GND" H 6855 3377 50  0000 C CNN
F 2 "" H 6850 3550 50  0001 C CNN
F 3 "" H 6850 3550 50  0001 C CNN
	1    6850 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 3500 6850 3550
Wire Wire Line
	6400 3300 6500 3300
Wire Wire Line
	6500 3400 6500 3300
Connection ~ 6500 3300
Wire Wire Line
	6500 3300 6550 3300
$Comp
L power:GND #PWR?
U 1 1 5F6C3ED0
P 6500 3750
AR Path="/5F938AED/5F6C3ED0" Ref="#PWR?"  Part="1" 
AR Path="/5F714E00/5F6C3ED0" Ref="#PWR0110"  Part="1" 
F 0 "#PWR0110" H 6500 3500 50  0001 C CNN
F 1 "GND" H 6505 3577 50  0000 C CNN
F 2 "" H 6500 3750 50  0001 C CNN
F 3 "" H 6500 3750 50  0001 C CNN
	1    6500 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 3700 6500 3750
$Comp
L Device:R R?
U 1 1 5F6C3ED9
P 6500 4700
AR Path="/5F938AED/5F6C3ED9" Ref="R?"  Part="1" 
AR Path="/5F714E00/5F6C3ED9" Ref="R83"  Part="1" 
F 0 "R83" H 6570 4746 50  0000 L CNN
F 1 "10k" H 6570 4655 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6430 4700 50  0001 C CNN
F 3 "~" H 6500 4700 50  0001 C CNN
	1    6500 4700
	1    0    0    -1  
$EndComp
$Comp
L Components:NTK3134NT1G Q?
U 1 1 5F6C3EE5
P 6550 4450
AR Path="/5F938AED/5F6C3EE5" Ref="Q?"  Part="1" 
AR Path="/5F714E00/5F6C3EE5" Ref="Q48"  Part="1" 
F 0 "Q48" H 6980 4596 50  0000 L CNN
F 1 "NTK3134NT1G" H 6980 4505 50  0000 L CNN
F 2 "Components:NTK3134NT1G" H 7000 4400 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/NTK3134N-D.PDF" H 7000 4300 50  0001 L CNN
F 4 "Power MOSFET" H 7000 4200 50  0001 L CNN "Description"
F 5 "0.55" H 7000 4100 50  0001 L CNN "Height"
F 6 "863-NTK3134NT1G" H 7000 4000 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/NTK3134NT1G?qs=g97OBWmz8%2FXjV%2Fg5rjrz3Q%3D%3D" H 7000 3900 50  0001 L CNN "Mouser Price/Stock"
F 8 "ON Semiconductor" H 7000 3800 50  0001 L CNN "Manufacturer_Name"
F 9 "NTK3134NT1G" H 7000 3700 50  0001 L CNN "Manufacturer_Part_Number"
	1    6550 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F6C3EEB
P 6850 4700
AR Path="/5F938AED/5F6C3EEB" Ref="#PWR?"  Part="1" 
AR Path="/5F714E00/5F6C3EEB" Ref="#PWR0113"  Part="1" 
F 0 "#PWR0113" H 6850 4450 50  0001 C CNN
F 1 "GND" H 6855 4527 50  0000 C CNN
F 2 "" H 6850 4700 50  0001 C CNN
F 3 "" H 6850 4700 50  0001 C CNN
	1    6850 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 4650 6850 4700
Wire Wire Line
	6400 4450 6500 4450
Wire Wire Line
	6500 4550 6500 4450
Connection ~ 6500 4450
Wire Wire Line
	6500 4450 6550 4450
$Comp
L power:GND #PWR?
U 1 1 5F6C3EF6
P 6500 4900
AR Path="/5F938AED/5F6C3EF6" Ref="#PWR?"  Part="1" 
AR Path="/5F714E00/5F6C3EF6" Ref="#PWR0111"  Part="1" 
F 0 "#PWR0111" H 6500 4650 50  0001 C CNN
F 1 "GND" H 6505 4727 50  0000 C CNN
F 2 "" H 6500 4900 50  0001 C CNN
F 3 "" H 6500 4900 50  0001 C CNN
	1    6500 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 4850 6500 4900
$EndSCHEMATC
