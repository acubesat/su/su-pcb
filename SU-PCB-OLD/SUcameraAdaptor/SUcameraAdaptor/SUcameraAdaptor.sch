EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "SU Camera adaptor PCB"
Date "2022-06-03"
Rev "DRAFT"
Comp "SpaceDot - AcubeSAT"
Comment1 "Panagiotis Bountzioukas"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 502244-1530:502244-1530 J1
U 1 1 6299E70E
P 2200 1900
F 0 "J1" V 2019 1850 50  0000 C CNN
F 1 "502244-1530" V 2110 1850 50  0000 C CNN
F 2 "502244-1530:5022441530" H 4050 2200 50  0001 L CNN
F 3 "https://www.molex.com/pdm_docs/sd/5022441530_sd.pdf" H 4050 2100 50  0001 L CNN
F 4 "FFC & FPC Connectors 0.5 FFC For LVDS Ass ssy 15Ckt EmbsTp pkg" H 4050 2000 50  0001 L CNN "Description"
F 5 "2.33" H 4050 1900 50  0001 L CNN "Height"
F 6 "538-502244-1530" H 4050 1800 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Molex/502244-1530?qs=xbccQsLEe0cR3kvi57AeAw%3D%3D" H 4050 1700 50  0001 L CNN "Mouser Price/Stock"
F 8 "Molex" H 4050 1600 50  0001 L CNN "Manufacturer_Name"
F 9 "502244-1530" H 4050 1500 50  0001 L CNN "Manufacturer_Part_Number"
	1    2200 1900
	0    1    1    0   
$EndComp
NoConn ~ 2200 1900
NoConn ~ 1600 2800
NoConn ~ 1600 2900
NoConn ~ 2200 3900
$Comp
L 501568-0407:501568-0407 J2
U 1 1 629A34DA
P 3600 2700
F 0 "J2" H 4000 2965 50  0000 C CNN
F 1 "501568-0407" H 4000 2874 50  0000 C CNN
F 2 "501568-0407:501568-0407" H 4250 2800 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/501568-0407.pdf" H 4250 2700 50  0001 L CNN
F 4 "MOLEX - 501568-0407 - HEADER, 1.00MM, 4WAY" H 4250 2600 50  0001 L CNN "Description"
F 5 "" H 4250 2500 50  0001 L CNN "Height"
F 6 "538-501568-0407" H 4250 2400 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Molex/501568-0407?qs=uEWtSLL707XxbhccfDRWNQ%3D%3D" H 4250 2300 50  0001 L CNN "Mouser Price/Stock"
F 8 "Molex" H 4250 2200 50  0001 L CNN "Manufacturer_Name"
F 9 "501568-0407" H 4250 2100 50  0001 L CNN "Manufacturer_Part_Number"
	1    3600 2700
	1    0    0    -1  
$EndComp
Text Label 4400 2800 0    50   ~ 0
5V
Text Label 3600 2700 2    50   ~ 0
GND
Text Label 3600 2800 2    50   ~ 0
D+
Text Label 4400 2700 0    50   ~ 0
D-
Text Label 2700 2500 0    50   ~ 0
5V
Text Label 2700 3600 0    50   ~ 0
GND
$Comp
L Device:R_Small R1
U 1 1 629A5EAB
P 2850 3000
F 0 "R1" H 2909 3046 50  0000 L CNN
F 1 "5.1k" H 2909 2955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" H 2850 3000 50  0001 C CNN
F 3 "~" H 2850 3000 50  0001 C CNN
	1    2850 3000
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R2
U 1 1 629A7167
P 2850 2700
F 0 "R2" H 2909 2746 50  0000 L CNN
F 1 "5.1k" H 2909 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" H 2850 2700 50  0001 C CNN
F 3 "~" H 2850 2700 50  0001 C CNN
	1    2850 2700
	-1   0    0    1   
$EndComp
Text Label 2700 2200 0    50   ~ 0
GND
Text Label 2700 2600 0    50   ~ 0
5V
Text Label 3100 2800 0    50   ~ 0
D-
Text Label 3100 2900 0    50   ~ 0
D+
Wire Wire Line
	2850 2600 2850 2200
Wire Wire Line
	2850 2200 2700 2200
Wire Wire Line
	2850 3100 2850 3600
Wire Wire Line
	2850 3600 2700 3600
Text Notes 3200 3250 0    50   ~ 0
OR USE 4.7k
Text Notes 4750 2650 0    50   ~ 0
INITIAL REV:\n1. 5V\n2. D-\n3. D+\n4. GND
Wire Wire Line
	2700 2800 2850 2800
Connection ~ 2850 2800
Wire Wire Line
	2850 2800 3100 2800
Wire Wire Line
	2700 2900 2850 2900
Connection ~ 2850 2900
Wire Wire Line
	2850 2900 3100 2900
Text Label 2700 3300 0    50   ~ 0
GND
Text Label 2700 2700 0    50   ~ 0
GND
Text Label 2700 3000 0    50   ~ 0
GND
NoConn ~ 2700 2300
NoConn ~ 2700 2400
NoConn ~ 2700 3100
NoConn ~ 2700 3200
NoConn ~ 2700 3400
NoConn ~ 2700 3500
$EndSCHEMATC
