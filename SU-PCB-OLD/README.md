View this project on [CADLAB.io](https://cadlab.io/project/23187). 

# AcubeSat - Science Unit PCB

This repository contains schematics and layout files for
the Science Unit PCB.

_Note: This repository is a work-in-progress and untested. Please use at your own discretion._

- [AcubeSat - Science Unit PCB](#acubesat---science-unit-pcb)
  - [Requirements](#requirements)
  - [Usage](#usage)
  - [Components](#components)
  - [BOM (Bill of Materials)](#bom-bill-of-materials)

## Requirements

- You will need **KiCad version 5** to open the schematics, layout, etc.
- All the libraries used are stored inside this repo for your convenience. There are no external dependencies.
- **STM32CubeMX** for the mcu pin initialization and configuration.

## Usage

```bash
.
├── .gitignore
├── LICENCE
├── README.md
├── fp-lib-table
├── STM32CubeMX
│   ├── stm32h743zi.ioc     # STM32CubeMX project
├── lib
│   ├── 3Dmodels            # The 3D models
│   │   ├── *.stl
│   │   ├── *.stp
│   │   ├── *.wrl
│   ├── Components.dcm      # Schematic component library documentation
│   ├── Components.lib      # Schematic component library files
│   └── Components.pretty   # Footprint library folders. The folder itself is the library.
│       ├── *.kicad_mod     # Footprint files
├── su-pcb-cache.lib
├── su-pcb-rescue.dcm
├── su-pcb-rescue.lib
├── su-pcb.kicad_pcb        # PCB Layout
├── su-pcb.pro              # KiCad Project
├── su-pcb.sch              # Board Schematic
└── sym-lib-table
```

Open the `.pro` file in KiCad and then:
- choose the `.sch` file to view the schematic
- choose the `.kicad_pcb` file to view the PCB's layout.

## Components

All the `ECAD` models (PCB symbol, footprint, 3D model) are imported from `Mouser`.

The custom library we have created inside KiCad is called `components`.

Steps to find individual component models:
- Select your component from Mouser.
- Go to [this](https://componentsearchengine.com/) link and search for your model using `Manufacturing Number`.
- Download the model and import it to KiCad `schematic` and `footprint` library.

## BOM (Bill of Materials)

| Component                            | Model                                                                                                                                | Mouser                                                                                                                                         | Quantity |
| ------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------- | :------: |
| MCU                                  | [ATSAMV71Q21B-AAB](http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-44003-32-bit-Cortex-M7-Microcontroller-SAM-V71Q-SAM-V71N-SAM-V71J_Datasheet.pdf)                                             | [Link](https://www.mouser.com/ProductDetail/Microchip-Technology-Atmel/ATSAMV71Q21B-AAB?qs=j%252B1pi9TdxUaaYXmCSj2x3Q%3D%3D)                                                                   |    1     |
| NAND Flash                           | [S34ML01G200TFI003](https://eu.mouser.com/datasheet/2/980/spansion_S34ML01G2_04G2_(1)-1592283.pdf)                                   | [Link](https://eu.mouser.com/ProductDetail/797-S34M01G200TFI003)                                                                               |    1     |
| SRAM                                 | [AS6C6416-55TIN](https://eu.mouser.com/datasheet/2/12/Alliance%20Memory_64M_AS6C6416-55TIN%20v1.0%20July%202017-1223671.pdf)         | [Link](https://eu.mouser.com/ProductDetail/913-AS6C6416-55TIN)                                                                                 |    1     |
| Temperature Sensor                   | [MCP9808-E/MS](https://eu.mouser.com/datasheet/2/268/25095A-15487.pdf)                                                               | [Link](https://eu.mouser.com/ProductDetail/579-MCP9808-E-MS)                                                                                   |    2     |
| CAN Interface IC                     | [SN65HVD233D](http://www.ti.com/general/docs/suppproductinfo.tsp?distId=26&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn65hvd233) | [Link](https://eu.mouser.com/ProductDetail/595-SN65HVD233D)                                                                                    |    1     |
| Watchdog                             | [ISL88705IB846Z](https://eu.mouser.com/datasheet/2/698/isl88705-706-707-708-716-813-1529497.pdf)                                     | [Link](https://eu.mouser.com/ProductDetail/968-ISL88705IB846Z)                                                                                 |    1     |
| Crystal                              | [ECS-80-20-4X](https://eu.mouser.com/datasheet/2/122/hc-49usx-791.pdf)                                                               | [Link](https://eu.mouser.com/ProductDetail/520-HCU800-20X)                                                                                     |    1     |
| LED                                  | [LR VH9F-P2R1-1-Z](https://media.osram.info/media/resource/hires/osram-dam-2493697/LR%20VH9F.pdf)                                    | [Link](https://eu.mouser.com/ProductDetail/720-LRVH9F-P2R1-1-0)                                                                                |    5     |
| Pressure sensor                      | [LPS22HHTR](https://eu.mouser.com/datasheet/2/389/lps22hh-1395924.pdf)                                                               | [Link](https://eu.mouser.com/ProductDetail/STMicroelectronics/LPS22HHTR?qs=%2Fha2pyFadugHO3rysltSJdTKODnXgoYiUH5ZkJL778eJvlCN0kS%252B3g%3D%3D) |    2     |
| Humidity Sensor                      | [SHT30-DIS-F2.5kS](https://eu.mouser.com/datasheet/2/682/Sensirion_Humidity_Sensors_SHT3x_Datasheet_Filter_-2001072.pdf)             | [Link](https://www.google.com/aclk?sa=l&ai=DChcSEwjnv6r2y8n5AhUa-VEKHSBJCOcYABAEGgJ3cw&sig=AOD64_18FRiwgg7ouq7PS0Cz3n-_oGVewA&q&adurl&ved=2ahUKEwiyyaH2y8n5AhWU7KQKHRZnCxcQ0Qx6BAgFEAE)                |    2     |
| ESD Suppressor                       | [USBLC6-2P6](https://eu.mouser.com/datasheet/2/389/usblc6-2-957370.pdf)                                                              | [Link](https://eu.mouser.com/ProductDetail/STMicroelectronics/USBLC6-2P6?qs=sGAEpiMZZMu6TJb8E8Cjr9vr90Pxd%252B%252Bb)                          |    1     |
| Ferrite Bead for Analog Power Supply | [BK1608HW601-T](https://eu.mouser.com/datasheet/2/396/mlci07_e-1396618.pdf)                                                          | [Link](https://eu.mouser.com/ProductDetail/Taiyo-Yuden/BK1608HW601-T?qs=sGAEpiMZZMtdyQheitOmRas3gyT7ksc5JBYn1eYVv3I%3D)                        |    1     |
| Power Switch IC                      | [STMPS2151STR](https://eu.mouser.com/datasheet/2/389/stmps2141-956387.pdf)                                                           | [Link](https://eu.mouser.com/ProductDetail/STMicroelectronics/STMPS2151STR?qs=qolwYojYp3THidumZGqCjQ%3D%3D)                                    |    1     |
| USB Receptacle                       | [1-1734775-1](https://eu.mouser.com/datasheet/2/418/NG_CD_1734775_C-658760.pdf)                                                      | [Link](https://eu.mouser.com/ProductDetail/TE-Connectivity-AMP/1-1734775-1?qs=sGAEpiMZZMulM8LPOQ%252BykyMvpYkjjOa9Js4GsP5FaKY%3D)              |    1     |
| 470 Ohm Resistor                     | [RG1005P-471-D-T10](https://eu.mouser.com/datasheet/2/392/n_catalog_partition01_en-1140338.pdf)                                      | [Link](https://eu.mouser.com/ProductDetail/754-RG1005P-471DT10)                                                                                |    2     |
| 10k Ohm Resistor                     | [RG1005P-103-B-T5](https://eu.mouser.com/datasheet/2/392/n_catalog_partition01_en-1140338.pdf)                                       | [Link](https://eu.mouser.com/ProductDetail/754-RG1005P-103-B-T5)                                                                               |    2     |
| 120 Ohm Resistor                     | [RG1005P-121-B-T5](https://eu.mouser.com/datasheet/2/392/n_catalog_partition01_en-1140338.pdf)                                       | [Link](https://eu.mouser.com/ProductDetail/754-RG1005P-121-BT5)                                                                                |    2     |
| 10  uF Ceramic Capacitor             | [GRM155R60J106ME15D](https://eu.mouser.com/datasheet/2/281/murata_03052018_GRM_Series_1-1310166.pdf)                                 | [Link](https://eu.mouser.com/ProductDetail/81-GRM155R60J106ME5D)                                                                               |    ?     |
| 0.1 uF Ceramic Capacitor             | [GRM022R61A104ME01L](https://eu.mouser.com/datasheet/2/281/murata_03052018_GRM_Series_1-1310166.pdf)                                 | [Link](https://eu.mouser.com/ProductDetail/81-GRM022R61A104ME1L)                                                                               |    ?     |
| 1   uF Ceramic Capacitor             | [JMK105BJ105KP-F](https://eu.mouser.com/datasheet/2/396/mlcc02_e-1307760.pdf)                                                        | [Link](https://eu.mouser.com/ProductDetail/Taiyo-Yuden/JMK105BJ105KP-F?qs=%2Fha2pyFaduhVfLicOtMYHjTOx4ltLP0HL6UW%2FNNtlnzpgCKvRJj7Ng%3D%3D)    |    ?     |
| 33  pF Ceramic Capacitor             | [GCM1555C1H330FA16J](https://eu.mouser.com/datasheet/2/281/murata_03122018_GCM_Series-1310150.pdf)                                   | [Link](https://eu.mouser.com/ProductDetail/81-GCM1555C1H330FA6J)                                                                               |    ?     |
